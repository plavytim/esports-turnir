# Upute za puštanje u pogon

## Postavljanje okoline

Aplikacija se pušta u pogon unutar Kubernetes klastera.
Kubernetes klaster može se postaviti na mnogo načina:
- u oblaku na Azure Kubernetes Service (Microsoft Azure)
- u oblaku na Elastic Kubernetes Service (Amazon Web Services)
- na serveru pomoću microk8s
- na serveru pomoću minikube

Naš klaster instaliran je na Ubuntu serveru pomoću microk8s.

```shell
snap install microk8s --classic
microk8s start
microk8s enable dns storage
microk8s config # kubeconfig za komunikaciju s klasterom
```

Kubeconfig potrebno je dodati kao CI/CD varijablu na GitLab repozitorij tipa `File` s nazivom `KUBECONFIG`.

## Deployment proces

Na repozitoriju projekta postavljen je Continuous Delivery (CI/CD). Koristi se princip GitOps, što znači da se na svaki push backend i frontend automatski izgrade te upogone. Developer ne mora trošiti vrijeme na deployment.

Repozitorij ima 2 Dockerfile-a, jedan za frontend, jedan za backend.

Dockerfile-ovi imaju slične naredbe kao i kod lokalnog razvoja, ali postavljaju produkcijsko okruženje.

Izgradnja backenda izvedena je u 2 koraka:
- Izgradnja mavena paketa
- Kopiranje .jar datoteke u Docker sliku

CI/CD izgradi Docker slike na temelju Dockerfile-ova. Zatim koristi relativno jednostavan Kubernetes manifest koji stvori Deployment, Service i Ingress na Kubernetes klasteru.
Pristup Kuberentes klaster ostvaraje se pomoću kubeconfig datoteke koja je specifirana u CI/CD varijablama.
