const {By,Key,Builder} = require("selenium-webdriver")
require("chromedriver")
 
const sleep = (milliseconds) => {
        return new Promise(resolve => setTimeout(resolve, milliseconds))
      }

/*
Ispitni slučaj 2: Registriraj se, ažuriraj profil i izbriši ga
Ulaz:
  - Ime, prezime, email adresa, korisničko ime i lozinka korisnika
Očekivani izlaz:
  - Nijedan korak ne izaziva grešku, baza ostaje netaknuta
Koraci:
  - Korisnik klikne gumb Registracija
  - Korisnik unese ime, prezime, email adresu, korisničko ime i lozinku
  - Korisnik ponovi unos lozinke
  - Korisnik klikne gumb za registraciju
  - Korisnik otvori hamburger menu
  - Korisnik klikne gumb Moj profil
  - Korisnik klikne gumb za izmjenu podataka
  - Korisnik unese novu email adresu
  - Korisnik spremi izmjene
  - Korisnik klikne gumb za brisanje korisničkog računa
  - Korisnik potvrdi namjeru
*/

async function RegisterUpdateDeleteUser(TEST_URL){
 
       let driver = await new Builder().forBrowser("chrome").build()
 
        await driver.get(TEST_URL)
            
        await driver.findElement(By.linkText("Registracija")).click()
        await sleep(500)
                
        await driver.findElement(By.name("firstname")).sendKeys("Sel")
        await driver.findElement(By.name("lastname")).sendKeys("Enium")
        await driver.findElement(By.name("email")).sendKeys("sel.enium@fer.hr")
        await driver.findElement(By.name("username")).sendKeys("selenium")
        await driver.findElement(By.name("password")).sendKeys("selenium")
        await driver.findElement(By.name("repeatPassword")).sendKeys("selenium")
        await driver.findElement(By.id("form-button")).click()
        await sleep(3000)
        
        await driver.findElement(By.id("hamburger")).click()
        await driver.findElement(By.linkText("Moj profil")).click()
        await sleep(500)
        await driver.findElement(By.id("update-user")).click()
        await sleep(500)
        await driver.findElement(By.name("email")).clear()
        await driver.findElement(By.name("email")).sendKeys("se55555@fer.hr")
        await driver.findElement(By.id("save")).click()
        await sleep(500)
        
        await driver.findElement(By.id("delete-user")).click()
        await sleep(500)
        await driver.findElement(By.id("potvrdi")).click()
        await sleep(500)
        
        await driver.quit()

        return true
 
}

export default RegisterUpdateDeleteUser