const {By,Key,Builder} = require("selenium-webdriver")
require("chromedriver")
 
const sleep = (milliseconds) => {
        return new Promise(resolve => setTimeout(resolve, milliseconds))
      }

/*
Ispitni slučaj 1: Prijavi se kao administrator
Ulaz:
  - korisničko ime i lozinka administratora
Očekivani izlaz:
  - Nakon prijave, korisnik će moći kliknuti na gumb Dodaj gamemastera (dostupno samo administratoru)
Koraci:
  - Korisnik klikne na gumb Prijava
  - Korisnik unese korisničko ime i lozinku
  - Korisnik klikne gumb za prijavu
  - Korisnik otvori hamburger menu
  - Korisnik klikne gumb Dodaj gamemastera
  - Korisnik klikne gumb Odjava
*/

async function LoginAsAdmin(TEST_URL){
 
       let driver = await new Builder().forBrowser("chrome").build()
 
        await driver.get(TEST_URL)
            
        await driver.findElement(By.linkText("Prijava")).click()
        await sleep(500)
        
        await driver.findElement(By.name("username")).sendKeys("karlo_karlic")
        await driver.findElement(By.name("password")).sendKeys("karlopass")
        await driver.findElement(By.id("form-button")).click()
        await sleep(500)

        await driver.findElement(By.id("hamburger")).click()
        await driver.findElement(By.linkText("Dodaj gamemastera")).click()
        await driver.findElement(By.linkText("Odjava")).click()

        await driver.quit()

        return true
 
}

export default LoginAsAdmin