const {By,Key,Builder} = require("selenium-webdriver")
require("chromedriver")
 
const sleep = (milliseconds) => {
        return new Promise(resolve => setTimeout(resolve, milliseconds))
      }

/*
Ispitni slučaj 4: Stvori i obriši turnir i meč
Ulaz:
  - korisničko ime i lozinka administratora ili gamemastera
Očekivani izlaz:
  - Nijedan korak ne izaziva grešku, baza ostaje netaknuta
Koraci:
  - Korisnik klikne na gumb Prijava
  - Korisnik unese korisničko ime i lozinku
  - Korisnik klikne gumb za prijavu
  - Korisnik otvori hamburger menu
  - Korisnik klikne gumb Turniri
  - Korisnik klikne gumb za svtaranje novog turnira
  - Korisnik unese naziv turnira i odabere igru
  - Korisnik potvrđuje stvaranje turnira
  - Korisnik biva vraćen na stranicu svih turnira
  - Korisnik odabire stvoren turnir
  - Korisnik odabire gumb za dodavanje meča
  - Korisnik unese datum i vrijeme meča
  - Korisnik odabere prva dva ponuđena tima
  - Korisnik potvrdi stvaranje meča
  - Korisnik biva vraćen na stranicu stvorenog turnira
  - Korisnik odabire gumb za brisanje turnira
*/

async function CreateDeleteTournamentMatch(TEST_URL){
 
       let driver = await new Builder().forBrowser("chrome").build()
 
        await driver.get(TEST_URL)
            
        await driver.findElement(By.linkText("Prijava")).click()
        await sleep(500)
        
        await driver.findElement(By.name("username")).sendKeys("karlo_karlic")
        await driver.findElement(By.name("password")).sendKeys("karlopass")
        await driver.findElement(By.id("form-button")).click()
        await sleep(500)
        
        await driver.findElement(By.id("hamburger")).click()
        await driver.findElement(By.linkText("Turniri")).click()
        await sleep(500)
        
        await driver.findElement(By.linkText("Stvori novi turnir")).click()
        await sleep(500)
        
        await driver.findElement(By.name("tournamentName")).sendKeys("novinovi")
        await driver.findElement(By.name("gameName")).sendKeys("c")
        await driver.findElement(By.id("form-button")).click()
        await sleep(500)
        
        await driver.findElement(By.xpath("//a[contains(text(),'novinovi')]")).click()
        await sleep(500)
        
        await driver.findElement(By.id("add-match")).click()
        await sleep(500)
        
        await driver.findElement(By.name("matchDate")).sendKeys("05092022")
        await driver.findElement(By.name("matchTime")).sendKeys("1000AM")
        await sleep(500)
        await driver.findElement(By.xpath("(//input[@type='checkbox'])[1]")).click()
        await driver.findElement(By.xpath("(//input[@type='checkbox'])[2]")).click()
        await driver.findElement(By.id("form-button")).click()
        await sleep(7500)

        await driver.switchTo().alert().accept()
        await sleep(500)
        
        await driver.findElement(By.id("delete-tournament")).click()
        await sleep(500)
        
        await driver.quit()

        return true

}

export default CreateDeleteTournamentMatch