const {By,Key,Builder} = require("selenium-webdriver")
require("chromedriver")
 
const sleep = (milliseconds) => {
        return new Promise(resolve => setTimeout(resolve, milliseconds))
      }

/*
Ispitni slučaj 3: Izazovi pogrešku prilikom registracije
Ulaz:
  - Ime, prezime, email adresa, korisničko ime i lozinka korisnika
Očekivani izlaz:
  - Korisnik nije uspješno registriran
Koraci:
  - Korisnik klikne gumb Registracija
  - Korisnik unese ime, prezime, email adresu u nepodržanom formatu, korisničko ime i lozinku
  - Korisnik ponovi unos lozinke
  - Korisnik klikne gumb za registraciju
  - Korisnik biva obaviješten o pogrešci
  - Korisnik klikne gumb Prijava (kako bi dokazao da registracija nije prošla)
*/
      
async function RegisterUserWrongEmailFormat(TEST_URL){
 
       let driver = await new Builder().forBrowser("chrome").build()
 
        await driver.get(TEST_URL)
            
        await driver.findElement(By.linkText("Registracija")).click()
        await sleep(500)
        
        await driver.findElement(By.name("firstname")).sendKeys("Sel")
        await driver.findElement(By.name("lastname")).sendKeys("Enium")
        await driver.findElement(By.name("email")).sendKeys("sel.enium@gmail.com")
        await driver.findElement(By.name("username")).sendKeys("selenium")
        await driver.findElement(By.name("password")).sendKeys("selenium")
        await driver.findElement(By.name("repeatPassword")).sendKeys("selenium")
        await driver.findElement(By.id("form-button")).click()
        await sleep(500)
        
        await driver.findElement(By.linkText("Prijava")).click()
        await sleep(500)

        await driver.quit()

        return true
 
}

export default RegisterUserWrongEmailFormat