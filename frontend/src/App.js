import React from "react";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import Home from "./components/Home/Home";
import Register from "./components/Register/Register";
import Login from "./components/Login/Login";
import Logout from "./components/Logout";
import Tournaments from "./components/Tournaments";
import Tournament from "./components/Tournament";
import Teams from "./components/Teams";
import Team from "./components/Team";
import Players from "./components/Players";
import Player from "./components/Player";
import CreateTournament from "./components/CreateTournament";
import CreateTeam from "./components/CreateTeam";
import MyProfile from "./components/MyProfile/MyProfile";
import MyTeams from "./components/MyTeams/MyTeams";
import DeleteUser from "./components/DeleteUser";
import UpdateProfile from "./components/MyProfile/UpdateProfile";
import ChangePassword from "./components/MyProfile/ChangePassword";
import AddGamemaster from "./components/AddGamemaster";
import LeaveTeam from "./components/LeaveTeam";
import NotFound from "./components/NotFound";
import UpdateTeam from "./components/UpdateTeam";
import AddMatch from "./components/AddMatch";
import RemoveMatch from "./components/RemoveMatch";
import Match from "./components/Match";
import AddMatchResult from "./components/AddMatchResult";
import AddTournamentResult from "./components/AddTournamentResult";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route path="/" exact element={<Home />} />
          <Route path="/login" exact element={<Login />} />
          <Route path="/register" exact element={<Register />} />
          <Route path="/logout" exact element={<Logout />} />
          <Route path="/tournaments" exact element={<Tournaments />} />
          <Route path="/tournaments/:id" element={<Tournament />} />
          <Route path="/teams" exact element={<Teams />} />
          <Route path="/teams/:id" element={<Team />} />
          <Route path="/players" exact element={<Players />} />
          <Route path="/players/:id" element={<Player />} />
          <Route path="/createtournament" exact element={<CreateTournament />} />
          <Route path="/createteam" exact element={<CreateTeam />} />
          <Route path="/myprofile" exact element={<MyProfile />} />
          <Route path="/myteams" exact element={<MyTeams />} />
          <Route path="/deleteuser" exact element={<DeleteUser />} />
          <Route path="/updateprofile" exact element={<UpdateProfile/>} />
          <Route path="/changepassword" exact element={<ChangePassword/>} />
          <Route path="/addgamemaster" exact element={<AddGamemaster />} />
          <Route path="/leaveteam" exact element={<LeaveTeam />} />
          <Route path="/updateteam/:id" element={<UpdateTeam />} />
          <Route path="/addmatch/:id" element={<AddMatch />} />
          <Route path="/removematch/:tournamentid/:matchid" element={<RemoveMatch />} />
          <Route path="/match/:tournamentid/:matchid" element={<Match />} />
          <Route path="/addmatchresult/:tournamentid/:matchid" element={<AddMatchResult />} />
          <Route path="/addtournamentresult/:id" element={<AddTournamentResult />} />
          <Route path="*" exact element={<NotFound />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
