import React from "react";
import { NavLink } from "react-router-dom";
import { checkRole } from "../CheckRoleFunction";

//style
import "./NavBar.css";

function addGamemasterLink(role) {
  if (role === "ADMIN") {
    return (
      <div className="link-container">
        <NavLink
          to="/addgamemaster"
          className={(navData) =>
            navData.isActive ? "active-nav-link" : "inactive-nav-link"
          }
        >
          Dodaj gamemastera
        </NavLink>
      </div>
    );
  }
}

const NavLinks = () => {
  if (localStorage.getItem("token") == null) {
    return (
      <div className="link-menu">
        <div className="link-container">
          <NavLink
            to="/"
            className={(navData) =>
              navData.isActive ? "active-nav-link" : "inactive-nav-link"
            }
          >
            Početna
          </NavLink>
        </div>
        <div className="link-container">
          <NavLink
            to="/tournaments"
            className={(navData) =>
              navData.isActive ? "active-nav-link" : "inactive-nav-link"
            }
          >
            Turniri
          </NavLink>
        </div>
        <div className="link-container">
          <NavLink
            to="/teams"
            className={(navData) =>
              navData.isActive ? "active-nav-link" : "inactive-nav-link"
            }
          >
            Timovi
          </NavLink>
        </div>
        <div className="link-container">
          <NavLink
            to="/players"
            className={(navData) =>
              navData.isActive ? "active-nav-link" : "inactive-nav-link"
            }
          >
            Igrači
          </NavLink>
        </div>
      </div>
    );
  } else {
    return (
      <div className="link-menu">
        <div className="link-container">
          <NavLink
            to="/tournaments"
            className={(navData) =>
              navData.isActive ? "active-nav-link" : "inactive-nav-link"
            }
          >
            Turniri
          </NavLink>
        </div>
        <div className="link-container">
          <NavLink
            to="/teams"
            className={(navData) =>
              navData.isActive ? "active-nav-link" : "inactive-nav-link"
            }
          >
            Timovi
          </NavLink>
        </div>
        <div className="link-container">
          <NavLink
            to="/players"
            className={(navData) =>
              navData.isActive ? "active-nav-link" : "inactive-nav-link"
            }
          >
            Igrači
          </NavLink>
        </div>
        <div className="link-container">
          <NavLink
            to="/myprofile"
            className={(navData) =>
              navData.isActive ? "active-nav-link" : "inactive-nav-link"
            }
          >
            Moj profil
          </NavLink>
        </div>
        <div className="link-container">
          <NavLink
            to="/myteams"
            className={(navData) =>
              navData.isActive ? "active-nav-link" : "inactive-nav-link"
            }
          >
            Moji timovi
          </NavLink>
        </div>
        {addGamemasterLink(checkRole())}
      </div>
    );
  }
};

export default NavLinks;
