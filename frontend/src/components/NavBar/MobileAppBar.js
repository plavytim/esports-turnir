import React from "react";
import { NavLink } from "react-router-dom";

//style
import "./NavBar.css";

const MobileAppBar = () => {
  if (localStorage.getItem("token") == null) {
    return (
      <div className="app-bar-menu">
        <div className="link-container">
          <NavLink
            to="/login"
            className={(navData) =>
              navData.isActive ? "active-nav-link" : "inactive-nav-link"
            }
          >
            Prijava
          </NavLink>
        </div>
        <div className="link-container">
          <NavLink
            to="/register"
            className={(navData) =>
              navData.isActive ? "active-nav-link" : "inactive-nav-link"
            }
          >
            Registracija
          </NavLink>
        </div>
      </div>
    );
  } else {
    return (
      <div className="app-bar-menu">
        <div className="link-container">
          <NavLink
            to="/"
            className={(navData) =>
              navData.isActive ? "active-nav-link" : "inactive-nav-link"
            }
          >
            Početna
          </NavLink>
        </div>
        <div className="link-container">
          <NavLink
            to="/logout"
            className={(navData) =>
              navData.isActive ? "active-nav-link" : "inactive-nav-link"
            }
          >
            Odjava
          </NavLink>
        </div>
      </div>
    );
  }
};

export default MobileAppBar;
