import { useState } from "react";

//icons
import {CgMenuRound} from "react-icons/cg";
import {CgClose} from "react-icons/cg";

//style
import "./NavBar.css"

import MobileNavLinks from "./MobileNavLinks";


const MobileNavBar = () =>{

   const[open, setOpen] = useState(false);

   const hamburgerIcon = <CgMenuRound 
                           id = "hamburger"
                           className = "hamburger-icon"
                           onClick = {() => setOpen(!open)}
                         />

   const closeIcon = <CgClose 
                        className = "hamburger-icon"
                        onClick = {() => setOpen(!open)}
                     />                     

   
   return <div className = "mobile-nav-bar">
      {open ? closeIcon : hamburgerIcon}
      {open && <MobileNavLinks/>}
   </div>
}

export default MobileNavBar;