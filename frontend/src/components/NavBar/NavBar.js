import NavLinks from "./NavLinks";
import MobileAppBar from "./MobileAppBar";

import { useState } from "react";

//icons
import { CgMenuRound } from "react-icons/cg";
import { CgClose } from "react-icons/cg";

//style
import "./NavBar.css"

const NavBar = () => {
   const [open, setOpen] = useState(false);

   const hamburgerIcon = <CgMenuRound
      id="hamburger"
      className="hamburger-icon"
      onClick={() => setOpen(!open)}
   />

   const closeIcon = <CgClose
      className="hamburger-icon"
      onClick={() => setOpen(!open)}
   />

   return <div>
      <div className="app-bar">
         <MobileAppBar />
      </div>
      <div className="nav-bar">
         <NavLinks />
      </div>
   </div>
}

export default NavBar;