import React from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { useEffect } from "react/cjs/react.development";
import Header from "./Header/Header";

function UpdateTeam() {
  const idObject = useParams();
  const teamId = idObject.id;

  const [teamForm, setTeamForm] = React.useState({
    teamName: "",
  });
  const [teamData, setTeamData] = React.useState({});
  const [error, setError] = React.useState("");
  const navigate = useNavigate();

  useEffect(() => {
    var fetchURL = process.env.REACT_APP_BACKEND_URL + "/teams/" + teamId;

    fetch(fetchURL)
      .then((response) => response.json())
      .then((data) => {
        setTeamData(data);
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
    console.log(teamData);
  }, []);

  var permission = true;

  if (Object(teamData.leadPlayer).username === localStorage.getItem("username")) {
    permission = true;
  }

  function onChange(event) {
    const { name, value } = event.target;
    setTeamForm((oldForm) => ({ ...oldForm, [name]: value }));
  }

  async function onSubmit(e) {
    e.preventDefault();
    setError("");

    const body = JSON.stringify({
      id: teamData.id,
      teamName: teamForm.teamName,
      gameName: teamData.game.name,
      numberOfWins: teamData.numberOfWins,
      numberOfPlayed: teamData.numberOfPlayed,
      numberOfTournaments: teamData.numberOfTournaments,
    });
    console.log(body);
    const options = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
      body: body,
    };

    let response = await fetch(
      process.env.REACT_APP_BACKEND_URL + "/teams/" + teamId,
      options
    );

    let data = await response.json();

    console.log(response.status);
    if (response.status !== 200) {
      setError(data.message);
    } else {
      navigate("/myteams");
    }
  }

  if (permission) {
    return (
      <div>
        <div>
        <img className="home-page" src="/slika.jpg"></img>
          <Header />
        </div>
        <div className="centeredCollumn">
          <h1>Promijeni podatke tima</h1>
          <form onSubmit={onSubmit}>
            <p>
              <input
                type="text"
                name="teamName"
                onChange={onChange}
                value={teamForm.teamName}
                placeholder="Novo ime tima"
                minLength="5"
                required
              ></input>
            </p>
            <p className="button-holder">
              <button type="submit">Potvrdi</button>
            </p>
            <p className="button-holder">
              <button className="buttonLeft" type="button" onClick={() => {navigate("/myteams")}}>Odustani</button>
            </p>
          </form>
          {error ? <p className="error">{error}</p> : null}
        </div>
      </div>
    );
  } else {
    return (
      <body>
        <Header />
        <div className="centeredCollumn">
          <p>
            Ne smijete mijenjati podatke za ovaj tim. Vratite se na početnu
            stranicu
            <Link style={{ color: "green" }} to="/">
              ovom poveznicom
            </Link>
            .
          </p>
        </div>
      </body>
    );
  }
}
export default UpdateTeam;
