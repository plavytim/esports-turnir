import React from "react";
import Header from "./Header/Header";
import { useNavigate } from "react-router-dom";

function LeaveTeam() {
  const [error, setError] = React.useState("");
  const navigate = useNavigate();

  console.log("LeadId je " + localStorage.getItem("leadId"));
  console.log("Id je " + localStorage.getItem("id"));

  async function onSubmit(e) {
    e.preventDefault();
    setError("");

    const options = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };

    let response = "";

    if (localStorage.getItem("leadId") === localStorage.getItem("id")) {
      localStorage.setItem("leadId", null);
      response = await fetch(
        process.env.REACT_APP_BACKEND_URL +
          "/teams/" +
          localStorage.getItem("teamId") +
          "/leadPlayer",
        options
      );
      console.log("LEAD PLAYER IZLAZI");
    } else {
      response = await fetch(
        process.env.REACT_APP_BACKEND_URL +
          "/teams/" +
          localStorage.getItem("teamId") +
          "/players/" +
          localStorage.getItem("id"),
        options
      );
      console.log("OBIČAN PLAYER IZLAZI");
    }
    let data = await response.json();
    localStorage.removeItem("teamId");
    localStorage.removeItem("leadId");
    console.log(data);
    console.log(response.status);

    if (response.status === 200) {
      alert("Uspješno ste izašli iz tima");
      navigate("/myteams");
    } else {
      setError(data.message);
    }
  }

  function onReset() {
    navigate("/teams/" + localStorage.getItem("teamId"));
  }

  console.log(localStorage.getItem("leadId"));

  return (
    <body>
      <img className="home-page" src="/slika.jpg"></img>
      <Header />
      <div className="centeredCollumn">
        <h1>Jeste li sigurni da želite izaći iz tima?</h1>
        <button className="buttonLeft" onClick={onSubmit}>Potvrdi</button>
        <button className="buttonLeft" onClick={onReset}>Odustani</button>
      </div>
      <div className="centeredCollumn">
      {error ? <p className="error">{error}</p> : null}
      </div>

    </body>
  );
}

export default LeaveTeam;
