import React, { useState, useEffect } from "react";
import Header from "./Header/Header";
import moment from "moment";
import { Link, useNavigate, useParams } from "react-router-dom";
import { checkRole } from "./CheckRoleFunction";

function Tournament() {
  const [data, setData] = useState([]);
  const [gameName, setGameName] = useState([]);
  const [matches, setMatches] = useState([]);
  const navigate = useNavigate();
  const idObject = useParams();
  const id = idObject.id;

  async function deleteTournament() {
    const options = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };
    let response = await fetch(
      process.env.REACT_APP_BACKEND_URL + "/tournaments/" + id,
      options
    );
    let data = await response.json();
    navigate("/tournaments");
  }

  useEffect(() => {
    fetch(process.env.REACT_APP_BACKEND_URL + "/tournaments/" + id)
      .then((response) => response.json())
      .then((data) => {
        setData(data);
        setGameName(data.game.name);
        setMatches(data.matches);
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }, []);

  let matchData = matches.map((element) => (
    <tr key={element.id}>
      <th>
        <Link className="crniLink" to={"/match/" + id + "/" + element.id}>
          {moment(element.beginning).format("D.M.y. u H:mm")}
        </Link>
      </th>
      <th>{element.team1.teamName}</th>
      <th>{element.team2.teamName}</th>
    </tr>
  ));

  function deleteTournamentButton(role) {
    if (role === "GAMEMASTER"
        || role === "ADMIN") {
      return (
        <button id="delete-tournament" className="buttonLeft" onClick={deleteTournament}>
          Izbriši turnir
        </button>
      );
    }
  }

  function addMatchButton(role) {
    if ((role === "GAMEMASTER"
        || role === "ADMIN")
        && Object(data.winnerTeam).teamName == null){
      return (
        <div>
          <button
            id="add-match"
            className="buttonLeft"
            onClick={() => {
              navigate("/addmatch/" + data.id);
            }}
          >
            Dodaj meč
          </button>
        </div>
      );
    }
  }

  function addTournamentResultButton(role) {
    if ((role === "GAMEMASTER"
        || role === "ADMIN")
        && Object(data.winnerTeam).teamName == null){

      return (
        <div>
          <button
            id="update-tournament"
            className="buttonLeft"
            onClick={() => {
              navigate("/addtournamentresult/" + data.id);
            }}
          >
            Dodaj rezultat turnira
          </button>
        </div>
      );
    }
  }


  return (
    <body>
      <img className="home-page" src="/slika.jpg"></img>
      <Header />
      <div className="tournaments">
        <div className="centeredCollumn">
          <div className="tournaments-text">
            <h1 className="mainTextLeft">{data.tournamentName}</h1>
            {/* <hr className="hr1"></hr> */}
            <p className="mainTextLeft">
              <b>Igra:</b> {gameName}
            </p>
            <p className="mainTextLeft">
              <b>Pobjednik:</b> {Object(data.winnerTeam).teamName == null ? "-" : Object(data.winnerTeam).teamName}
            </p>
            <p className="mainTextLeft">
              <b>↓ Mečevi ↓</b>
            </p>
          </div>
          <div className="table">
            <table className="bigTable">
              <tbody>
                <tr className="bigTableFirstRow" key="POCETNIREDAK">
                  <th>Početak</th>
                  <th>Tim 1</th>
                  <th>Tim 2</th>
                </tr>
                {matchData}
              </tbody>
            </table>
          </div>
          
        </div>
        <div className="button-tournaments">
          {deleteTournamentButton(checkRole())}
          {addMatchButton(checkRole())}
          {addTournamentResultButton(checkRole())}
        </div>
      </div>
    </body>
  );
}

export default Tournament;
