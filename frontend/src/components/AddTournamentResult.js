import React, { useState, useEffect } from "react";
import Header from "./Header/Header";
import { useNavigate, useParams } from "react-router-dom";

function AddTournamentResult() {
  const [tournamentData, setTournamentData] = useState([]);
  const [error, setError] = React.useState("");
  const navigate = useNavigate();
  const idObject = useParams();
  const id = idObject.id;
  const winnerTeamCandidates = [];

  useEffect(() => {
    fetch(process.env.REACT_APP_BACKEND_URL + "/tournaments/" + id)
      .then((response) => response.json())
      .then((data) => {
        setTournamentData(data);
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }, []);

  console.log(tournamentData);

  if (tournamentData.matches !== undefined && tournamentData !== "") {
    tournamentData.matches.forEach(function (element) {
      if (!winnerTeamCandidates.includes(element.team1.teamName)) {
        winnerTeamCandidates.push(element.team1.teamName);
      }
      if (!winnerTeamCandidates.includes(element.team2.teamName)) {
        winnerTeamCandidates.push(element.team2.teamName);
      }
    });
  }

  async function onSubmit(e) {
    e.preventDefault();
    setError("");

    const body = JSON.stringify({
      id: tournamentData.id,
      tournamentName: tournamentData.tournamentName,
      winnerTeamName: tournamentData.winnerTeam,
    });
    const options = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
      body: body,
    };

    console.log("ŠALJEM OVAJ BODY " + body);
    let response = await fetch(
      process.env.REACT_APP_BACKEND_URL + "/tournaments/" + id,
      options
    );
    let data = await response.json();
    if (response.status !== 200) {
      setError(data.message);
    } else {
      alert("Uspješno ste dodali rezultat turnira");
      navigate("/tournaments/" + id);
    }
  }

  const displayWinnerTeamCandidates = winnerTeamCandidates.map((team) => (
    <option value={team}>{team}</option>
  ));

  function handleChange(event) {
    tournamentData.winnerTeam = event.target.value;
    console.log("WINNER TEAM CHANGED TO " + tournamentData.winnerTeam);
  }

    return (
        <body>
            <div>
            <img className="home-page" src="/slika.jpg"></img>
                <Header/>
            </div>
            <div className="centeredCollumn">
                    <form
                    className="victory-tour"
                    onSubmit={onSubmit}
                    action={process.env.REACT_APP_BACKEND_URL + "/tournaments/" + id}
                    >
                        <label>Odaberite pobjednika turnira:</label>
                        <select name="matchWinningTeam" onChange={handleChange} required>
                            <option value="" selected disabled hidden>
                            Odaberi tim
                            </option>
                            {displayWinnerTeamCandidates}
                        </select>
                        <p className="button-holder">
                            <button type="submit">Potvrdi</button>
                        </p>
                        <div className="button-holder-pass" >
                      <button className = "button" type="button" onClick={() => {navigate("/tournaments/" + id)} }>Odustani</button>
                     </div>
                    </form>
                </div>
        </body>
    )
}

export default AddTournamentResult;
