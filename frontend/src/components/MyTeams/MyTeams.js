import React, { useState, useEffect } from "react";
import { useNavigate, Link } from "react-router-dom";

import Header from "../Header/Header";

import "../../App.css"

function MyTeams() {
  const [data, setData] = useState([]);
  const navigate = useNavigate();

  function redirectTimeout() {
    setTimeout(() => {
      navigate("/login");
    }, 0);
  }

  useEffect(() => {
    fetch(process.env.REACT_APP_BACKEND_URL + "/teams")
      .then((response) => response.json())
      .then((data) => {
        setData(data);
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }, []);


  const filteredData = [];

  function checkIfPlayerInTeam(team) {
    for (let i = 0; i < team.players.length; i++) {
      if (team.players[i].username == localStorage.getItem("username")) {
        filteredData.push(team);
        return;
      }
    }
  }

  data.forEach(checkIfPlayerInTeam);
  console.log(filteredData);
  if (localStorage.getItem("token") != null) {
    return (
      <div>
        <img className="home-page" src="/slika.jpg"></img>
        <Header />
        <div>
          <table className="bigTable">
            <tbody>
              <tr className="bigTableFirstRow" key="POCETNIREDAK">
                <th>Ime</th>
                <th>Igra</th>
                <th>Voditelj</th>
              </tr>
              {filteredData.map((element) => (
                <tr key={element.id}>
                  <th>
                    <Link className="crniLink" to={"/teams/" + element.id}>
                      {element.teamName}
                    </Link>
                  </th>
                  <th>{element.game.name}</th>
                  <th>{element.leadPlayer.username}</th>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <p className="button-holder">
          <a href="/createteam">
            <button type="submit">Stvori novi tim</button>
          </a>
        </p>
      </div>
    );
  } else {
    return (
      <div>
        <Header />
        Prvo se prijavite
        {redirectTimeout()}
      </div>
    );
  }
}

export default MyTeams;
