import React from "react";

import { useState } from "react";


//styles
import "./SideBar.css"

//icons
import {CgMenuRound} from "react-icons/cg";
import {CgClose} from "react-icons/cg";
import SideLinks from "./SideLinks";

const SideBar = () =>{
   const[open, setOpen] = useState(false);

   const hamburgerIcon = <CgMenuRound 
                           id= "hamburger"
                           className = "hamburger-icon"
                           onClick = {() => setOpen(!open)}
                         />

   const closeIcon = <CgClose 
                        className = "hamburger-icon"
                        onClick = {() => setOpen(!open)}
                     />    

   return ( <div className = "side-bar">
      {open ? closeIcon : hamburgerIcon}
      {open && <SideLinks/>}
   </div>
   );
}

export default SideBar;