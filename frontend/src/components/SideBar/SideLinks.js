import React from "react";
import { NavLink } from "react-router-dom";
import { checkRole } from "../CheckRoleFunction";

//style
import "./SideBar";

function addGamemasterLink(role) {
  if (role === "ADMIN") {
    return (
      <div className="link-container-side">
        <NavLink
          to="/addgamemaster"
          className={(navData) =>
            navData.isActive ? "active-nav-link" : "inactive-nav-link"
          }
        >
          Dodaj gamemastera
        </NavLink>
      </div>
    );
  }
}

/*function addMatchLink(role) {
  if (role === "GAMEMASTER") {
    return (
      <div className="link-container-side">
        <NavLink
          to="/addmatch"
          className={(navData) =>
            navData.isActive ? "active-nav-link" : "inactive-nav-link"
          }
        >
          Dodaj meč
        </NavLink>
      </div>
    ); 
  }
}*/

const SideLinks = () => {
  return (
    <div className="side-link-menu">
      <div className="link-container-side">
        <NavLink
          to="/myprofile"
          className={(navData) =>
            navData.isActive ? "active-nav-link" : "inactive-nav-link"
          }
        >
          Moj profil
        </NavLink>
      </div>
      <div className="link-container-side">
        <NavLink
          to="/myteams"
          className={(navData) =>
            navData.isActive ? "active-nav-link" : "inactive-nav-link"
          }
        >
          Moji timovi
        </NavLink>
      </div>
      {addGamemasterLink(checkRole())}
    </div>
  );
};

export default SideLinks;
