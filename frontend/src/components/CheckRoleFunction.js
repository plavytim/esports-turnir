export function checkRole() {
    if (localStorage.getItem("roleName") === "ADMIN") {
        return "ADMIN";
    } else if (localStorage.getItem("roleName") === "GAMEMASTER") {
        return "GAMEMASTER";
    } else {
        return "PLAYER";
    }
}