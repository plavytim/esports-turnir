import React, { useState, useEffect } from "react";
import { useNavigate, useParams, Link } from "react-router-dom";
import moment from "moment";
import Header from "./Header/Header";
import { checkRole } from "./CheckRoleFunction";

function Match() {
  const idObject = useParams();
  const tournamentId = idObject.tournamentid;
  const matchId = idObject.matchid;
  const [data, setData] = useState("");
  const [error, setError] = React.useState("");
  const navigate = useNavigate();

  useEffect(() => {
    fetch(process.env.REACT_APP_BACKEND_URL + "/matches/" + matchId)
      .then((response) => response.json())
      .then((data) => {
        setData(data);
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }, []);

  function addMatchResult(role) {
    if ((role === "GAMEMASTER"
        || role === "ADMIN")
        && data.winnerTeamId === null){

      return (
        <div
          className="centeredCollumn"
          onClick={() => {
            navigate("/addmatchresult/" + tournamentId + "/" + matchId);
          }}
        >
          <button className="buttonLeft">
            Dodaj rezultat meča</button>
        </div>
      );
    }
  }

  function removeMatchButton(role) {
    if (role === "GAMEMASTER"
        || role === "ADMIN") {
      return (
        <div className="centeredCollumn">
          <button
            className="buttonLeft"
            onClick={() => {
              navigate("/removematch/" + tournamentId + "/" + matchId);
            }}
          >
            Ukloni meč
          </button>
        </div>
      );
    }
  }
    
    if (data !== "") {
        return (
            <body>
                <div>
                <img className="home-page" src="/slika.jpg"></img>
                    <Header/>
                </div>
                <div className="centeredCollumn">
                    <h1 className="mainTextLeft">Podaci o meču</h1>
                      <div className="all-match-info">
                        <div className="match-text">
                          <div className="match-team-info">
                          <p className="mainTextLeft">
                          <b>Početak meča:</b> {moment(data.beginning).format("D.M.y. u H:mm")}
                          </p>
                          <p className="mainTextLeft">
                          <b>Igra:</b> {data.team1.game.name}
                          </p>
                          <p className="mainTextLeft">
                            <b>Pobjednik meča:</b> {(data.winnerTeamId === null) ? "-" : (data.winnerTeamId === data.team1.id ? data.team1.teamName : data.team2.teamName)}
                          </p>
                          </div>
                          <div className="all-team-info">
                          <div className="match-team-info">
                          <p className="mainTextLeft">
                          <b>Tim 1:</b>
                          <Link className="crniLink-spec" to={"/teams/" + data.team1.id}>
                          &nbsp;
                          {data.team1.teamName}
                          </Link>
                          </p>
                          <p className="mainTextLeft">
                          <b>Broj pobjeda:</b> {data.team1.numberOfWins}
                          </p>
                          <p className="mainTextLeft">
                          <b>Broj odigranih mečeva:</b> {data.team1.numberOfPlayed}
                          </p>
                          <p className="mainTextLeft">
                          <b>Broj odigranih turnira:</b> {data.team1.numberOfTournaments}
                          </p>
                          </div>
                          <div className="match-team-info">
                          <p className="mainTextLeft">
                          <b>Tim 2:</b>
                          <Link className="crniLink-spec" to={"/teams/" + data.team2.id}>
                          &nbsp;
                          {data.team2.teamName}
                          </Link>
                          </p>
                          <p className="mainTextLeft">
                          <b>Broj pobjeda:</b> {data.team2.numberOfWins}
                          </p>
                          <p className="mainTextLeft">
                          <b>Broj odigranih mečeva:</b> {data.team2.numberOfPlayed}
                          </p>
                          <p className="mainTextLeft">
                          <b>Broj odigranih turnira:</b> {data.team2.numberOfTournaments}
                          </p>
                          </div>
                        </div>
                      </div>
                      <div className="match-buttons">
                        {addMatchResult(checkRole())}
                        {removeMatchButton(checkRole())}
                        <div className="centeredCollumn">
                        <button className = "buttonLeft" type="button" onClick={() => {navigate("/tournaments/" + tournamentId)} }>Natrag</button>
                        </div>
                      </div>
                    </div>
                </div>
                
                
            </body>
        );
    } return null;
}

export default Match;
