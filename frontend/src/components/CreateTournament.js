import React from "react";
import { useNavigate } from "react-router-dom";
import Header from "./Header/Header";

function CreateTournament() {
  const [tournamentForm, setTournamentForm] = React.useState({
    tournamentName: "",
    gameName: "",
  });
  const [error, setError] = React.useState("");
  const navigate = useNavigate();

  function onChange(event) {
    const { name, value } = event.target;
    setTournamentForm((oldForm) => ({ ...oldForm, [name]: value }));
  }

  function setGameName(event) {
    tournamentForm.gameName = event.target.value;
  }

  async function onSubmit(e) {
    e.preventDefault();
    setError("");

    const body = JSON.stringify({
      tournamentName: tournamentForm.tournamentName,
      gameName: tournamentForm.gameName,
    });

    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
      body: body,
    };

    let response = await fetch(
      process.env.REACT_APP_BACKEND_URL + "/tournaments",
      options
    );

    let data = await response.json();

    console.log(response.status);
    if (response.status != 201) {
      setError(data.message);
    } else {
      navigate("/tournaments");
    }
  }

  return (
    <body>
      <img className="home-page" src="/slika.jpg"></img>
      <div>
        <Header />
        <div className="centeredCollumn">
        <h1>Stvori novi turnir</h1>
        <form
          onSubmit={onSubmit}
          action={process.env.REACT_APP_BACKEND_URL + "/tournaments"}
        >
          <p>
            <input
              type="text"
              name="tournamentName"
              onChange={onChange}
              value={tournamentForm.tournamentName}
              placeholder="Ime turnira"
              minLength="5"
              required
            />
          </p>
          <p className="centeredCollumn">
            <select name="gameName" onBlur={setGameName} required>
              <option value="" selected disabled hidden>
                Odaberi igru:
              </option>
              <option value="LOL">League of Legends</option>
              <option value="CSGO">Counter Strike: Global Offensive</option>
            </select>
          </p>
          <p id="form-button" className="button-holder">
            <button type="submit">Potvrdi</button>
          </p>
          <p className="error">{error}</p>
        </form>
      </div>
      </div>
      
    </body>
  );
}

export default CreateTournament;
