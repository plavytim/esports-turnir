import React, { useEffect } from "react";
import Header from "./Header/Header";
import { useNavigate, useParams } from "react-router-dom";

function AddMatch() {
    const [matchForm, setMatchForm] = React.useState({
        matchDate: "",
        matchTime: "",
        tournamentName: "",
        team1Name: "",
        team2Name: "",
    });
    const [tournamentData, setTournamentData] = React.useState("");
    const [teamData, setTeamData] = React.useState("");
    const [error, setError] = React.useState("");
    const [submitButton, setSubmitButton] = React.useState({
      text: "Potvrdi",
      disabled: false,
    });

    let teamCandidates = "";
    const idObject = useParams();
    const id = idObject.id;
    const navigate = useNavigate();

    useEffect(() => {
        fetch(process.env.REACT_APP_BACKEND_URL + "/tournaments/" + id)
          .then((response) => response.json())
          .then((data) => {
            setTournamentData(data);
          })
          .catch((error) => {
            console.error("Error fetching data: ", error);
          });
    }, []);

    useEffect(() => {
        fetch(process.env.REACT_APP_BACKEND_URL + "/teams")
        .then((response) => response.json())
        .then((data) => {
          setTeamData(data);
        })
        .catch((error) => {
          console.error("Error fetching data: ", error);
        });
    }, []);

    async function onSubmit(e) {
      e.preventDefault("");
      setError("");
      setSubmitButton({
        text: "Učitavanje...",
        disabled: true
      });

      const body = JSON.stringify({
        matchBeginning: matchForm.matchDate + "T" + matchForm.matchTime,
        tournamentName: tournamentData.tournamentName,
        team1Name: matchForm.team1Name,
        team2Name: matchForm.team2Name,
      });
  
      const options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
        body: body,
      };
  
      let response = await fetch(
        process.env.REACT_APP_BACKEND_URL + "/matches",
        options
      );
  
      let data = await response.json();
  
      if (response.status !== 201) {
        setError(data.message);
        setSubmitButton({
          text: "Potvrdi",
          disabled: false
        });
      } else {
        alert("Uspješno ste dodali meč");
        navigate("/tournaments/" + tournamentData.id);
      }
    }

    function setMatchDate(event) {
      matchForm.matchDate = event.target.value;
    }

    function setMatchTime(event) {
      matchForm.matchTime = event.target.value;
    }

    function setMatchTeam(event) {
      if (event.target.checked == true) {
        if (matchForm.team1Name === "") {
          matchForm.team1Name = event.target.value;
        } else if (matchForm.team2Name === "") {
          matchForm.team2Name = event.target.value;
        } else {
          //disableCheckboxes();
        }
      } else {
        if (matchForm.team1Name === event.target.value) {
          matchForm.team1Name = "";
        } else if (matchForm.team2Name === event.target.value) {
          matchForm.team2Name = "";
        }
      }
    }

    /*function checkDisabled(item) {
      console.log(item);
      if ((matchForm.team1Name !== "" && matchForm.team2Name !== "") 
          && (matchForm.team1Name !== item && matchForm.team2Name !== item)) {
          return true;
        }
      return false;
    }*/

    if (teamData !== "" && tournamentData !== "") {
      teamCandidates = teamData
          .filter(function (team) {
            if (team.game.id === tournamentData.game.id) {
              return true;
            }
            return false;
          })
          .map((team) => (
            <div className="checkbox-and-name">
              <input type="checkbox" class="regular-checkbox"  value={team.teamName} key={team.id} onClick={setMatchTeam}/>
              <label for={team.id}>{team.teamName}</label>
            </div>  
          ));
    }

    return (
        <body>
            <img className="home-page" src="/slika.jpg"></img>
            <Header />
            <div className="centeredCollumn">
                <h1>Stvori meč</h1>
                <div className="create-match-all">
                <form className="form-create-match"
                  onSubmit={onSubmit}
                  action={process.env.REACT_APP_BACKEND_URL + "/matches"}>
                    <div className="create-match-input">
                      <div className="input-box">
                        <input 
                          type="date"
                          name="matchDate"
                          onChange={setMatchDate}
                          value={matchForm.matchDateTime}
                          required
                        />
                      </div>
                      <div className="input-box">
                        <input
                          type="time"
                          name="matchTime"
                          onChange={setMatchTime}
                          value={matchForm.matchDateTime}
                          required
                        />
                      </div>
                    </div>
                    <div className="team-candidates">
                      {teamCandidates}
                    </div>
                    <button id="form-button" type="submit" disabled={submitButton.disabled}>{submitButton.text}</button>
                    <div className="button-holder-pass" >
                      <button className = "button" type="button" onClick={() => {navigate("/tournaments/" + id)} }>Odustani</button>
                     </div>
                    {/* <button id="form-button" type="submit" disabled={submitButton.disabled}>{submitButton.text}</button> */}
                </form>
                {error ? <p className="error">{error}</p> : null}
      
            </div>
            </div>
        </body>
    );
}

export default AddMatch;
