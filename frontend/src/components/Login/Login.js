import React from "react";
import { Link, useNavigate } from "react-router-dom";
import Header from "../Header/Header";

import { AiFillEyeInvisible } from "react-icons/ai";
import { AiFillEye } from "react-icons/ai";

import "./Login.css";

const crypto = require("crypto");

function Login(props) {
  const [loginForm, setLoginForm] = React.useState({
    username: "",
    password: "",
  });
  const [error, setError] = React.useState("");
  const [data, setData] = React.useState("");
  const [passwordRevealed, setPasswordRevealed] = React.useState(false);
  const navigate = useNavigate();

  function redirectTimeout() {
    setTimeout(() => {
      navigate("/");
    }, 0);
  }

  const EyeOpen = (
    <AiFillEye
      className="eye-open"
      onClick={() => setPasswordRevealed(!passwordRevealed)}
    />
  );

  const EyeClosed = (
    <AiFillEyeInvisible
      className="eye-closed"
      onClick={() => setPasswordRevealed(!passwordRevealed)}
    />
  );

  function onChange(event) {
    const { name, value } = event.target;
    setLoginForm((oldForm) => ({ ...oldForm, [name]: value }));
  }

  async function onSubmit(e) {
    e.preventDefault();
    setError("");

    const hashedPassword = crypto
      .createHash("sha1")
      .update(loginForm.password)
      .digest("hex");
    const body = JSON.stringify({
      username: loginForm.username,
      password: hashedPassword,
    });
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: body,
    };
    let response = await fetch(
      process.env.REACT_APP_BACKEND_URL + "/login",
      options
    );
    let data = await response.json();
    setData(data);
    console.log(data);

    if (data.token == null) {
      // Napraviti da su poruke na hrvatskome
      setError(data.message);
    } else {
      console.log(data.role);
      localStorage.setItem("token", data.token);
      localStorage.setItem("username", data.username);
      localStorage.setItem("id", data.id);
      localStorage.setItem("roleId", data.role.id);
      localStorage.setItem("roleName", data.role.rolename);
      navigate("/");
    }
  }

  if (localStorage.getItem("token") == null) {
    return (
      <body>
        <img className="home-page" src="/slika.jpg"></img>
        <Header />
        <div className="centeredCollumn">
          <h1>Prijava</h1>

          <form className="login-form" onSubmit={onSubmit}>
          <div className="text-box-container">
              <div>
                Ime: 
              </div>
              <input
                type="text"
                name="username"
                onChange={onChange}
                value={loginForm.username}
                placeholder="Korisničko ime"
                minLength="5"
                maxLength="15"
                required
              ></input>
            </div>
            <div className="text-box-container-special">
              <div>
                    Unesite svoju lozinku: 
                </div>
              <div className="input-container">
                  <input
                    className="input-eye"
                    type={passwordRevealed ? "text" : "password"}
                    name="password"
                    onChange={onChange}
                    value={loginForm.password}
                    placeholder="Lozinka"
                    required
                  ></input>
                  <div
                      title={passwordRevealed ? "Sakrij lozinku" : "Prikaži lozinku"}
                      className="eye"
                    >
                    {passwordRevealed ? EyeOpen : EyeClosed}
                    </div>
              </div>
            </div>
            <p className="button-holder">
              <button id="form-button" type="submit">Prijavi se</button>
            </p>
          </form>
        </div>
        <div className="centeredCollumn">
        {error ? <p className="error">{error}</p> : null}
        </div>
      </body>
    );
  } else {
    return (
      <div>
        <Header />
        Već ste prijavljeni
        {redirectTimeout()}
      </div>
    );
  }
}

export default Login;
