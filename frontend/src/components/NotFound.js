import React, { useState, useEffect } from "react";
import Header from "./Header/Header";

function NotFound() {

  return (
    <div>
      <img className="home-page" src="/slika.jpg"></img>
      <Header />
      <div className="centeredCollumn">
        <h1 className="mainTextLeft">404</h1>
        <h2 className="mainTextLeft">Čini se da ovdje nema ničeg.</h2>
      </div>
    </div>
  );
}

export default NotFound;
