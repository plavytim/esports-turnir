import React from "react";
import Header from "./Header/Header";
import { useNavigate } from "react-router-dom";

function DeleteUser() {
    const [error, setError] = React.useState("");
    const navigate = useNavigate();

    async function onSubmit(e) {
        e.preventDefault();
        setError("");

        const options = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer" + localStorage.getItem("token"),
            }
        };
        let response = await fetch
            (process.env.REACT_APP_BACKEND_URL + "/players/" + localStorage.getItem("id"),
            options
        );
        let data = await response.json();

        if (response.status === 200) {
            localStorage.clear();
            navigate("/");
        } else {
            setError(data.error);
        }
    }

    function onReset() {
        navigate("/myprofile");
    }
       
    return (
        <body>
            <img className="home-page" src="/slika.jpg"></img>
            <Header />
            <div className="centeredCollumn">
                <div className="delete-user">
                <p>
                    Jeste li sigurni da želite obrisati svoj korisnički račun?
                </p> 
                <button className="buttonLeft" id="potvrdi" onClick={onSubmit}>
                    Potvrdi
                </button> 
                <button className="buttonLeft" id="odustani" onClick={onReset}>
                    Odustani
                </button>
                </div>
                <div className="centeredCollumn">
                        {error ? <p className="error">{error}</p> : null}
                </div>
            </div>
        </body>
    );
}

export default DeleteUser;