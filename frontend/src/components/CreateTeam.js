import React from "react";
import { useNavigate } from "react-router-dom";
import Header from "./Header/Header";

function CreateTeam() {
  const [teamForm, setTeamForm] = React.useState({
    teamName: "",
    gameName: "",
    leadPlayerUsername: "",
  });

  const [error, setError] = React.useState("");
  const navigate = useNavigate();

  function onChange(event) {
    const { name, value } = event.target;
    setTeamForm((oldForm) => ({ ...oldForm, [name]: value }));
  }

  function setGameName(event) {
    teamForm.gameName = event.target.value;
  }

  async function onSubmit(e) {
    e.preventDefault();
    setError("");

    const body = JSON.stringify({
      teamName: teamForm.teamName,
      gameName: teamForm.gameName,
      leadPlayerUsername: localStorage.getItem("username"),
    });

    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
      body: body,
    };

    let response = await fetch(
      process.env.REACT_APP_BACKEND_URL + "/teams",
      options
    );

    let data = await response.json();

    console.log(response.status);
    if (response.status != 201) {
      setError(data.message);
    } else {
      navigate("/myteams");
    }
  }

  return (
    <body>
      <div>
      <img className="home-page" src="/slika.jpg"></img>
        <Header />
      </div>
      <div className="centeredCollumn">
        <h1>Stvori novi tim</h1>
        <form
          onSubmit={onSubmit}
          action={process.env.REACT_APP_BACKEND_URL + "/teams"}
        >
          <p>
            <input
              type="text"
              name="teamName"
              onChange={onChange}
              value={teamForm.teamName}
              placeholder="Ime tima"
              minLength="5"
              required
            />
          </p>
          <p className="centeredCollumn">
            <select name="gameName" onBlur={setGameName} required>
              <option value="" selected disabled hidden>
                Odaberi igru:
              </option>
              <option value="LOL">League of Legends</option>
              <option value="CSGO">Counter Strike: Global Offensive</option>
            </select>
          </p>
          <p className="button-holder">
            <button type="submit">Potvrdi</button>
          </p>
          {error ? <p className="error">{error}</p> : null}

        </form>
      </div>
    </body>
  );
}

export default CreateTeam;
