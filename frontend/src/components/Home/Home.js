import React, { useState, useEffect } from "react";

import Header from "../Header/Header";

import FullCalendar from "@fullcalendar/react"; // must go before plugins
import dayGridPlugin from "@fullcalendar/daygrid";

import "./Home.css"

function Home() {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch(process.env.REACT_APP_BACKEND_URL + "/tournaments")
      .then((response) => response.json())
      .then((data) => {
        setData(data);
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }, []);

  var colors = ["blue", "green", "red"];
  var matches = [];

  for (let i = 0; i < data.length; i++) {
    let matchesOfTournament = data[i].matches;
    let color = colors[i % 3];
    let title = data[i].tournamentName;
    let id = data[i].id
    for (let match of matchesOfTournament) {
      matches.push({
        url: "/tournaments/" + id,
        title: title,
        start: match.beginning,
        backgroundColor: color,
      });
    }
  }

  if (localStorage.getItem("token") == null) {
    return (
      <body>
        <img className="home-page" src="/slika.jpg"></img>
        <Header />
        <div className="centeredCollumn">
          <h1 className="mainText">Dobro došli na eSports turnir</h1>
        </div>
        <div className="calendar">
          <FullCalendar
            plugins={[dayGridPlugin]}
            initialView="dayGridMonth"
            eventTimeFormat={{
              hour: "numeric",
              minute: "2-digit",
              hour12: false,
            }}
            locale="hr"
            firstDay="1"
            buttonText={{
              today: "danas"
            }}
            events={matches}
            />
        </div>
      </body>
    );
  } else {
    return (
      <body>
        <img className="home-page" src="/slika.jpg"></img>
        <Header />
        <div className="centeredCollumn">
          <h1 className="mainText">
            Pozdrav {localStorage.getItem("username")}!
          </h1>
        </div>
        <div className="calendar">
          <FullCalendar
            plugins={[dayGridPlugin]}
            initialView="dayGridMonth"
            eventTimeFormat={{
              hour: "numeric",
              minute: "2-digit",
              hour12: false,
            }}
            locale="hr"
            firstDay="1"
            buttonText={{
              today: "danas"
            }}
            events={matches}
          />
        </div>
      </body>
    );
  }
}

export default Home;
