import React, { useState, useEffect } from "react";
import Header from "./Header/Header";

function Player() {
  const [data, setData] = useState([]);
  const [role, setRole] = useState([]);

  useEffect(() => {
    var myURL = window.location.href;
    var myURLLista = myURL.split("/");
    var fetchURL =
      process.env.REACT_APP_BACKEND_URL +
      "/players/" +
      myURLLista[myURLLista.length - 1];

    fetch(fetchURL)
      .then((response) => response.json())
      .then((data) => {
        setData(data);
        setRole(data.role.rolename.toLowerCase())
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }, []);

  return (
    <body>
    <div>
    <img className="home-page" src="/slika.jpg"></img>
      <Header />
      <div className="centeredCollumn">
        <h1 className="mainTextLeft">{data.username}</h1>
        <div className="player">
          <p className="mainTextLeft">
            <b>Ime:</b> {data.firstname}
          </p>
          <p className="mainTextLeft">
            <b>Prezime:</b> {data.lastname}
          </p>
          <p className="mainTextLeft">
            <b>E-mail adresa:</b> {data.email}
          </p>
          <p className="mainTextLeft">
            <b>Rola:</b> {role}
          </p>
        </div>   
      </div>
    </div>
    </body>
  );
}

export default Player;
