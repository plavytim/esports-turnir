import MobileNavBar from "../NavBar/MobileNavBar";
import NavBar from "../NavBar/NavBar";
import SideBar from "../SideBar/SideBar";

import "./Header.css";

const Header = () => {
  return (
    <div className="header-container">
      <div className="logo-container">
        <a href="/">
          <img className="logo" src="/logo.png" />
        </a>
      </div>
      <NavBar />
      <MobileNavBar />
      {localStorage.getItem("username") != null && <SideBar />}
    </div>
  );
};

export default Header;
