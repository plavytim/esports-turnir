import React, { useState, useEffect } from "react";
import Header from "../Header/Header";
import { Navigate, useNavigate } from "react-router-dom";

import { AiFillEyeInvisible } from "react-icons/ai";
import { AiFillEye } from "react-icons/ai";

import "./MyProfile.css"


const crypto = require("crypto");




const ChangePassword = () => {
   const navigate = useNavigate();
   const [error, setError] = React.useState("");
   const [passwordError, setPasswordError] = React.useState("");
   const [passwordRevealed, setPasswordRevealed] = React.useState(false);


   const EyeOpen = (
      <AiFillEye
        className="eye-open"
        onClick={() => setPasswordRevealed(!passwordRevealed)}
      />
    );
  
    const EyeClosed = (
      <AiFillEyeInvisible
        className="eye-closed"
        onClick={() => setPasswordRevealed(!passwordRevealed)}
      />
    );

   
   const [profileForm, setProfileForm] = React.useState({
      username: "",
      oldPassword: "",
      newPassword: "",
      newPasswordAgain: "",
    });

    const [oldProfileForm, setOldProfileForm] = React.useState({
      firstname: "",
      lastname: "",
      email: "",
      username: "",
      password: "",
    });


    useEffect(() => {
      fetch(process.env.REACT_APP_BACKEND_URL + "/players")
        .then((response) => response.json())
        .then((data) => {
          for (var player of data) {
            if (player.username === localStorage.getItem("username")) {
              setOldProfileForm(player);
            }
          }
        })
        .catch((error) => {
          console.error("Error fetching data: ", error);
        });
    }, []);

    function onChange(event) {
      const { name, value } = event.target;
      setProfileForm((oldForm) => ({ ...oldForm, [name]: value }));
    }

   function onBlur(event) {
      setPasswordError("");
  
      if (profileForm.newPassword !== profileForm.newPasswordAgain) {
        setPasswordError("Lozinke nisu iste");
      } else {
        setPasswordError("");
      }
    }

    async function onSubmit(e) {
      e.preventDefault();
      setError("");
  
      const hashedPasswordOld = crypto
        .createHash("sha1")
        .update(profileForm.oldPassword)
        .digest("hex");

      const hashedPasswordNew = crypto
        .createHash("sha1")
        .update(profileForm.newPassword)
        .digest("hex");

      const bodyPost = JSON.stringify({
        username: profileForm.username,
        password: hashedPasswordOld,
      });
      const bodyPut = JSON.stringify({
         firstname: oldProfileForm.firstname,
         lastname: oldProfileForm.lastname,
         username: oldProfileForm.username,
         password: hashedPasswordNew,
         email: oldProfileForm.email,
         id: localStorage.getItem("id"),
         role:{
            id:localStorage.getItem("roleId"),
            rolename: localStorage.getItem("roleName")
         }
       });

      const optionsPost = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: bodyPost,
      };
      const optionsPut = {
         method: "PUT",
         headers: {
           "Content-Type": "application/json",
           "Authorization": "Bearer" + localStorage.getItem("token"),
         },
         body: bodyPut,
       };

      let funkcija = async() => {
         const response1 = await fetch(
            process.env.REACT_APP_BACKEND_URL + "/login",
            optionsPost)
         console.log(response1)
         if(response1.status === 200){
            const response2 = await fetch(
               process.env.REACT_APP_BACKEND_URL + "/players/" + localStorage.getItem("id"),
               optionsPut)
               console.log(response2)
               navigate("/myprofile")
               
         }else{
            setError("Krivo korisničko ime ili lozinka")
         }
      }

      funkcija();
   }



   return(
      <div>
         <img className="home-page" src="/slika.jpg"></img>
         <Header/>
         <div className="main-column">
            <form className="input-fields" onSubmit={onSubmit}>
            <div className="text-box-container-spec">
               <div>
                  Unesite svoje korisničko ime: 
               </div>
               <input
                     type="text"
                     name="username"
                     onChange={onChange}
                     value={profileForm.username}
                     minLength="2"              
                     required/>
            </div>
            <div className="text-box-container">
               <div>
                  Unesite svoju trenutačnu lozinku: 
               </div>
               <div className="input-container">
                  <input
                        type={passwordRevealed ? "text" : "password"}
                        name="oldPassword"
                        onChange={onChange}
                        onBlur={onBlur}
                        value={profileForm.oldPassword}
                        placeholder="Stara lozinka"
                        required              />
                  <div
                     title={passwordRevealed ? "Sakrij lozinku" : "Prikaži lozinku"}
                     className="eye"
                  >
                  {passwordRevealed ? EyeOpen : EyeClosed}
                  </div>
               </div>
            </div>
            <div className="text-box-container">
               <div>
                  Unesite svoju novu lozinku: 
               </div>
               <div className="input-container">
                  <input
                        type={passwordRevealed ? "text" : "password"}
                        name="newPassword"
                        onChange={onChange}
                        onBlur={onBlur}
                        value={profileForm.newPassword}
                        placeholder="Nova lozinka"
                        required
                        />
                  <div
                     title={passwordRevealed ? "Sakrij lozinku" : "Prikaži lozinku"}
                     className="eye"
                  >
                  {passwordRevealed ? EyeOpen : EyeClosed}
                  </div>
               </div>
            </div>
            <div className="text-box-container">
               <div>
                  Ponovno unesite svoju novu lozinku: 
               </div>
               <div className="input-container">
                  <input
                        type={passwordRevealed ? "text" : "password"}
                        name="newPasswordAgain"
                        onChange={onChange}
                        onBlur={onBlur}
                        placeholder="Ponovite novu lozinku"
                        value={profileForm.newPasswordAgain}
                        required         
                        />
                  <div
                     title={passwordRevealed ? "Sakrij lozinku" : "Prikaži lozinku"}
                     className="eye"
                  >
                  {passwordRevealed ? EyeOpen : EyeClosed}
                  </div>
               </div>
            </div>
            <div className="button-holder">
              <button className = "button" type="submit">Spremi izmjene</button>
            </div>
            <div className="button-holder-pass" >
              <button className = "button" type="button" onClick={() => {navigate("/myprofile")}}>Odustani</button>
            </div>
            </form>
         </div>
         <div className="centeredCollumn">
         {error ? <p className="error">{error}</p> : null}
         {passwordError ? <p className="error">{error}</p> : null}
        </div>
      </div>
   )

}

export default ChangePassword;