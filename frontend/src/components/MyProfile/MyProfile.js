import React, { useState, useEffect } from "react";
import Header from "../Header/Header";
import { useNavigate } from "react-router-dom";

const MyProfile = () => {
   const [data, setData] = useState([]);
   const [role, setRole] = useState([]);
   const navigate = useNavigate();

   function redirectTimeout() {
      setTimeout(() => {
         navigate("/login");
      }, 0);
   }

   useEffect(() => {
      fetch(process.env.REACT_APP_BACKEND_URL + "/players/" + localStorage.getItem("id"))
         .then((response) => response.json())
         .then((data) => {
            setData(data)
            setRole(data.role.rolename.toLowerCase())
         })
         .catch((error) => {
            console.error("Error fetching data: ", error);
         });
   }, []);

   if (localStorage.getItem("token") == null) {
      return (
         <div>
            <Header />
            Prvo se prijavite
            {redirectTimeout()}
         </div>
      );
   } else {
      return (
         <div>
            <img className="home-page" src="/slika.jpg"></img>
            <Header />
            <div className="centeredCollumn">
               <h1 className="mainTextLeft">Moj profil</h1>
               <div className="my-player-info">
                  <div className="user-information">
                     <p className="mainTextLeft">
                        <b>Korisničko ime:</b> {data.username}
                     </p>
                     <p className="mainTextLeft">
                        <b>Ime:</b> {data.firstname}
                     </p>
                     <p className="mainTextLeft">
                        <b>Prezime:</b> {data.lastname}
                     </p>
                     <p className="mainTextLeft">
                        <b>E-mail adresa:</b> {data.email}
                     </p>
                     <p className="mainTextLeft">
                        <b>Rola:</b> {role}
                     </p>
                  </div>
                  
                  <br />
                  <div className="profile-buttons">
                     <div id="update-user" className="button-holder-pass" >
                        <button className = "button" type="button" onClick={() => {navigate("/updateprofile")} }>Izmjenite korisničke podatke</button>
                     </div>
                     <div id="change-password" className="button-holder-pass" >
                        <button className = "button" type="button" onClick={() => {navigate("/changepassword")} }>Izmjenite lozinku</button>
                     </div>
                     <div id="delete-user" className="button-holder-pass" >
                        <button className = "button" type="button" onClick={() => {navigate("/deleteuser")} }>Izbrišite korisnički račun</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      );
   }
};

export default MyProfile;
