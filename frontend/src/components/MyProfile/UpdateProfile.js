import React, { useState, useEffect } from "react";
import Header from "../Header/Header";
import { useNavigate } from "react-router-dom";

import { AiFillEyeInvisible } from "react-icons/ai";
import { AiFillEye } from "react-icons/ai";

import "./MyProfile.css"


const crypto = require("crypto");


const UpdateProfile = () => {
   const navigate = useNavigate();

   const [data, setData] = useState([]);
   const [error, setError] = React.useState("");
   const [passwordError, setPasswordError] = React.useState("");
   const [passwordRevealed, setPasswordRevealed] = React.useState(false);
   const [changed, setChanged] = React.useState(false);

   const EyeOpen = (
      <AiFillEye
        className="eye-open"
        onClick={() => setPasswordRevealed(!passwordRevealed)}
      />
    );
  
    const EyeClosed = (
      <AiFillEyeInvisible
        className="eye-closed"
        onClick={() => setPasswordRevealed(!passwordRevealed)}
      />
    );

    const [profileForm, setProfileForm] = React.useState({
      firstname: "",
      lastname: "",
      email: "",
      username: "",
      password: "",
      repeatPassword: ""
    });

    useEffect(() => {
      fetch(process.env.REACT_APP_BACKEND_URL + "/players")
        .then((response) => response.json())
        .then((data) => {
          for (var player of data) {
            if (player.username === localStorage.getItem("username")) {
              setProfileForm(player);
            }
          }
        })
        .catch((error) => {
          console.error("Error fetching data: ", error);
        });
    }, []);

    
    function onChange(event) {
      const { name, value } = event.target;
      setProfileForm((oldForm) => ({ ...oldForm, [name]: value }));
    }

    async function onSubmit(e) {
      e.preventDefault();
      setError("");

      setChanged(true);
  
      const body = JSON.stringify({
        firstname: profileForm.firstname,
        lastname: profileForm.lastname,
        username: profileForm.username,
        password: profileForm.password,
        email: profileForm.email,
        id: localStorage.getItem("id"),
        role:{
           id:localStorage.getItem("roleId"),
           rolename: localStorage.getItem("roleName")
        }
      });

      const body2 = JSON.stringify({
         username: profileForm.username,
         password: profileForm.password,
       });

      const options = {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer" + localStorage.getItem("token"),
        },
        body: body,
      };

      const options2 = {
         method: "POST",
         headers: {
           "Content-Type": "application/json",
         },
         body: body2,
       };

      let funkcija = async () => {
         const response = await fetch(process.env.REACT_APP_BACKEND_URL + "/players/" + localStorage.getItem("id"),
         options)

         const end = await fetch(process.env.REACT_APP_BACKEND_URL + "/login",
         options2)
         let data = await end.json();
         setData(data);
         console.log(data);
         localStorage.setItem("username", profileForm.username);
         localStorage.setItem("token", data.token);
         navigate("/myprofile")

      }

      funkcija();
    }



   return(
      <div>
        <img className="home-page" src="/slika.jpg"></img>
         <Header/>
         <div className="main-column">
          <form className="input-fields" onSubmit={onSubmit}>
             <div className="text-box-container">Izmijenite podatke koje želite: </div>
            <div className="text-box-container">
              <div>
                Ime: 
              </div>
              <input
                type="text"
                name="firstname"
                onChange={onChange}
                value={profileForm.firstname}
                minLength="2"              />
            </div>
            <div className="text-box-container">
              <div>
                Prezime: 
              </div>
              <input
                type="text"
                name="lastname"
                onChange={onChange}
                value={profileForm.lastname}
                minLength="2"
              />
            </div>
            <div className="text-box-container">
                <div>
                  E-mail adresa: 
                </div>
              <input
                type="text"
                name="email"
                onChange={onChange}
                value={profileForm.email}
                pattern="^[a-zA-Z]{2}[0-9]{5}@fer.hr$|^[a-zA-Z]{2,}[.][a-zA-Z]{2,}@fer.hr$"
              />
            </div>
            <div className="text-box-container">
                <div>
                  Korisničko ime: 
                </div>
              <input
                type="text"
                name="username"
                onChange={onChange}
                value={profileForm.username}
                minLength="5"
                maxLength="15"
              />
            </div>
            <div className="button-holder">
              <button id = "save" className = "button" type="submit">Spremi izmjene</button>
            </div>
            <div className="button-holder-pass" >
              <button className = "button" type="button" onClick={() => {navigate("/myprofile")} }>Odustani</button>
            </div>
          </form>
        </div>
        <div className="main-column">
        {error ? <p className="error">{error}</p> : null}
        {passwordError ? <p className="error">{error}</p> : null}
        </div>
      </div>
   )

}

export default UpdateProfile;