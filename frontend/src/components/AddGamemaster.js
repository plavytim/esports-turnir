import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Header from "./Header/Header";

function AddGamemaster() {
    const [data, setData] = React.useState("");
    const [player, setPlayer] = React.useState({
      id: "",
      firstname: "",
      lastname: "",
      email: "",
      username: "",
      password: "",
      role: {
        id: "",
        rolename: "",
      }
    });
    const [error, setError] = React.useState("");

    let gamemasterCandidates = "";
    const navigate = useNavigate();   

    useEffect(() => {
        fetch(process.env.REACT_APP_BACKEND_URL + "/players")
          .then((response) => response.json())
          .then((data) => {
            setData(data);
          })
          .catch((error) => {
            console.error("Error fetching data: ", error);
          });
      }, []);

    if (data !== "") {
        gamemasterCandidates = data
            .filter(function (player) {
            if (player.roleName === "GAMEMASTER") {
                return false;
            }
            return true;
            })
            .map((player) => (
                <option value={JSON.stringify(player)}>{player.username}</option>
            ));
    }

    function handleChange(event) {
      let json = JSON.parse(event.target.value);

      setPlayer({
        id: json.id,
        firstname: json.firstname,
        lastname: json.lastname,
        email: json.email,
        username: json.username,
        password: json.password,
        role: {
          id: 2,
          rolename: "GAMEMASTER",
        }
      })
    }

    async function onSubmit(e) {
        e.preventDefault();
        setError("");

        const body = JSON.stringify({
            id: player.id,
            firstname: player.firstname,
            lastname: player.lastname,
            email: player.email,
            username: player.username,
            password: player.password,
            role: player.role,
          });
      
          const options = {
            method: "PUT",
            headers: {
              "Content-Type": "application/json",
              Authorization: "Bearer " + localStorage.getItem("token"),
            },
            body: body,
          };
      
          let response = await fetch(
            process.env.REACT_APP_BACKEND_URL +
              "/players/" +
              player.id,
            options
          );
          let data = await response.json();
          if (response.status !== 200) {
            setError(data.message);
          } else {
            alert("Uspješno ste dodali gamemastera");
            navigate("/");
          }
    }

    return (
    <body>
        <div>
        <img className="home-page" src="/slika.jpg"></img>
        <Header />
        </div>
        <div className="centeredCollumn">
            <h1>Dodaj novog gamemastera</h1>
            <form className="add-gm" onSubmit={onSubmit} action={process.env.REACT_APP_BACKEND_URL + "/players/" + player.id}>
                <select name="playerUsername" onChange={handleChange} required>
                  <option value="" selected disabled hidden>
                    Odaberi korisnika
                  </option>
                  {gamemasterCandidates}
                </select>
                <p className="button-holder">
                    <button type="submit">Potvrdi</button>
                </p>
            </form>
        </div>
    </body>
    );
}

export default AddGamemaster;