import React from "react";
import { useNavigate } from "react-router-dom";
import Header from "./Header/Header";

function Logout() {
  const navigate = useNavigate();

  function redirectTimeout() {
    setTimeout(() => {
      navigate("/");
    }, 850);
  }

  localStorage.clear();
  return (
    <body>
      <img className="home-page" src="slika.jpg"></img>
      <Header />
      <div className="centeredCollumn">
        <p className="mainText">
          Uspješno ste odjavljeni. Vraćam vas na početnu stranicu...
        </p>
        {redirectTimeout()}
      </div>
    </body>
  );
}

export default Logout;
