import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import Header from "./Header/Header";
import { checkRole } from "./CheckRoleFunction";

function Tournaments() {
  const [data, setData] = useState("");
  const navigate = useNavigate();
  let tournamentData = "";

  useEffect(() => {
    fetch(process.env.REACT_APP_BACKEND_URL + "/tournaments")
      .then((response) => response.json())
      .then((data) => {
        setData(data);
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }, []);

  function createTournamentButton(role) {
    console.log(role);
    if (role === "GAMEMASTER"
        || role === "ADMIN") {
      return (
        <p className="button-holder">
          <a href="/createtournament">
            <button className="buttonLeft"type="submit">Stvori novi turnir</button>
          </a>
        </p>
      );
    }
  }

  if (data !== "") {
    tournamentData = data.map((element) => (
      <tr key={element.id}>
        <th>
          <Link className="crniLink" to={"/tournaments/" + element.id}>
            {element.tournamentName}
          </Link>
        </th>
        <th>{element.game.name}</th>
        <th>{Object(element.winnerTeam).teamName == null ? "-" : Object(element.winnerTeam).teamName}</th>
      </tr>
    ));
  }

  return (
    <body>
     <img className="home-page" src="slika.jpg"></img>
      <Header/>
      <div className="tournament">
        <div className="table">
          <table className="bigTable">
            <tbody>
              <tr className="bigTableFirstRow" key="POCETNIREDAK">
                <th>Ime</th>
                <th>Igra</th>
                <th>Pobjednik</th>
              </tr>
              {tournamentData}
            </tbody>
          </table>
        </div>
        <div className="button-tournament">
          {createTournamentButton(checkRole())}
        </div>
      </div>
    </body>
  );
}

export default Tournaments;
