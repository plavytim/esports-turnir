import React from "react";
import { Link, useNavigate } from "react-router-dom";
import Header from "../Header/Header";

import { AiFillEyeInvisible } from "react-icons/ai";
import { AiFillEye } from "react-icons/ai";

const crypto = require("crypto");

function Register() {
  const [registerForm, setRegisterForm] = React.useState({
    firstname: "",
    lastname: "",
    email: "",
    username: "",
    password: "",
    repeatPassword: "",
  });
  const [data, setData] = React.useState({});
  const [error, setError] = React.useState("");
  const [passwordError, setPasswordError] = React.useState("");
  const [passwordRevealed, setPasswordRevealed] = React.useState(false);
  const [submitButton, setSubmitButton] = React.useState({
    text: "Registriraj se",
    disabled: false, 
  });
  const navigate = useNavigate();

  function redirectTimeout() {
    setTimeout(() => {
      navigate("/");
    }, 0);
  }

  const EyeOpen = (
    <AiFillEye
      className="eye-open"
      onClick={() => setPasswordRevealed(!passwordRevealed)}
    />
  );

  const EyeClosed = (
    <AiFillEyeInvisible
      className="eye-closed"
      onClick={() => setPasswordRevealed(!passwordRevealed)}
    />
  );

  function onChange(event) {
    const { name, value } = event.target;
    setRegisterForm((oldForm) => ({ ...oldForm, [name]: value }));
  }

  function onBlur(event) {
    setPasswordError("");

    if (registerForm.password !== registerForm.repeatPassword) {
      setPasswordError("Lozinke nisu iste");
    } else {
      setPasswordError("");
    }
  }

  async function onSubmit(e) {
    e.preventDefault();
    setError("");
    setSubmitButton({
      text: "Učitavanje...",
      disabled: true
    });

    const hashedPassword = crypto
      .createHash("sha1")
      .update(registerForm.password)
      .digest("hex");

    const body = JSON.stringify({
      firstname: registerForm.firstname,
      lastname: registerForm.lastname,
      username: registerForm.username,
      password: hashedPassword,
      email: registerForm.email,
    });
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: body,
    };
    let response = await fetch(
      process.env.REACT_APP_BACKEND_URL + "/register",
      options
    );
    let data = await response.json();
    setData(data);

    if (response.status !== 201) {
      setError(data.message);
      setSubmitButton({
        text: "Registriraj se",
        disabled: false
      });
    } else {
      localStorage.setItem("token", data.token);
      localStorage.setItem("username", data.username);
      localStorage.setItem("id", data.id);
      localStorage.setItem("roleId", data.role.id);
      localStorage.setItem("roleName", data.role.rolename);
      navigate("/");
    }
  }

  if (localStorage.getItem("token") == null) {
    return (
      <body>
        <img className="home-page" src="/slika.jpg"></img>
        <Header />
        <div className="centeredCollumn">
          <h1>Registracija</h1>
          <form className="login-form" onSubmit={onSubmit}>
          <div className="text-box-container">
              <div>
                Ime: 
              </div>
              <input
                type="text"
                name="firstname"
                onChange={onChange}
                value={registerForm.firstname}
                placeholder="Ime"
                minLength="2"
                required
              />
            </div>
            <div className="text-box-container">
              <div>
                Prezime: 
              </div>
              <input
                type="text"
                name="lastname"
                onChange={onChange}
                value={registerForm.lastname}
                placeholder="Prezime"
                minLength="2"
                required
              />
            </div>
            <div className="text-box-container">
                <div>
                  E-mail adresa: 
                </div>
              <input
                type="text"
                name="email"
                onChange={onChange}
                value={registerForm.email}
                placeholder="FER e-mail adresa"
                pattern="^.*@fer.hr$"
                required
              />
            </div>
            <div className="text-box-container">
                <div>
                  Korisničko ime: 
                </div>
              <input
                type="text"
                name="username"
                onChange={onChange}
                value={registerForm.username}
                placeholder="Korisničko ime"
                minLength="5"
                maxLength="15"
                required
              />
            </div>
            <div className="text-box-container-special">
            <div>
                  Unesite svoju lozinku: 
               </div>
            <div className="input-container">
                <input
                  className="input-eye"
                  type={passwordRevealed ? "text" : "password"}
                  name="password"
                  onChange={onChange}
                  onBlur={onBlur}
                  value={registerForm.password}
                  placeholder="Lozinka"
                  required
                ></input>
                <div
                     title={passwordRevealed ? "Sakrij lozinku" : "Prikaži lozinku"}
                     className="eye"
                  >
                  {passwordRevealed ? EyeOpen : EyeClosed}
                  </div>
              </div>
            </div>
            <div className="passwordContainer">
            <div className="text-box-container">
               <div>
                  Ponovno unesite svoju novu lozinku: 
               </div>
                <input
                  type={passwordRevealed ? "text" : "password"}
                  name="repeatPassword"
                  onChange={onChange}
                  onBlur={onBlur}
                  value={registerForm.repeatPassword}
                  placeholder="Ponovite lozinku"
                  required
                ></input>
              </div>
            </div>
            <p className="button-holder">
              <button id="form-button" type="submit" disabled={submitButton.disabled}>{submitButton.text}</button>
            </p>
          </form>
        </div>
        <div className="centeredCollumn">
          {error ? <p className="error">{error}</p> : null}
          {passwordError ? <p className="error">{passwordError}</p>:null}
          
        </div>
      </body>
    );
  } else {
    return (
      <div>
        <Header />
        Već ste prijavljeni
        {redirectTimeout()}
      </div>
    );
  }
}

export default Register;
