import React, { useRef, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { checkRole } from "./CheckRoleFunction";
import Header from "./Header/Header";

function Team() {
  const [data, setData] = React.useState("");
  const [players, setPlayers] = React.useState([]);
  const [requests, setRequests] = React.useState([]);
  const [playerUsernameForm, setPlayerUsernameForm] = React.useState({
    playerUsername: "",
  });
  const [showForm, setShowForm] = React.useState(false);
  const [error, setError] = React.useState("");
  const [message, setMessage] = React.useState("");

  let teamId = useRef(0);
  let playerData = "";
  const navigate = useNavigate();

  const [update, setUpdate] = React.useState(0);

  const sendJoinMessage = () => {
    var messagePrompt = prompt("Molim unesite poruku za tim!");
    setMessage(messagePrompt);
    if (messagePrompt == null || messagePrompt == "") {
      setError("Poruka ne smije biti ostavljena prazna.");
    } else if (messagePrompt.length > 250) {
      setError(
        "Poruka je preduga. Duljina poruke mora biti manja od 300 znakova."
      );
    } else {
      postJoinMessage(messagePrompt);
    }
  };

  async function postJoinMessage(message) {
    var myURL = window.location.href;
    var myURLLista = myURL.split("/");
    teamId = myURLLista[myURLLista.length - 1];

    const body = JSON.stringify({
      playerUsername: localStorage.getItem("username"),
      requestMessage: message,
    });

    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
      body: body,
    };

    let response = await fetch(
      process.env.REACT_APP_BACKEND_URL + "/teams/" + teamId + "/requests",
      options
    );

    let data = await response.json();

    if (response.status !== 200) {
      setError(data.message);
    } else {
      navigate("/teams");
    }
  }

  function onClick(event) {
    setShowForm(!showForm);
  }

  function setPlayerUsername(event) {
    playerUsernameForm.playerUsername = event.target.value;
  }

  function renderForm() {
    const leadCandidates = players
      .filter(function (player) {
        if (player.username === data.leadPlayer.username) {
          return false;
        }
        return true;
      })
      .map((player) => (
        <option value={player.username}>{player.username}</option>
      ));

    return (
      <div>
        <form className="leader-change"
          onSubmit={onSubmit}
          action={
            process.env.REACT_APP_BACKEND_URL +
            "/teams/" +
            teamId +
            "/leadPlayer"
          }
        >
          <label>Odaberite novog voditelja:</label>
          <select name="playerUsername" onBlur={setPlayerUsername} required>
            <option value="" selected disabled hidden>
              Odaberi korisnika
            </option>
            {leadCandidates}
          </select>
          <p className="button-holder">
            <button type="submit">Potvrdi</button>
          </p>
        </form>
      </div>
    );
  }

  async function onSubmit(e) {
    e.preventDefault();
    setError("");

    const body = JSON.stringify({
      username: playerUsernameForm.playerUsername,
    });

    const options = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
      body: body,
    };

    let response = await fetch(
      process.env.REACT_APP_BACKEND_URL +
      "/teams/" +
      teamId.current +
      "/leadPlayer",
      options
    );
    let data = await response.json();
    if (response.status !== 200) {
      setError(data.message);
    } else {
      alert("Uspješno ste promijenili voditelja tima");
      navigate("/teams");
    }
  }

  useEffect(() => {
    var myURL = window.location.href;
    var myURLLista = myURL.split("/");
    teamId.current = myURLLista[myURLLista.length - 1];
    var fetchURL =
      process.env.REACT_APP_BACKEND_URL + "/teams/" + teamId.current;

    fetch(fetchURL)
      .then((response) => response.json())
      .then((data) => {
        setData(data);
        setPlayers(data.players);
        setRequests(data.requests);
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }, [update]);

  if (players != "") {
    playerData = players.map((player) => (
      <li key={player.id}>{player.username}</li>
    ));
  }

  function changeLeader(role) {
    if (role === "ADMIN" || localStorage.getItem("username") == Object(data.leadPlayer).username) {
      return (
        <button className="buttonLeft" onClick={onClick}>
          Promijeni voditelja tima
        </button>
      );
    }
  }

  function joinTeamButton() {
    var available = 0;

    if (localStorage.getItem("token") == null) {
      available = 0;
    } else if (players.length >= 5) {
      available = 0;
    } else {
      var nasaoIme = 0;
      for (let i = 0; i < players.length; i++) {
        console.log("PROVJERA PLAYERS");
        console.log(players);
        if (localStorage.getItem("username") == players[i].username) {
          nasaoIme = 1;
          break;
        }
      }
      if (nasaoIme) {
        available = 0;
      } else {
        available = 1;
      }
    }

    if (available) {
      return (
        <button className="buttonLeft" onClick={() => sendJoinMessage()}>
          Pridruži se timu
        </button>
      );
    } else {
      return (
        <button className="buttonLeft" disabled>
          Pridruživanje nije moguće
        </button>
      );
    }
  }

  function changeTeamData() {
    if (localStorage.getItem("username") == Object(data.leadPlayer).username) {
      return (
        <button
          className="buttonLeft"
          onClick={() => {
            navigate("/updateteam/" + teamId.current);
          }}
        >
          Promijeni timske podatke
        </button>
      );
    }
  }

  function leaveTeam() {
    localStorage.setItem("leadId", Object(data.leadPlayer).id);
    if (players !== "") {
      if (
        players.findIndex(
          (player) => player.id == localStorage.getItem("id")
        ) !== -1
      ) {
        localStorage.setItem("teamId", data.id);
        return (
          <div>
            <button
              className="buttonLeft"
              onClick={() => {
                navigate("/leaveteam");
              }}
            >
              Izađi iz tima
            </button>
          </div>
        );
      }
    }
  }

  async function sendConfirmRequest(requestID) {
    console.log("RADIM CONFIRM REQUEST");
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };

    let response = await fetch(
      process.env.REACT_APP_BACKEND_URL +
      "/teams/" +
      teamId.current +
      "/requests/" +
      requestID,
      options
    );
    let data = await response.json();
    if (response.status !== 200) {
      console.log(data.message);
    } else {
      alert("Uspješno ste prihvatili zahtjev!");
      setUpdate(1);
    }
  }

  async function sendRejectRequest(requestID) {
    console.log("RADIM REJECT REQUEST");
    const options = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };

    let response = await fetch(
      process.env.REACT_APP_BACKEND_URL +
      "/teams/" +
      teamId.current +
      "/requests/" +
      requestID,
      options
    );
    let data = await response.json();
    if (response.status !== 200) {
      console.log(data.message);
      setError(data.message);
    } else {
      alert("Uspješno ste odbili zahtjev!");
      setUpdate(1);
    }
  }
  const generateRequests = (role) => {
    if (localStorage.getItem("username") == Object(data.leadPlayer).username) {
      return (
        <div className="text-info-player-requests">
          <p>
            <b>Zahtjevi za ulazak u tim </b>
          </p>
          {requests.map((element) => (
            <div className="player-requests" key={element.id}>
              <p>Zahtjev od: {Object(element.player).username}</p>
              <p>Poruka: {element.requestMessage}</p>

              <button
                className="buttonLeft"
                onClick={() => sendConfirmRequest(element.id)}
              >
                Potvrdi zahtjev
              </button>{" "}
              <button
                className="buttonLeft"
                onClick={() => sendRejectRequest(element.id)}
              >
                Odbij zahtjev
              </button>{" "}
            </div>
          ))}
        </div>
      );
    }
  };

  return (
    <body>
      <img className="home-page" src="/slika.jpg"></img>
      <div>
        <Header />
        <div className="centeredCollumn">
          <h1 className="mainTextLeft">Podaci o timu {data.teamName}</h1>
          <div className="all-team-comp">
          <div className="all-team-info">
          <div className="team-information">
            <p className="mainTextLeft">
              <b>Voditelj/ica tima:</b> {Object(data.leadPlayer).username}
            </p>
            <p className="mainTextLeft">
              <b>Igra:</b> {Object(data.game).name}
            </p>
            <p className="mainTextLeft">
              <b>Broj pobjeda:</b> {data.numberOfWins}
            </p>
            <p className="mainTextLeft">
              <b>Broj odigranih mečeva:</b> {data.numberOfPlayed}
            </p>
            <p className="mainTextLeft">
              <b>Broj odigranih turnira:</b> {data.numberOfTournaments}
            </p>
          </div>
          <div className="team-players">
            <p className="mainTextLeft">
              <b>Igrači</b>
            </p>
            <ul>{playerData}</ul>
          </div>
          </div>
          <div className="all-team-buttons">
          {joinTeamButton()}
          {changeLeader(checkRole())}
          {showForm ? renderForm() : null}
          {changeTeamData()}
          {leaveTeam()}
          {generateRequests()}
          </div>
          </div>
          {error ? <p className="error">{error}</p> : null}
        </div>
      </div>
    </body >
  );
}

export default Team;
