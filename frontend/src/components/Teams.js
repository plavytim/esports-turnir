import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import Header from "./Header/Header";

function Teams() {
  const [data, setData] = useState("");
  let teamData = "";

  useEffect(() => {
    fetch(process.env.REACT_APP_BACKEND_URL + "/teams")
      .then((response) => response.json())
      .then((data) => {
        setData(data);
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }, []);

  if (data !== "") {
    teamData = data.map((element) => (
      <tr key={element.id}>
        <th>
          <Link className="crniLink" to={"/teams/" + element.id}>
            {element.teamName}
          </Link>
        </th>
        <th>{element.game.name}</th>
        <th>{element.leadPlayer.username}</th>
      </tr>
    ));
  }

  return (
    <body>
      <img className="home-page" src="/slika.jpg"></img>
      <div>
        <Header/>
      </div>
      <div>
        <table className="bigTable">
          <tbody>
            <tr className="bigTableFirstRow" key="POCETNIREDAK">
              <th>Ime</th>
              <th>Igra</th>
              <th>Voditelj</th>
            </tr>
            {teamData}
          </tbody>
        </table>
      </div>
    </body>
  );
}

export default Teams;
