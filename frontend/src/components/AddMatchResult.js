import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import moment from "moment";
import Header from "./Header/Header";

function AddMatchResult() {
  const idObject = useParams();
  const tournamentId = idObject.tournamentid;
  const matchId = idObject.matchid;
  const [matchData, setMatchData] = useState("");
  const [tournamentData, setTournamentData] = useState("");
  const [error, setError] = React.useState("");
  const navigate = useNavigate();

  useEffect(() => {
    fetch(process.env.REACT_APP_BACKEND_URL + "/tournaments/" + tournamentId)
      .then((response) => response.json())
      .then((data) => {
        setTournamentData(data);
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }, []);
  useEffect(() => {
    fetch(process.env.REACT_APP_BACKEND_URL + "/matches/" + matchId)
      .then((response) => response.json())
      .then((data) => {
        setMatchData(data);
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }, []);

  async function onSubmit(e) {
    e.preventDefault();
    setError("");

    const body = JSON.stringify({
      id: matchData.id,
      tournament: tournamentData,
      beginning: matchData.beginning,
      team1: matchData.team1,
      team2: matchData.team2,
      winner: matchData.winnerTeamId,
    });

    const options = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
      body: body,
    };
    let response = await fetch(
      process.env.REACT_APP_BACKEND_URL + "/matches/" + matchId,
      options
    );
    let data = await response.json();
    console.log(data)
    if (response.status !== 200) {
      setError(data.message);
    } else {
      alert("Uspješno ste dodali rezultat meča");
      navigate("/match/" + tournamentId + "/" + matchId);
    }
  }

  function handleChange(event) {
    matchData.winnerTeamId = event.target.value;
  }


    if (matchData != "") {
        return (
            <body>
                <div>
                <img className="home-page" src="/slika.jpg"></img>
                    <Header/>
                </div>
                <div className="centeredCollumn">
                    <br />
                    <form
                    onSubmit={onSubmit}
                    action={
                        process.env.REACT_APP_BACKEND_URL +
                        "/matches/" +
                        matchId
                    }
                    >
                        <label>Odaberite pobjednika meča:</label>
                        <select name="matchWinningTeam" onChange={handleChange} required>
                            <option value="" selected disabled hidden>
                            Odaberi tim
                            </option>
                            <option value={matchData.team1.id}>{matchData.team1.teamName}</option>
                            <option value={matchData.team2.id}>{matchData.team2.teamName}</option>
                        </select>
                        <p className="button-holder">
                            <button type="submit">Potvrdi</button>
                        </p>
                        <div className="centeredCollumn">
                        <button className = "buttonLeft" type="button" onClick={() => {navigate("/match/" + tournamentId + "/" + matchId)} }>Odustani</button>
                        </div>
                    </form>
                </div>
            </body>     
        );
    } return null;
}

export default AddMatchResult;
