import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import Header from "./Header/Header";

function Players() {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch(process.env.REACT_APP_BACKEND_URL + "/players")
      .then((response) => response.json())
      .then((data) => {
        setData(data);
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }, []);

  return (
    <body>
      <img className="home-page" src="/slika.jpg"></img>
      <Header/>
      <table className="bigTable">
        <tbody>
          <tr className="bigTableFirstRow" key="POCETNIREDAK">
            <th>Korisničko ime</th>
            <th>Ime</th>
            <th>Prezime</th>
          </tr>
          {data.map((element) => (
            <tr key={element.id}>
              <th>
                <Link className="crniLink" to={"/players/" + element.id}>
                  {element.username}
                </Link>
              </th>
              <th>{element.firstname}</th>
              <th>{element.lastname}</th>
            </tr>
          ))}
        </tbody>
      </table>
    </body>
  );
}

export default Players;
