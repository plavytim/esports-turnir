import React, { useEffect } from "react";
import Header from "./Header/Header";
import { useNavigate, useParams } from "react-router-dom";

function RemoveMatch() {
    const [matchData, setMatchData] = React.useState("");
    const [error, setError] = React.useState("");

    const idObject = useParams();
    const tournamentId = idObject.tournamentid;
    const matchId = idObject.matchid;  
    const navigate = useNavigate();

    useEffect(() => {
        fetch(process.env.REACT_APP_BACKEND_URL + "/matches")
          .then((response) => response.json())
          .then((data) => {
            setMatchData(data);
          })
          .catch((error) => {
            console.error("Error fetching data: ", error);
          });
    }, []);

    console.log(matchData);

    async function onSubmit(e) {
        e.preventDefault("");
        setError("");
    
        const options = {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + localStorage.getItem("token"),
          }
        };

        let response = await fetch(
          process.env.REACT_APP_BACKEND_URL + "/matches/" + matchId,
          options
        );
    
        let data = await response.json();
    
        if (response.status !== 200) {
          setError(data.message);
        } else {
          alert("Uspješno ste uklonili meč");
          navigate("/tournaments");
        }
    }

    function onReset() {
      navigate("/match/" + tournamentId + "/" + matchId);
    }

    return (
        <body>
          <img className="home-page" src="/slika.jpg"></img>
            <div>
                <Header />
            </div>
            <div className="centeredCollumn">
                <h1>
                    Jeste li sigurni da želite obrisati meč?
                </h1> 
                <button className="buttonLeft" id="potvrdi" onClick={onSubmit}>
                    Potvrdi
                </button> 
                <button className="buttonLeft" id="odustani" onClick={onReset}>
                    Odustani
                </button>
            </div>            
        </body>
    );
}

export default RemoveMatch;
