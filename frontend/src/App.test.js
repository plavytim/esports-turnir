import CreateDeleteTournamentMatch from "../tests/CreateDeleteTournamentMatch";
import LoginAsAdmin from "../tests/LoginAsAdmin";
import RegisterUpdateDeleteUser from "../tests/RegisterUpdateDeleteUser";
import RegisterUserWrongEmailFormat from "../tests/RegisterUserWrongEmailFormat";

/*
Ispitivanje sustava proveli smo pomoću Selenium testova. Selenium testove implementirali smo u react-scripts okvir za bolju preglednost rezultata testova.
Svi testovi se pokreću naredbom `npm test a` te bi svi trebali završiti uspješno.
*/ 

jest.setTimeout(20000);

var TEST_URL = "http://localhost:3000"

test("Login as admin", async () => {
  expect(await LoginAsAdmin(TEST_URL)).toEqual(true);
});

test("Register, update and delete user", async () => {
  expect(await RegisterUpdateDeleteUser(TEST_URL)).toEqual(true);
});

test("Try registering user with wrong email format", async () => {
  expect(await RegisterUserWrongEmailFormat(TEST_URL)).toEqual(true);
});

test("Create and delete tournament and match", async () => {
  expect(await CreateDeleteTournamentMatch(TEST_URL)).toEqual(true);
});