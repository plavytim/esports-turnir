package hr.fer.plavy.tim.esports_turnir;

import hr.fer.plavy.tim.esports_turnir.db.DataInitializer;
import hr.fer.plavy.tim.esports_turnir.exception.EntityMissingException;
import hr.fer.plavy.tim.esports_turnir.model.Team;
import hr.fer.plavy.tim.esports_turnir.service.TeamService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TeamServiceTests {

    @Autowired
    private TeamService teamService;

    @Autowired
    private DataInitializer dataInitializer;

    private static List<Team> teams;

    @BeforeAll
    void init(){
        teams = dataInitializer.getTeams();
    }

    @Test
    @DisplayName("Test for getting all teams from database")
    void listAllPlayersTest(){
        List<Team> teamList = teamService.listAll();
        for(int i = 0; i < teamList.size(); i++){
            assertEquals(teams.get(i), teamList.get(i));
        }
    }

    @Test
    @DisplayName("Test for getting team with specific id")
    void fetchTest(){
        long teamId = teams.get(0).getId();
        Team team = teamService.fetch(teamId);
        assertEquals(teams.get(0), team);
    }

    @Test
    @DisplayName("Test for getting non-existing team")
    void fetchExceptionTest(){
        long teamId = 0;
        EntityMissingException thrown = assertThrows(EntityMissingException.class,
                () -> teamService.fetch(teamId)
        );

        assertEquals("Entity with reference " + teamId + " of " + Team.class + " not found.", thrown.getMessage());
    }
}
