package hr.fer.plavy.tim.esports_turnir;

import hr.fer.plavy.tim.esports_turnir.db.DataInitializer;
import hr.fer.plavy.tim.esports_turnir.exception.EntityMissingException;
import hr.fer.plavy.tim.esports_turnir.model.Game;
import hr.fer.plavy.tim.esports_turnir.service.GameService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GameServiceTests {

    @Autowired
    private GameService gameService;

    @Autowired
    private DataInitializer dataInitializer;

    private static List<Game> games;

    @BeforeAll
    void init(){
        games = dataInitializer.getGames();
    }

    @Test
    @DisplayName("Test for getting all games from database")
    void listAllGamesTest(){
        List<Game> gameList = gameService.listAll();
        for(int i = 0; i < gameList.size(); i++){
            assertEquals(games.get(i), gameList.get(i));
        }
    }

    @Test
    @DisplayName("Test for getting game with specific id")
    void fetchTest(){
        long gameId = games.get(0).getId();
        Game game = gameService.fetch(gameId);
        assertEquals(games.get(0), game);
    }

    @Test
    @DisplayName("Test for getting non-existing game")
    void fetchExceptionTest(){
        long gameId = 0;
        EntityMissingException thrown = assertThrows(EntityMissingException.class,
                                () -> gameService.fetch(gameId)
        );

        assertEquals("Entity with reference " + gameId + " of " + Game.class + " not found.", thrown.getMessage());
    }
}
