package hr.fer.plavy.tim.esports_turnir;

import hr.fer.plavy.tim.esports_turnir.db.DataInitializer;
import hr.fer.plavy.tim.esports_turnir.exception.EntityMissingException;
import hr.fer.plavy.tim.esports_turnir.model.Player;
import hr.fer.plavy.tim.esports_turnir.model.Tournament;
import hr.fer.plavy.tim.esports_turnir.service.TournamentService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TournamentServiceTests {

    @Autowired
    private TournamentService tournamentService;

    @Autowired
    private DataInitializer dataInitializer;

    private static List<Tournament> tournaments;

    @BeforeAll
    void init(){
        tournaments = dataInitializer.getTournaments();
    }

    @Test
    @DisplayName("Test for getting all tournaments from database")
    void listAllPlayersTest(){
        List<Tournament> tournamentList = tournamentService.listAll();
        for(int i = 0; i < tournamentList.size(); i++){
            assertEquals(tournaments.get(i), tournamentList.get(i));
        }
    }

    @Test
    @DisplayName("Test for getting tournament with specific id")
    void fetchTest(){
        long tournamentId = tournaments.get(0).getId();
        Tournament tournament = tournamentService.fetch(tournamentId);
        assertEquals(tournaments.get(0), tournament);
    }

    @Test
    @DisplayName("Test for getting non-existing tournament")
    void fetchExceptionTest(){
        long tournamentId = 0;
        EntityMissingException thrown = assertThrows(EntityMissingException.class,
                () -> tournamentService.fetch(tournamentId)
        );

        assertEquals("Entity with reference " + tournamentId + " of " + Tournament.class + " not found.", thrown.getMessage());
    }
}
