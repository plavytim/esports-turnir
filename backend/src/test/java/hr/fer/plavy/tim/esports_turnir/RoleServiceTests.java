package hr.fer.plavy.tim.esports_turnir;

import hr.fer.plavy.tim.esports_turnir.db.DataInitializer;
import hr.fer.plavy.tim.esports_turnir.enums.RoleEnum;
import hr.fer.plavy.tim.esports_turnir.exception.EntityMissingException;
import hr.fer.plavy.tim.esports_turnir.model.Role;
import hr.fer.plavy.tim.esports_turnir.service.RoleService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class RoleServiceTests {

    @Autowired
    private RoleService roleService;

    @Autowired
    private DataInitializer dataInitializer;

    private static List<Role> roles;

    @BeforeAll
    void init(){
        roles = dataInitializer.getRoles();
    }

    @Test
    @DisplayName("Test for getting role with specific name")
    void fetchByUsernameTest(){
        String rolename = RoleEnum.GAMEMASTER.toString();
        Role role = roleService.fetchByRolename(rolename);
        assertEquals(roles.get(1), role);
    }

    @Test
    @DisplayName("Test for getting non-existing player")
    void fetchExceptionTest(){
        String rolename = "";
        EntityMissingException thrown = assertThrows(EntityMissingException.class,
                () -> roleService.fetchByRolename(rolename)
        );

        assertEquals("Entity with reference " + rolename + " of " + Role.class + " not found.", thrown.getMessage());
    }
}
