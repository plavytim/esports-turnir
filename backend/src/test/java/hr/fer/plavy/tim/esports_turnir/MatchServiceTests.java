package hr.fer.plavy.tim.esports_turnir;

import hr.fer.plavy.tim.esports_turnir.db.DataInitializer;
import hr.fer.plavy.tim.esports_turnir.exception.EntityMissingException;
import hr.fer.plavy.tim.esports_turnir.model.Match;
import hr.fer.plavy.tim.esports_turnir.service.MatchService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MatchServiceTests {

    @Autowired
    private MatchService matchService;

    @Autowired
    private DataInitializer dataInitializer;

    private static List<Match> matches;

    @BeforeAll
    void init(){
        matches = dataInitializer.getMatches();
    }

    @Test
    @DisplayName("Test for getting all matches from database")
    void listAllMatchesTest(){
        List<Match> matchList = matchService.listAll();
        for(int i = 0; i < matchList.size(); i++){
            assertEquals(matches.get(i), matchList.get(i));
        }
    }

    @Test
    @DisplayName("Test for getting match with specific id")
    void fetchTest(){
        long matchId = matches.get(0).getId();
        Match match = matchService.fetch(matchId);
        assertEquals(matches.get(0), match);
    }

    @Test
    @DisplayName("Test for getting non-existing match")
    void fetchExceptionTest(){
        long matchId = 0;
        EntityMissingException thrown = assertThrows(EntityMissingException.class,
                () -> matchService.fetch(matchId)
        );

        assertEquals("Entity with reference " + matchId + " of " + Match.class + " not found.", thrown.getMessage());
    }
}
