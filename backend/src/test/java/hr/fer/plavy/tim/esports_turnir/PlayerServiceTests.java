package hr.fer.plavy.tim.esports_turnir;

import hr.fer.plavy.tim.esports_turnir.db.DataInitializer;
import hr.fer.plavy.tim.esports_turnir.exception.EntityMissingException;
import hr.fer.plavy.tim.esports_turnir.model.Player;
import hr.fer.plavy.tim.esports_turnir.service.PlayerService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PlayerServiceTests {

    @Autowired
    private PlayerService playerService;

    @Autowired
    private DataInitializer dataInitializer;

    private static List<Player> players;

    @BeforeAll
    void init(){
        List<Player> players31 = dataInitializer.getPlayers31();
        System.out.println(players31.get(0));
        List<Player> players32 = dataInitializer.getPlayers32();
        List<Player> players21 = dataInitializer.getPlayers21();
        List<Player> players22 = dataInitializer.getPlayers22();
        List<Player> players23 = dataInitializer.getPlayers23();
        players = Stream.of(players31, players32, players21, players22, players23)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    @Test
    @DisplayName("Test for getting all players from database")
    void listAllPlayersTest(){
        List<Player> playerList = playerService.listAll();
        for(int i = 0; i < players.size(); i++){
            assertEquals(players.get(i), playerList.get(i));
        }
    }

    @Test
    @DisplayName("Test for getting player with specific id")
    void fetchTest(){
        long playerId = players.get(0).getId();
        Player player = playerService.fetch(playerId);
        assertEquals(players.get(0), player);
    }

    @Test
    @DisplayName("Test for getting non-existing player")
    void fetchExceptionTest(){
        long playerId = 0;
        EntityMissingException thrown = assertThrows(EntityMissingException.class,
                () -> playerService.fetch(playerId)
        );

        assertEquals("Entity with reference " + playerId + " of " + Player.class + " not found.", thrown.getMessage());
    }
}
