package hr.fer.plavy.tim.esports_turnir.service;

import hr.fer.plavy.tim.esports_turnir.dto.AddMatchToTournamentDTO;
import hr.fer.plavy.tim.esports_turnir.dto.CreateTournamentDTO;
import hr.fer.plavy.tim.esports_turnir.dto.UpdateTournamentDTO;
import hr.fer.plavy.tim.esports_turnir.model.Tournament;

import java.util.List;
import java.util.Optional;

public interface TournamentService {

    List<Tournament> listAll();

    Optional<Tournament> findById(long tournamentId);

    Tournament fetch(long tournamentId);

    Optional<Tournament> findByTournamentName(String tournamentName);

    Tournament fetchByTournamentName(String tournamentName);

    Tournament createTournament(CreateTournamentDTO tournamentDTO);

    Tournament updateTournament(UpdateTournamentDTO updateTournamentDTO);

    Tournament deleteTournament(long tournamentId);

    Tournament addMatchToTournament(AddMatchToTournamentDTO addMatchToTournamentDTO);
}
