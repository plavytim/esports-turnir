package hr.fer.plavy.tim.esports_turnir.service.impl;

import hr.fer.plavy.tim.esports_turnir.dto.AddMatchToTournamentDTO;
import hr.fer.plavy.tim.esports_turnir.dto.CreateTournamentDTO;
import hr.fer.plavy.tim.esports_turnir.dto.UpdateTournamentDTO;
import hr.fer.plavy.tim.esports_turnir.exception.EntityMissingException;
import hr.fer.plavy.tim.esports_turnir.exception.RequestDeniedException;
import hr.fer.plavy.tim.esports_turnir.mapper.TournamentMapper;
import hr.fer.plavy.tim.esports_turnir.model.*;
import hr.fer.plavy.tim.esports_turnir.repository.TournamentRepository;
import hr.fer.plavy.tim.esports_turnir.service.GameService;
import hr.fer.plavy.tim.esports_turnir.service.MatchService;
import hr.fer.plavy.tim.esports_turnir.service.TeamService;
import hr.fer.plavy.tim.esports_turnir.service.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class TournamentServiceImpl implements TournamentService {

    private final TournamentRepository tournamentRepository;
    private final GameService gameService;
    private final TeamService teamService;
    private final TournamentMapper tournamentMapper;


    @Autowired
    public TournamentServiceImpl(TournamentRepository tournamentRepository, GameService gameService, TeamService teamService, TournamentMapper tournamentMapper) {
        this.tournamentRepository = tournamentRepository;
        this.gameService = gameService;
        this.teamService = teamService;
        this.tournamentMapper = tournamentMapper;
    }

    @Override
    public List<Tournament> listAll() {
        return tournamentRepository.findAll();
    }

    @Override
    public Optional<Tournament> findById(long tournamentId) {
        return tournamentRepository.findById(tournamentId);
    }

    @Override
    public Tournament fetch(long tournamentId) {
        return findById(tournamentId).orElseThrow(
                () -> new EntityMissingException(Tournament.class, tournamentId)
        );
    }

    @Override
    public Optional<Tournament> findByTournamentName(String tournamentName) {
        return tournamentRepository.findByTournamentName(tournamentName);
    }

    @Override
    public Tournament fetchByTournamentName(String tournamentName) {
        return findByTournamentName(tournamentName).orElseThrow(
                () -> new EntityMissingException(Tournament.class, tournamentName)
        );
    }

    @Override
    public Tournament createTournament(CreateTournamentDTO createTournamentDTO) {
        validateCreateDTO(createTournamentDTO);

        String tournamentName = createTournamentDTO.getTournamentName();
        String gameName = createTournamentDTO.getGameName();
        Game game = gameService.fetchByName(gameName);

        if (tournamentRepository.existsTournamentByTournamentName(createTournamentDTO.getTournamentName())) {
            throw new RequestDeniedException(
                    "Tournament with this name: " + createTournamentDTO.getTournamentName() + " already exists"
            );
        }

        Tournament createdTournament = new Tournament(tournamentName, game);
        return tournamentRepository.save(createdTournament);
    }

    /*TODO
     *  mijenjanje imena turnira
     *   mijenjanje pobjednika -- paziti da je jedan od timova koji je igrao
     */

    @Override
    public Tournament updateTournament(UpdateTournamentDTO updateTournamentDTO) {
        validateUpdateDTO(updateTournamentDTO);

        long tournamentId = updateTournamentDTO.getId();
        Tournament tournament = fetch(tournamentId);
        Game game = tournament.getGame();
        if (!game.equals(teamService.fetchByTeamName(updateTournamentDTO.getWinnerTeamName()).getGame())){
            throw new IllegalArgumentException(
                    "Winner Team:" + updateTournamentDTO.getWinnerTeamName() + "does not play" + game.getName()
            );
        }

        Tournament newTournament = tournamentMapper.updateTournamentFromTournamentDTO(tournament, updateTournamentDTO);

        return tournamentRepository.save(newTournament);
    }

    @Override
    public Tournament deleteTournament(long tournamentId) {
        Tournament tournament = fetch(tournamentId);
        tournamentRepository.delete(tournament);
        return tournament;
    }

    public Tournament addMatchToTournament(AddMatchToTournamentDTO addMatchToTournamentDTO){
        validateAddMatchToTournament(addMatchToTournamentDTO);

        String tournamentName = addMatchToTournamentDTO.getTournamentName();
        Tournament tournament = fetchByTournamentName(tournamentName);

        Match match = addMatchToTournamentDTO.getMatch();

        boolean matchAlreadyInTournament = tournamentRepository.matchAlreadyInTournament(match);

        if (matchAlreadyInTournament){
            throw new RequestDeniedException(
                    "Match between " + match.getTeam1().getTeamName() + " and " +
                            match.getTeam2().getTeamName() + " already exists"
            );
        }

        tournament.addMatch(match);

        return tournamentRepository.save(tournament);
    }

    private void validate(Tournament tournament) {

        Assert.notNull(tournament, "Tournament object must be given");

        String tournamentName = tournament.getTournamentName();
        Assert.hasText(tournamentName, "Tournament name must be given");

    }

    private void validateCreateDTO(CreateTournamentDTO createTournamentDTO) {
        String tournamentName = createTournamentDTO.getTournamentName();
        String gameName = createTournamentDTO.getGameName();

        Assert.hasText(tournamentName, "Tournament name must be given");
        Assert.isTrue(tournamentName.length() >= 3, "Tournament name must have at least 3 characters");
        Assert.hasText(gameName, "Game Name must be given");

    }

    private void validateAddMatchToTournament (AddMatchToTournamentDTO addMatchToTournamentDTO){
        Assert.notNull(addMatchToTournamentDTO, "Match and Tournament information must be given");

        String tournamentName = addMatchToTournamentDTO.getTournamentName();
        Assert.hasText(addMatchToTournamentDTO.getTournamentName(), "Tournament name must be given");
        Assert.isTrue(tournamentName.length() >= 3,
                "Tournament name must be longer 3 characters");

        Match match = addMatchToTournamentDTO.getMatch();
    }

    public void validateMatch(Match match) {
        Assert.notNull(match, "Match object must be given");

        LocalDateTime beginning = match.getBeginning();
        Assert.notNull(beginning, "Beginning must be given");

        Team team1 = match.getTeam1();
        Assert.notNull(team1, "Team1 must be given");

        Team team2 = match.getTeam2();
        Assert.notNull(team2, "Team2 must be given");
    }

    private void validateUpdateDTO(UpdateTournamentDTO updateTournamentDTO) {
        Assert.notNull(updateTournamentDTO, "UpdateTournamentDTO must be given");

        String tournamentName = updateTournamentDTO.getTournamentName();
        Assert.hasText(tournamentName, "Tournament name must be given");
        Assert.isTrue(tournamentName.length() >= 3, "Tournament name must be at least 3 characters long");

        String winnerTeamName = updateTournamentDTO.getWinnerTeamName();
        Assert.hasText(winnerTeamName, "Winner team name must be given");
        Assert.isTrue(winnerTeamName.length() >= 3, "Winner team name must be at least 3 characters long");
    }
}
