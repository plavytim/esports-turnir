package hr.fer.plavy.tim.esports_turnir.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.*;

@Entity
@Table(name = "TEAMS")
public class Team {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @Size(min = 3, max = 15)
    private String teamName;

    @OneToOne
    private Game game;

    @ManyToOne
    private Player leadPlayer;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.MERGE})
    @JoinTable(
            joinColumns = @JoinColumn(name = "teamId"),
            inverseJoinColumns = @JoinColumn(name = "playerId")
    )
    @OrderColumn
    private Set<Player> players;

    @Column
    private Integer numberOfWins;

    @Column
    private Integer numberOfPlayed;

    @Column
    private Integer numberOfTournaments;

    @OneToMany(fetch = FetchType.EAGER)
    private Set<JoinTeamRequest> requests;

    public Team() {
    }

    public Team(String teamName, Game game, Player leadPlayer) {
        this.teamName = teamName;
        this.game = game;
        this.leadPlayer = leadPlayer;
        this.players = new HashSet<>(List.of(leadPlayer));
        this.numberOfWins = 0;
        this.numberOfPlayed = 0;
        this.numberOfTournaments = 0;
        this.requests = new HashSet<>();
    }

    public Team(String teamName, Game game, Player leadPlayer, Set<Player> players) {
        this.teamName = teamName;
        this.game = game;
        this.leadPlayer = leadPlayer;
        this.players = players;
        this.numberOfWins = 0;
        this.numberOfPlayed = 0;
        this.numberOfTournaments = 0;
        this.requests = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long Id) {
        this.id = Id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Player getLeadPlayer() {
        return leadPlayer;
    }

    public void setLeadPlayer(Player leadPlayer) {
        this.leadPlayer = leadPlayer;
    }

    public Set<Player> getPlayers() {
        return players;
    }

    public void setPlayers(Set<Player> players) {
        this.players = players;
    }

    public Integer getNumberOfWins() {
        return numberOfWins;
    }

    public void setNumberOfWins(Integer numberOfWins) {
        this.numberOfWins = numberOfWins;
    }

    public Integer getNumberOfPlayed() {
        return numberOfPlayed;
    }

    public void setNumberOfPlayed(Integer numberOfPlayed) {
        this.numberOfPlayed = numberOfPlayed;
    }

    public Integer getNumberOfTournaments() {
        return numberOfTournaments;
    }

    public void setNumberOfTournaments(Integer numberOfTournaments) {
        this.numberOfTournaments = numberOfTournaments;
    }

    public Set<JoinTeamRequest> getRequests() {
        return requests;
    }

    public void setRequests(Set<JoinTeamRequest> requests) {
        this.requests = requests;
    }

    @Override
    public String toString() {
        return "Team#" + id + " '" + teamName + "', lead: " + leadPlayer + ", players: " + players;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Team)) return false;
        Team team = (Team) o;
        return getId().equals(team.getId()) && getTeamName().equals(team.getTeamName()) && getGame().equals(team.getGame()) && getLeadPlayer().equals(team.getLeadPlayer()) && getPlayers().equals(team.getPlayers()) && getNumberOfWins().equals(team.getNumberOfWins()) && getNumberOfPlayed().equals(team.getNumberOfPlayed()) && getNumberOfTournaments().equals(team.getNumberOfTournaments()) && getRequests().equals(team.getRequests());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTeamName(), getGame(), getLeadPlayer(), getPlayers(), getNumberOfWins(), getNumberOfPlayed(), getNumberOfTournaments(), getRequests());
    }

    public void addPlayer(Player newPlayer) {
        this.players.add(newPlayer);
    }

    public void removePlayer(Player player) {
        this.players.remove(player);
    }

    public void addRequest(JoinTeamRequest request){
        this.requests.add(request);
    }

    public void removeRequest(JoinTeamRequest request){
        this.requests.remove(request);
    }

}
