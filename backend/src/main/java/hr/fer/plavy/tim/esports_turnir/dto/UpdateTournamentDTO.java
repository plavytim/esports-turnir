package hr.fer.plavy.tim.esports_turnir.dto;

import hr.fer.plavy.tim.esports_turnir.model.Game;
import hr.fer.plavy.tim.esports_turnir.model.Team;

public class UpdateTournamentDTO {
    private Long id;
    private String tournamentName;
    private String winnerTeamName;

    public UpdateTournamentDTO(Long id, String tournamentName, String winnerTeamName) {
        this.id = id;
        this.tournamentName = tournamentName;
        this.winnerTeamName = winnerTeamName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public String getWinnerTeamName() {
        return winnerTeamName;
    }

    public void setWinnerTeamName(String winnerTeamName) {
        this.winnerTeamName = winnerTeamName;
    }
}
