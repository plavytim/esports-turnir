package hr.fer.plavy.tim.esports_turnir.dto;

public class UpdateTeamDTO {
    private Long id;
    private String teamName;
    private String gameName;
    private int numberOfWins;
    private int numberOfPlayed;
    private int numberOfTournaments;

    public UpdateTeamDTO(long id, String teamName, String gameName, int numberOfWins, int numberOfPlayed, int numberOfTournaments) {
        this.id = id;
        this.teamName = teamName;
        this.gameName = gameName;
        this.numberOfWins = numberOfWins;
        this.numberOfPlayed = numberOfPlayed;
        this.numberOfTournaments = numberOfTournaments;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public int getNumberOfWins() {
        return numberOfWins;
    }

    public void setNumberOfWins(int numberOfWins) {
        this.numberOfWins = numberOfWins;
    }

    public int getNumberOfPlayed() {
        return numberOfPlayed;
    }

    public void setNumberOfPlayed(int numberOfPlayed) {
        this.numberOfPlayed = numberOfPlayed;
    }

    public int getNumberOfTournaments() {
        return numberOfTournaments;
    }

    public void setNumberOfTournaments(int numberOfTournaments) {
        this.numberOfTournaments = numberOfTournaments;
    }
}
