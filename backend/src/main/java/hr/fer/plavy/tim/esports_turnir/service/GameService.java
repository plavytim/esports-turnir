package hr.fer.plavy.tim.esports_turnir.service;

import hr.fer.plavy.tim.esports_turnir.model.Game;

import java.util.List;
import java.util.Optional;

public interface GameService {

    List<Game> listAll();

    Optional<Game> findById(long gameId);

    Game fetch(long gameId);

    Optional<Game> findByName(String gameName);

    Game fetchByName(String gameName);

    Game createGame(Game newGame);

    Game updateGame(Game game);

    Game deleteGame(long gameId);


}
