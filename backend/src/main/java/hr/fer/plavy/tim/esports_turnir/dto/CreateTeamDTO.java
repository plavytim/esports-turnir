package hr.fer.plavy.tim.esports_turnir.dto;

public class CreateTeamDTO {

    private String teamName;
    private String gameName;
    private String leadPlayerUsername;

    public CreateTeamDTO(String teamName, String gameName, String leadPlayerUsername) {
        this.teamName = teamName;
        this.gameName = gameName;
        this.leadPlayerUsername = leadPlayerUsername;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getLeadPlayerUsername() {
        return leadPlayerUsername;
    }

    public void setLeadPlayerUsername(String leadPlayerUsername) {
        this.leadPlayerUsername = leadPlayerUsername;
    }
}
