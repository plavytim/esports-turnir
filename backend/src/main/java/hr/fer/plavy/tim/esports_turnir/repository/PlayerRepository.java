package hr.fer.plavy.tim.esports_turnir.repository;

import hr.fer.plavy.tim.esports_turnir.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PlayerRepository extends JpaRepository<Player, Long> {

    Optional<Player> findByUsernameAndPassword(String username, String password);

    Optional<Player> findByUsername(String username);

    boolean existsPlayerByUsername(String username);

    boolean existsPlayerByUsernameAndIdNot(String username, long id);
}
