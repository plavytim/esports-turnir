package hr.fer.plavy.tim.esports_turnir.security;

import hr.fer.plavy.tim.esports_turnir.exception.ApplicationExceptionHandler;
import hr.fer.plavy.tim.esports_turnir.exception.TokenException;
import hr.fer.plavy.tim.esports_turnir.model.Player;
import hr.fer.plavy.tim.esports_turnir.model.Role;
import hr.fer.plavy.tim.esports_turnir.service.PlayerService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
public class AuthFilter extends GenericFilterBean {

    @Autowired
    private TokenProvider jwtTokenUtil;

    @Autowired
    private PlayerService playerService;

    @Autowired
    @Qualifier("applicationExceptionHandler")
    private ApplicationExceptionHandler exceptionHandler;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;

        String authHeader = httpRequest.getHeader("Authorization");

        if (!httpRequest.getMethod().equals("GET")) {
//        if (true) {

            if (authHeader == null) {
                setResponse(httpResponse, "Authorization token must be provided");
                return;
            }

            String[] authHeaderArr = authHeader.split("Bearer");
            if (authHeader.length() < 1 && authHeaderArr[1] == null) {
                setResponse(httpResponse, "Authorization token must be Bearer [token]");
                return;
            }

            String token = authHeaderArr[1];

            try {
                Claims claims = Jwts.parser().setSigningKey(SecurityConstants.API_SECRET_KEY)
                        .parseClaimsJws(token).getBody();
                setRole(httpRequest, token, claims);
            } catch (Exception e) {
                setResponse(httpResponse, "Invalid/Expired token");
                return;
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void setRole (HttpServletRequest httpRequest, String token, Claims claims) {
        String username = claims.get("username").toString();
        Player player = playerService.fetchByUsername(username);
        Role role = player.getRole();
        UserDetails userDetails = playerService.loadUserDetailsByUsername(username);
        UsernamePasswordAuthenticationToken authentication = jwtTokenUtil.getAuthentication
                (token, SecurityContextHolder.getContext().getAuthentication(), userDetails, role);
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
        logger.info("authenticated user " + username + ", setting security context");
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    private void setResponse(HttpServletResponse response, String message) throws IOException {
        response.setStatus(403);
        response.setHeader("Content-Type", "application/json");
        response.getWriter().print(new TokenException(message));
    }
}
