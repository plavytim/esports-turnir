package hr.fer.plavy.tim.esports_turnir.controller;

import hr.fer.plavy.tim.esports_turnir.dto.PlayerLoginDTO;
import hr.fer.plavy.tim.esports_turnir.email.EmailSenderService;
import hr.fer.plavy.tim.esports_turnir.model.Player;
import hr.fer.plavy.tim.esports_turnir.security.JWToken;
import hr.fer.plavy.tim.esports_turnir.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/")
public class ApplicationController {

    private final PlayerService playerService;
    private EmailSenderService emailSenderService;

    @Autowired
    public ApplicationController(PlayerService service, EmailSenderService emailSenderService) {
        this.playerService = service;
        this.emailSenderService = emailSenderService;
    }

    @GetMapping("")
    public String getStatus() {
        return "Status OK";
    }

    @PostMapping("/register")
    public ResponseEntity<JWToken> registerPlayer(@RequestBody Player newPlayer) {
        JWToken registeredToken = playerService.createPlayer(newPlayer);
        emailSenderService.sendEmail(newPlayer.getEmail(), newPlayer.getUsername());
        return ResponseEntity.created(URI.create("/players/" + registeredToken.getId())).body(registeredToken);
    }


    @PostMapping("/login")
    public ResponseEntity<JWToken> login(@RequestBody PlayerLoginDTO playerDTO) {
        return ResponseEntity.ok(playerService.login(playerDTO));
    }
}
