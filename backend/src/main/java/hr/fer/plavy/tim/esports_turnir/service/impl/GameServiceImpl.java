package hr.fer.plavy.tim.esports_turnir.service.impl;

import hr.fer.plavy.tim.esports_turnir.exception.EntityMissingException;
import hr.fer.plavy.tim.esports_turnir.exception.RequestDeniedException;
import hr.fer.plavy.tim.esports_turnir.model.Game;
import hr.fer.plavy.tim.esports_turnir.repository.GameRepository;
import hr.fer.plavy.tim.esports_turnir.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Service
public class GameServiceImpl implements GameService {

    private final GameRepository gameRepository;

    @Autowired
    public GameServiceImpl(GameRepository repository) {
        this.gameRepository = repository;
    }

    @Override
    public List<Game> listAll() {
        return gameRepository.findAll();
    }

    @Override
    public Optional<Game> findById(long gameId) {
        return gameRepository.findById(gameId);
    }

    @Override
    public Game fetch(long gameId) {
        return findById(gameId).orElseThrow(
                () -> new EntityMissingException(Game.class, gameId)
        );
    }

    @Override
    public Optional<Game> findByName(String gameName) {
        return gameRepository.findByName(gameName);
    }

    @Override
    public Game fetchByName(String gameName) {
        return findByName(gameName).orElseThrow(
                () -> new EntityMissingException(Game.class, gameName)
        );
    }

    @Override
    public Game createGame(Game newGame) {
        validate(newGame);
        Assert.isNull(newGame.getId(), "Game ID must be null, not: " + newGame.getId());

        if (gameRepository.existsByName(newGame.getName())) {
            throw new RequestDeniedException(
                    "Game with name: " + newGame.getName() + " already exists"
            );
        }
        return gameRepository.save(newGame);
    }

    @Override
    public Game updateGame(Game game) {
        validate(game);
        long gameId = game.getId();

        if (!gameRepository.existsById(gameId)) {
            throw new EntityMissingException(Game.class, gameId);
        } else if (gameRepository.existsByNameAndIdNot(game.getName(), gameId)) {
            throw new RequestDeniedException(
                    "Game with name: " + game.getName() + " already exists"
            );
        }

        return gameRepository.save(game);
    }

    @Override
    public Game deleteGame(long gameId) {
        Game game = fetch(gameId);
        gameRepository.delete(game);
        return game;
    }

    private void validate(Game game) {
        Assert.notNull(game, "Game object must be given");

        String name = game.getName();
        Assert.hasText(name, "Game name must be given");
        Assert.isTrue(name.length() >= 3, "Game name must be at least 3 characters long");

        String genre = game.getGenre();
        Assert.hasText(genre, "Genre must be given");

        String platform = game.getPlatform();
        Assert.hasText(platform, "Platform must be given");

        int numberOfPlayers = game.getNumberOfPlayers();
        Assert.isTrue(numberOfPlayers >= 1, "Must be at least 1 player");

        LocalTime duration = game.getDuration();
        Assert.notNull(duration, "Duration must be given");
    }
}
