package hr.fer.plavy.tim.esports_turnir.dto;

public class CreateTournamentDTO {
    private String tournamentName;
    private String gameName;

    public CreateTournamentDTO(String tournamentName, String gameName) {
        this.tournamentName = tournamentName;
        this.gameName = gameName;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }
}
