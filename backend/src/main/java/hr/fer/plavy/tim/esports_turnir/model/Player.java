package hr.fer.plavy.tim.esports_turnir.model;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "PLAYERS")
public class Player {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @Size(min = 2)
    private String firstname;

    @Column(nullable = false)
    @Size(min = 2)
    private String lastname;

    @Column(unique = true, nullable = false)
    @Size(min = 5, max = 15)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(unique = true, nullable = false)
    @Email(regexp = "^[a-zA-Z]{2}[0-9]{5}@fer.hr$|^[a-zA-Z]{1}[a-zA-Z-]*[a-zA-Z]{1}[.][a-zA-Z]{2,}[0-9]*@fer.hr$")
    private String email;

    @ManyToOne
    private Role role;

    @ManyToMany(mappedBy = "players", cascade = {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.MERGE})
    private Set<Team> teams;

    @CreationTimestamp
    @Column(updatable = false)
    private LocalDateTime creationDateTime;

    public Player() {
    }

    public Player(String firstname, String lastname, String username, String password, String email, Role role) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
        this.email = email;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() { return role; };

    public void setRole(Role role) { this.role = role; }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }

    public void addTeam(Team team){
        this.teams.add(team);
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    @Override
    public String toString() {
        return "Username: " + username + ", email: " + email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Player)) return false;
        Player player = (Player) o;
        return getId().equals(player.getId()) && getFirstname().equals(player.getFirstname()) && getLastname().equals(player.getLastname()) && getUsername().equals(player.getUsername()) && getPassword().equals(player.getPassword()) && getEmail().equals(player.getEmail()) && getRole().equals(player.getRole());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstname(), getLastname(), getUsername(), getPassword(), getEmail(), getRole());
    }
}
