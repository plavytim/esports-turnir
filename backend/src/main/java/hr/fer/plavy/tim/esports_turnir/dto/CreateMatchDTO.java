package hr.fer.plavy.tim.esports_turnir.dto;

import java.time.LocalDateTime;

public class CreateMatchDTO {

    private LocalDateTime matchBeginning;
    private String tournamentName;
    private String team1Name;
    private String team2Name;

    public CreateMatchDTO(LocalDateTime matchBeginning, String tournamentName, String team1Name, String team2Name) {
        this.matchBeginning = matchBeginning;
        this.tournamentName = tournamentName;
        this.team1Name = team1Name;
        this.team2Name = team2Name;
    }

    public LocalDateTime getMatchBeginning() {
        return matchBeginning;
    }

    public void setMatchBeginning(LocalDateTime matchBeginning) {
        this.matchBeginning = matchBeginning;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public String getTeam1Name() {
        return team1Name;
    }

    public void setTeam1Name(String team1Name) {
        this.team1Name = team1Name;
    }

    public String getTeam2Name() {
        return team2Name;
    }

    public void setTeam2Name(String team2Name) {
        this.team2Name = team2Name;
    }

    @Override
    public String toString() {
        return "CreateMatchDTO{" +
                "matchBeginning=" + matchBeginning +
                ", tournamentName='" + tournamentName + '\'' +
                ", team1Name='" + team1Name + '\'' +
                ", team2Name='" + team2Name + '\'' +
                '}';
    }
}
