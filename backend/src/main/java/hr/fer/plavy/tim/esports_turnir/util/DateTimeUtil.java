package hr.fer.plavy.tim.esports_turnir.util;

import java.time.LocalDateTime;
import java.time.LocalTime;

public class DateTimeUtil {
    public static LocalDateTime addLocalTime(LocalDateTime beginning, LocalTime duration){
        return LocalDateTime.of(
                beginning.toLocalDate(),
                beginning.toLocalTime()
                        .plusHours(duration.getHour())
                        .plusMinutes(duration.getMinute())
        );
    }

    public static LocalDateTime registrationDeadline(LocalDateTime beginning){
        return LocalDateTime.of(
                beginning.toLocalDate(),
                beginning.toLocalTime()
                        .minusMinutes(10L)
        );
    }

    public static Boolean isDuringMatch(LocalDateTime dateTime, LocalDateTime beginning, LocalDateTime ending){
        return dateTime.isAfter(dateTime) && dateTime.isBefore(ending);
    }
}
