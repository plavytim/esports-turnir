package hr.fer.plavy.tim.esports_turnir.exception;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class ApplicationExceptionHandler {
    @ExceptionHandler({RequestDeniedException.class, IllegalArgumentException.class})
    protected ResponseEntity<?> handleRequestDeniedException(Exception exception, WebRequest request) {
        Map<String, String> properties = new HashMap<>();
        properties.put("message", exception.getMessage());
        properties.put("status", "400");
        properties.put("error", "Bad Request");
        return new ResponseEntity<>(properties, HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(LoginException.class)
    protected ResponseEntity<?> handleLoginException(Exception exception, WebRequest request) {
        Map<String, String> properties = new HashMap<>();
        properties.put("message", exception.getMessage());
        properties.put("status", "401");
        properties.put("error", "Unauthorized");
        return new ResponseEntity<>(properties, HttpStatus.UNAUTHORIZED);
    }
    @ExceptionHandler(EntityMissingException.class)
    protected ResponseEntity<?> handleEntityMissingException(Exception exception, WebRequest request) {
        Map<String, String> properties = new HashMap<>();
        properties.put("message", exception.getMessage());
        properties.put("status", "404");
        properties.put("error", "Not Found");
        return new ResponseEntity<>(properties, HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(TokenException.class)
    protected ResponseEntity<?> handleTokenException(Exception exception, WebRequest request) {
        Map<String, String> properties = new HashMap<>();
        properties.put("message", exception.getMessage());
        properties.put("status", "403");
        properties.put("error", "Forbidden");
        return new ResponseEntity<>(properties, HttpStatus.FORBIDDEN);
    }
}
