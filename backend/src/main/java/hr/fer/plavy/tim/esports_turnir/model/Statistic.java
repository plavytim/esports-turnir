package hr.fer.plavy.tim.esports_turnir.model;

public class Statistic {

    private String gameName;
    private Integer numberOfPlayed;
    private Integer numberOfWins;
    private Integer numberOfKills;
    private Integer numberOfDeaths;
    private Integer numberOfAssists;

    public Statistic() {
    }

    public Statistic(String gameName) {
        this.gameName = gameName;
        this.numberOfWins = 0;
        this.numberOfPlayed = 0;
        this.numberOfKills = 0;
        this.numberOfDeaths = 0;
        this.numberOfAssists = 0;
    }

    public Statistic( String gameName, Integer numberOfPlayed, Integer numberOfWins, Integer numberOfKills, Integer numberOfDeaths, Integer numberOfAssists) {
        this.gameName = gameName;
        this.numberOfPlayed = numberOfPlayed;
        this.numberOfWins = numberOfWins;
        this.numberOfKills = numberOfKills;
        this.numberOfDeaths = numberOfDeaths;
        this.numberOfAssists = numberOfAssists;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGame(String gameName) {
        this.gameName = gameName;
    }

    public Integer getNumberOfPlayed() {
        return numberOfPlayed;
    }

    public void setNumberOfPlayed(Integer numberOfPlayed) {
        this.numberOfPlayed = numberOfPlayed;
    }

    public Integer getNumberOfWins() {
        return numberOfWins;
    }

    public void setNumberOfWins(Integer numberOfWins) {
        this.numberOfWins = numberOfWins;
    }

    public Integer getNumberOfKills() {
        return numberOfKills;
    }

    public void setNumberOfKills(Integer numberOfKills) {
        this.numberOfKills = numberOfKills;
    }

    public Integer getNumberOfDeaths() {
        return numberOfDeaths;
    }

    public void setNumberOfDeaths(Integer numberOfDeaths) {
        this.numberOfDeaths = numberOfDeaths;
    }

    public Integer getNumberOfAssists() {
        return numberOfAssists;
    }

    public void setNumberOfAssists(Integer numberOfAssists) {
        this.numberOfAssists = numberOfAssists;
    }
}
