package hr.fer.plavy.tim.esports_turnir.email;

public interface EmailSenderService {

    void sendEmail(String email, String username);

    public void sendCalendarInvite(String fromEmail, CalendarRequest calendarRequest) throws Exception;

}
