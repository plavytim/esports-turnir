package hr.fer.plavy.tim.esports_turnir.model;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table(name = "GAMES")
public class Game {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = true)
    @Size(min = 3)
    private String name;

    @Column(nullable = false)
    private String genre;

    @Column(nullable = false)
    private String platform;

    @Column(nullable = false)
    @Min(1)
    private Integer numberOfPlayers;

    @Column(nullable = false)
    private LocalTime duration;

    public Game() {
    }

    public Game(String name, String genre, String platform, Integer numberOfPlayers, LocalTime duration) {
        this.name = name;
        this.genre = genre;
        this.platform = platform;
        this.numberOfPlayers = numberOfPlayers;
        this.duration = duration;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public Integer getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public void setNumberOfPlayers(Integer numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
    }

    public LocalTime getDuration() {
        return duration;
    }

    public void setDuration(LocalTime duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", genre='" + genre + '\'' +
                ", platform='" + platform + '\'' +
                ", numberOfPlayers=" + numberOfPlayers +
                ", duration=" + duration +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Game)) return false;
        Game game = (Game) o;
        return getId().equals(game.getId()) && getName().equals(game.getName()) && getGenre().equals(game.getGenre()) && getPlatform().equals(game.getPlatform()) && getNumberOfPlayers().equals(game.getNumberOfPlayers()) && getDuration().equals(game.getDuration());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getGenre(), getPlatform(), getNumberOfPlayers(), getDuration());
    }
}
