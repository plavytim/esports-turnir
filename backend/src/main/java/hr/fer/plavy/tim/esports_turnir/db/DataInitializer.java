package hr.fer.plavy.tim.esports_turnir.db;

import hr.fer.plavy.tim.esports_turnir.dto.AddMatchToTournamentDTO;
import hr.fer.plavy.tim.esports_turnir.dto.PlayerUsernameDTO;
import hr.fer.plavy.tim.esports_turnir.dto.CreateTeamDTO;
import hr.fer.plavy.tim.esports_turnir.model.*;
import hr.fer.plavy.tim.esports_turnir.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DataInitializer {

    private final GameService gameService;
    private final PlayerService playerService;
    private final MatchService matchService;
    private final TeamService teamService;
    private final TournamentService tournamentService;
    private final RoleService roleService;

    List<Role> roles;
    List<Player> players31;
    List<Player> players32;
    List<Player> players21;
    List<Player> players22;
    List<Player> players23;
    List<Game> games;
    List<Team> teams;
    List<Tournament> tournaments;
    List<Match> matches;
    Player player13;
    Player player14;
    Player player15;
    Player player16;
    Player player17;
    Player player18;
    Player player19;
    Player player20;

   @Autowired
    public DataInitializer(GameService gameService, PlayerService playerService, MatchService matchService, TeamService teamService, TournamentService tournamentService, RoleService roleService) {
        this.gameService = gameService;
        this.playerService = playerService;
        this.matchService = matchService;
        this.teamService = teamService;
        this.tournamentService = tournamentService;
        this.roleService = roleService;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public List<Player> getPlayers31() {
        return players31;
    }

    public List<Player> getPlayers32() {
        return players32;
    }

    public List<Player> getPlayers21() {
        return players21;
    }

    public List<Player> getPlayers22() {
        return players22;
    }

    public List<Player> getPlayers23() {
        return players23;
    }

    public Player getPlayer13() {
        return player13;
    }

    public Player getPlayer14() {
        return player14;
    }

    public Player getPlayer15() {
        return player15;
    }

    public Player getPlayer16() {
        return player16;
    }

    public Player getPlayer17() {
        return player17;
    }

    public Player getPlayer18() {
        return player18;
    }

    public Player getPlayer19() {
        return player19;
    }

    public Player getPlayer20() {
        return player20;
    }

    public List<Game> getGames() {
        return games;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public List<Tournament> getTournaments() {
        return tournaments;
    }

    public List<Match> getMatches() {
        return matches;
    }

    @EventListener
    public void appReady(ApplicationReadyEvent event){
        roles = DatabaseUtil.getRoles();
        players31 = DatabaseUtil.getPlayers31();
        players32 = DatabaseUtil.getPlayers32();
        players21 = DatabaseUtil.getPlayers21();
        players22 = DatabaseUtil.getPlayers22();
        players23 = DatabaseUtil.getPlayers23();
        player13 = DatabaseUtil.getPlayer13();
        player14 = DatabaseUtil.getPlayer14();
        player15 = DatabaseUtil.getPlayer15();
        player16 = DatabaseUtil.getPlayer16();
        player17 = DatabaseUtil.getPlayer17();
        player18 = DatabaseUtil.getPlayer18();
        player19 = DatabaseUtil.getPlayer19();
        player20 = DatabaseUtil.getPlayer20();

        roles.forEach(roleService::createRole);
        players31.forEach(playerService::createPlayer);
        players32.forEach(playerService::createPlayer);
        players21.forEach(playerService::createPlayer);
        players22.forEach(playerService::createPlayer);
        players23.forEach(playerService::createPlayer);

        playerService.createPlayer(player13);
        playerService.createPlayer(player14);
        playerService.createPlayer(player15);
        playerService.createPlayer(player16);
        playerService.createPlayer(player17);
        playerService.createPlayer(player18);
        playerService.createPlayer(player19);
        playerService.createPlayer(player20);

        games = Arrays.asList(DatabaseUtil.getGame1(), DatabaseUtil.getGame2());

        games.forEach(gameService::createGame);

        CreateTeamDTO team31DTO = DatabaseUtil.getTeam31DTO();
        Team team31 = teamService.createTeam(team31DTO);
        for(int i = 1; i <= team31.getPlayers().size() + 1; i++){
            teamService.addPlayerToTeam(team31.getId(), new PlayerUsernameDTO(players31.get(i).getUsername()));
        }
        team31 = teamService.fetch(team31.getId());

        CreateTeamDTO team32DTO = DatabaseUtil.getTeam32DTO();
        Team team32 = teamService.createTeam(team32DTO);
        for(int i = 1; i <= team32.getPlayers().size() + 1; i++){
            teamService.addPlayerToTeam(team32.getId(), new PlayerUsernameDTO(players32.get(i).getUsername()));
        }

        team32 = teamService.fetch(team32.getId());

        CreateTeamDTO team21DTO = DatabaseUtil.getTeam21DTO();
        Team team21 = teamService.createTeam(team21DTO);
        for(int i = 1; i <= team21.getPlayers().size(); i++){
            teamService.addPlayerToTeam(team21.getId(), new PlayerUsernameDTO(players21.get(i).getUsername()));
        }

        team21 = teamService.fetch(team21.getId());

        CreateTeamDTO team22DTO = DatabaseUtil.getTeam22DTO();
        Team team22 = teamService.createTeam(team22DTO);
        for(int i = 1; i <= team22.getPlayers().size(); i++){
            teamService.addPlayerToTeam(team22.getId(), new PlayerUsernameDTO(players22.get(i).getUsername()));
        }

        team22 = teamService.fetch(team22.getId());

        CreateTeamDTO team23DTO = DatabaseUtil.getTeam23DTO();
        Team team23 = teamService.createTeam(team23DTO);
        for(int i = 1; i <= team23.getPlayers().size(); i++){
            teamService.addPlayerToTeam(team23.getId(), new PlayerUsernameDTO(players23.get(i).getUsername()));
        }

        team23 = teamService.fetch(team23.getId());

        teams = Arrays.asList(team31, team32 ,team21, team22, team23);

        Tournament tournament1 = tournamentService.createTournament(DatabaseUtil.getTournament1DTO());

        Tournament tournament2 = tournamentService.createTournament(DatabaseUtil.getTournament2DTO());

        Match match1 = matchService.createMatch(DatabaseUtil.getMatch1DTO());

        Match match2 = matchService.createMatch(DatabaseUtil.getMatch2DTO());

        matches = Arrays.asList(match1, match2);

//        tournamentService.addMatchToTournament(
//                new AddMatchToTournamentDTO(
//                        tournament1.getTournamentName(),
//                        match1
//                )
//        );

        tournament1 = tournamentService.fetch(tournament1.getId());

//        tournamentService.addMatchToTournament(
//                new AddMatchToTournamentDTO(
//                        tournament2.getTournamentName(),
//                        match2
//                )
//        );

        tournament2 = tournamentService.fetch(tournament2.getId());

        tournaments = Arrays.asList(tournament1, tournament2);
    }
}
