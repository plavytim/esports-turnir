package hr.fer.plavy.tim.esports_turnir.exception;

public class EntityMissingException extends IllegalArgumentException {

    public EntityMissingException(Class<?> cls, Object ref) {
        super("Entity with reference " + ref + " of " + cls + " not found.");
    }
}
