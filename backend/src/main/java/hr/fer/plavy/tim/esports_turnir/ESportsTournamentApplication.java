package hr.fer.plavy.tim.esports_turnir;

import hr.fer.plavy.tim.esports_turnir.security.AuthFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ESportsTournamentApplication {

	@Autowired
	private AuthFilter authFilter;

	public static void main(String[] args) {
		SpringApplication.run(ESportsTournamentApplication.class, args);
	}

	@Bean
	public FilterRegistrationBean<AuthFilter> registrationBean(){
		FilterRegistrationBean<AuthFilter> registrationBean = new FilterRegistrationBean<>();
		registrationBean.setFilter(authFilter);
		registrationBean.addUrlPatterns("/players/*");
		registrationBean.addUrlPatterns("/games/*");
		registrationBean.addUrlPatterns("/teams/*");
		registrationBean.addUrlPatterns("/tournaments/*");
		return registrationBean;
	}
}
