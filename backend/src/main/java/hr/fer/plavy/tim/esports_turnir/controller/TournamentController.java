package hr.fer.plavy.tim.esports_turnir.controller;

import hr.fer.plavy.tim.esports_turnir.dto.AddMatchToTournamentDTO;
import hr.fer.plavy.tim.esports_turnir.dto.CreateTournamentDTO;
import hr.fer.plavy.tim.esports_turnir.dto.UpdateTournamentDTO;
import hr.fer.plavy.tim.esports_turnir.model.Tournament;
import hr.fer.plavy.tim.esports_turnir.service.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/tournaments")
public class TournamentController {

    private final TournamentService tournamentService;

    @Autowired
    public TournamentController(TournamentService tournamentService){ this.tournamentService = tournamentService; }

    @GetMapping("")
    public List<Tournament> getAllTournaments(){
        return tournamentService.listAll();
    }

    @GetMapping("/{tournamentId}")
    public Tournament getTournament(@PathVariable long tournamentId){
        return tournamentService.fetch(tournamentId);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'GAMEMASTER')")
    @PostMapping("")
    public ResponseEntity<Tournament> createTournament(@RequestBody CreateTournamentDTO newTournamentDTO){
        Tournament createdTournament = tournamentService.createTournament(newTournamentDTO);
        return ResponseEntity.created(URI.create("/tournaments" + createdTournament.getId())).body(createdTournament);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'GAMEMASTER')")
    @PutMapping("/{tournamentId}")
    public Tournament updateTournament(@PathVariable long tournamentId, @RequestBody UpdateTournamentDTO updateTournamentDTO){
        if(!updateTournamentDTO.getId().equals(tournamentId)){
            throw new IllegalArgumentException("Tournament ID must be preserved");
        }

        return tournamentService.updateTournament(updateTournamentDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'GAMEMASTER')")
    @DeleteMapping("/{tournamentId}")
    public Tournament deleteTournament(@PathVariable long tournamentId){
        return tournamentService.deleteTournament(tournamentId);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'GAMEMASTER')")
    @PostMapping("/matches")
    public ResponseEntity<Tournament> addMatchToTournament(@RequestBody AddMatchToTournamentDTO addMatchToTournamentDTO){
        Tournament updatedTournament = tournamentService.addMatchToTournament(addMatchToTournamentDTO);
        return ResponseEntity.accepted().body(updatedTournament);
    }

}
