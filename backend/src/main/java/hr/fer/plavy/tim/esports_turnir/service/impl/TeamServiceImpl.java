package hr.fer.plavy.tim.esports_turnir.service.impl;

import hr.fer.plavy.tim.esports_turnir.dto.JoinTeamDTO;
import hr.fer.plavy.tim.esports_turnir.dto.PlayerUsernameDTO;
import hr.fer.plavy.tim.esports_turnir.dto.CreateTeamDTO;
import hr.fer.plavy.tim.esports_turnir.dto.UpdateTeamDTO;
import hr.fer.plavy.tim.esports_turnir.exception.EntityMissingException;
import hr.fer.plavy.tim.esports_turnir.exception.RequestDeniedException;
import hr.fer.plavy.tim.esports_turnir.mapper.TeamMapper;
import hr.fer.plavy.tim.esports_turnir.model.*;
import hr.fer.plavy.tim.esports_turnir.repository.TeamRepository;
import hr.fer.plavy.tim.esports_turnir.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private GameService gameService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private TeamMapper teamMapper;

    @Autowired
    private MatchService matchService;

    @Autowired
    private RequestService requestService;

    @Override
    public List<Team> listAll() {
        return teamRepository.findAll();
    }

    @Override
    public Optional<Team> findById(long teamId) {
        return teamRepository.findById(teamId);
    }

    @Override
    public Team fetch(long teamId) {
        return findById(teamId).orElseThrow(
                () -> new EntityMissingException(Team.class, teamId)
        );
    }

    @Override
    public Optional<Team> findByTeamName(String teamName) {
        return teamRepository.findByTeamName(teamName);
    }

    @Override
    public Team fetchByTeamName(String teamName) {
        return findByTeamName(teamName).orElseThrow(
                () -> new EntityMissingException(Team.class, teamName)
        );
    }

    @Override
    public Team createTeam(CreateTeamDTO newTeamDTO) {
        validateCreateDTO(newTeamDTO);

        Game game = gameService.fetchByName(newTeamDTO.getGameName());
        Player leadPlayer = playerService.fetchByUsername(newTeamDTO.getLeadPlayerUsername());

        if (teamRepository.existsByGameAndTeamName(game, newTeamDTO.getTeamName())) {
            throw new RequestDeniedException(
                    "Team: " + newTeamDTO.getTeamName() + " already exists for game " + game.getName()
            );
        }

        if (teamRepository.existsByGameAndLeadPlayer(game, leadPlayer)) {
            throw new RequestDeniedException(
                    "Player: " + leadPlayer.getUsername() + " already has team for game: " + game.getName()
            );
        }

        Team createdTeam = new Team(
                newTeamDTO.getTeamName(),
                game,
                leadPlayer
        );

        return teamRepository.save(createdTeam);
    }

    @Override
    public Team updateTeam(UpdateTeamDTO updateTeamDTO) {
        validateUpdateDTO(updateTeamDTO);

        long teamId = updateTeamDTO.getId();
        Game game = gameService.fetchByName(updateTeamDTO.getGameName());
        if (teamRepository.existsByGameAndTeamName(game, updateTeamDTO.getTeamName())) {
            throw new RequestDeniedException(
                    "Team: " + updateTeamDTO.getTeamName() + " already exists for game " + game.getName()
            );
        }

        Team team = fetch(teamId);
        Team newTeam = teamMapper.updateTeamFromTeamDTO(team, updateTeamDTO);

        return teamRepository.save(newTeam);
    }

    @Override
    public Team deleteTeam(long teamId) {
        Team team = fetch(teamId);

        Set<Match> matches = matchService.findTeamsAllMatches(teamId).orElse(new HashSet<>());

        matches.forEach(match -> matchService.deleteMatch(match.getId()));

        teamRepository.delete(team);
        return team;
    }

    @Override
    public Team addPlayerToTeam(long teamId, PlayerUsernameDTO playerUsername) {
        validateUsernameDTO(playerUsername);

        String username = playerUsername.getUsername();

        Team team = fetch(teamId);
        Player player = playerService.fetchByUsername(username);

        boolean playerAlreadyInTeam =
                team.getPlayers().stream()
                        .anyMatch(p -> p.getUsername().equals(username));

        boolean teamIsFull = team.getPlayers().size() >= team.getGame().getNumberOfPlayers();

        if (playerAlreadyInTeam) {
            throw new RequestDeniedException(
                    "Player with username: " + username + " already in team " + team.getTeamName()
            );
        } else if (teamIsFull) {
            throw new RequestDeniedException(
                    "Team " + team.getTeamName() + " is already full"
            );
        }

        team.addPlayer(player);

        return teamRepository.save(team);
    }

    /*TODO
     *  dodati provjeru je li igraci lead i ako je napraviti sta treba
     * */
    @Override
    public Team removePlayerFromTeam(long teamId, long playerId) {
        Team team = fetch(teamId);
        Player player = playerService.fetch(playerId);

        if (!team.getPlayers().contains(player)) {
            throw new RequestDeniedException(
                    "Player with username: " + player.getUsername() + " is not in Team " + team.getTeamName()
            );
        }

        team.removePlayer(player);

        return teamRepository.save(team);
    }

    @Override
    public Team setNewTeamLead(long teamId, PlayerUsernameDTO playerUsernameDTO) {
        validateUsernameDTO(playerUsernameDTO);

        String username = playerUsernameDTO.getUsername();

        Team team = fetch(teamId);
        Player newLeadPlayer = playerService.fetchByUsername(username);

        if (!team.getPlayers().contains(newLeadPlayer)) {
            throw new RequestDeniedException(
                    "Player: " + username + " is not in Team " + team.getTeamName()
            );
        }

        Player oldTeamLead = team.getLeadPlayer();
        team.setLeadPlayer(newLeadPlayer);

        team.removePlayer(oldTeamLead);
        team.addPlayer(oldTeamLead);

        return teamRepository.save(team);
    }

    @Override
    public Team removeLeadFromTeam(long teamId) {
        Team team = fetch(teamId);

        if (team.getPlayers().size() <= 1) {
            return deleteTeam(teamId);
        }

        Player oldTeamLead = team.getLeadPlayer();

        team.removePlayer(oldTeamLead);

        Player newTeamLead = Collections.min(team.getPlayers(), Comparator.comparing(Player::getCreationDateTime));

        team.setLeadPlayer(newTeamLead);

        return teamRepository.save(team);
    }

    @Override
    public Optional<List<Team>> findByTeamLead(Player leadPlayer) {
        return teamRepository.findAllByLeadPlayer(leadPlayer);
    }

    @Override
    public Team sendRequest(long teamId, JoinTeamDTO joinTeamDTO) {
        validateRequest(joinTeamDTO);

        Team team = fetch(teamId);
        Player player = playerService.fetchByUsername(joinTeamDTO.getPlayerUsername());

        if (teamRepository.existsByGameAndContainsPlayer(team.getGame(), player)) {
            throw new RequestDeniedException("Player " + player.getUsername() + " is already in Team for Game " + team.getGame().getName());
        }

        boolean playerAlreadyInTeam = team.getPlayers().contains(player);

        if (playerAlreadyInTeam) {
            throw new RequestDeniedException("Player " + player.getUsername() + " is already in Team " + team.getTeamName());
        }

        boolean alreadySentRequest = team.getRequests()
                .stream()
                .anyMatch(request -> request.getPlayer().getUsername().equals(joinTeamDTO.getPlayerUsername()));

        if (alreadySentRequest) {
            throw new RequestDeniedException("Player " + player.getUsername() + " has already sent request to Team " + team.getTeamName());
        }

        JoinTeamRequest request = requestService.createRequest(player, joinTeamDTO.getRequestMessage());

        team.addRequest(request);

        return teamRepository.save(team);
    }

    /*TODO
     *  provjera ima li mjesta u timu
     *  provjera je li igrac vec igra tu igru
     *  ubacit playera u team
     *  pobrisat taj request
     *  pobrisat ostale requestove za taj game od tog playera*/

    @Override
    public Team confirmRequest(long teamId, long requestId) {
        Team team = fetch(teamId);
        Game game = team.getGame();

        boolean requestExists = team.getRequests().stream().anyMatch(request -> request.getId().equals(requestId));

        if (!requestExists) {
            throw new EntityMissingException(JoinTeamRequest.class, requestId);
        }

        if (team.getPlayers().size() >= team.getGame().getNumberOfPlayers()) {
            throw new RequestDeniedException("Team " + team.getTeamName() + "is already full");
        }

        JoinTeamRequest request = requestService.fetch(requestId);
        Player player = request.getPlayer();

        if (teamRepository.existsByGameAndContainsPlayer(game, player)) {
            throw new RequestDeniedException("Player " + player.getUsername() + " is already in Team for Game " + game.getName());
        }

        team.addPlayer(player);
        teamRepository.save(team);


        List<Team> otherTeamsWithSameGame = teamRepository.findAllByGame(game).orElse(new ArrayList<>());

        List<Team> otherTeams = otherTeamsWithSameGame.stream()
                .filter(otherTeam -> otherTeam.getRequests().stream().anyMatch(otherRequest -> otherRequest.getPlayer().equals(player)))
                .collect(Collectors.toList());

        otherTeams.forEach(otherTeam -> deleteRequest(otherTeam, player));

        return team;
    }

    /* TODO
     *   pobrisat taj request*/

    @Override
    public Team rejectRequest(long teamId, long requestId) {
        Team team = fetch(teamId);

        boolean requestExists = team.getRequests().stream().anyMatch(request -> request.getId().equals(requestId));

        if (!requestExists) {
            throw new EntityMissingException(JoinTeamRequest.class, requestId);
        }

        JoinTeamRequest request = requestService.fetch(requestId);
        Player player = request.getPlayer();

        deleteRequest(team, player);

        return team;
    }

    private void deleteRequest(Team team, Player player) {
        team.getRequests().stream()
                .filter(request -> request.getPlayer().equals(player))
                .forEach(request -> {
                    team.removeRequest(request);
                    teamRepository.save(team);
                    requestService.deleteRequest(request);
                });
    }

    private void validateRequest(JoinTeamDTO joinTeamDTO) {
        Assert.notNull(joinTeamDTO, "Request informations must be given");

        Assert.hasText(joinTeamDTO.getPlayerUsername(), "Player username must be given");
    }

    private void validateCreateDTO(CreateTeamDTO newTeamDTO) {
        Assert.notNull(newTeamDTO, "Team information must be given");

        String teamName = newTeamDTO.getTeamName();
        Assert.hasText(teamName, "Team name must be given");
        Assert.isTrue(teamName.length() >= 3 && teamName.length() <= 15,
                "Team name must be at least 3 and maximum 15 characters long");

        String gameName = newTeamDTO.getGameName();
        Assert.hasText(gameName, "Game name must be given");
        Assert.isTrue(gameName.length() >= 3, "Game name must be at least 3 characters long");

        String leadPlayerUsername = newTeamDTO.getLeadPlayerUsername();
        Assert.hasText(leadPlayerUsername, "Username must be given");
        Assert.isTrue(leadPlayerUsername.length() >= 5 && leadPlayerUsername.length() <= 15,
                "Username must be at least 5 and maximum 15 characters long");
    }

    private void validateUpdateDTO(UpdateTeamDTO teamDTO) {
        Assert.notNull(teamDTO, "Team object must be given");

        String teamName = teamDTO.getTeamName();
        Assert.hasText(teamName, "Team name must be given");
        Assert.isTrue(teamName.length() >= 3 && teamName.length() <= 15,
                "Team name must be at least 3 and maximum 15 characters long");

        String gameName = teamDTO.getGameName();
        Assert.hasText(gameName, "Game name must be given");
        Assert.isTrue(gameName.length() >= 3, "Game name must be at least 3 characters long");

        int numberOfWins = teamDTO.getNumberOfWins();
        Assert.isTrue(numberOfWins >= 0, "Number of wins must be 0 or greater");

        int numberOfPlayed = teamDTO.getNumberOfPlayed();
        Assert.isTrue(numberOfPlayed >= 0, "Number of played must be 0 or greater");

        int numberOfTournaments = teamDTO.getNumberOfTournaments();
        Assert.isTrue(numberOfTournaments >= 0, "Number of tournament must be 0 or greater");
    }

    private void validateUsernameDTO(PlayerUsernameDTO playerUsername) {
        Assert.notNull(playerUsername, "Player username must be given");

        String username = playerUsername.getUsername();
        Assert.hasText(username, "Player username must be given");
        Assert.isTrue(username.length() >= 5 && username.length() <= 15,
                "Player username must be at least 5 and maximum 15 characters long");
    }

}
