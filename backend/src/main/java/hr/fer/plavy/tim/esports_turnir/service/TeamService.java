package hr.fer.plavy.tim.esports_turnir.service;

import hr.fer.plavy.tim.esports_turnir.dto.JoinTeamDTO;
import hr.fer.plavy.tim.esports_turnir.dto.PlayerUsernameDTO;
import hr.fer.plavy.tim.esports_turnir.dto.CreateTeamDTO;
import hr.fer.plavy.tim.esports_turnir.dto.UpdateTeamDTO;
import hr.fer.plavy.tim.esports_turnir.model.Player;
import hr.fer.plavy.tim.esports_turnir.model.Team;

import java.util.List;
import java.util.Optional;

public interface TeamService {

    List<Team> listAll();

    Optional<Team> findById(long teamId);

    Team fetch(long teamId);

    Optional<Team> findByTeamName(String teamName);

    Team fetchByTeamName(String teamName);

    Team createTeam(CreateTeamDTO newTeamDTO);

    Team updateTeam(UpdateTeamDTO updateTeamDTO);

    Team deleteTeam(long teamId);

    Team addPlayerToTeam(long teamId, PlayerUsernameDTO playerUsername);

    Team removePlayerFromTeam(long teamId, long playerId);

    Team setNewTeamLead(long teamId, PlayerUsernameDTO playerUsernameDTO);

    Team removeLeadFromTeam(long teamId);

    Optional<List<Team>> findByTeamLead(Player leadPlayer);

    Team sendRequest(long teamId, JoinTeamDTO joinTeamDTO);

    Team confirmRequest(long teamId, long requestId);

    Team rejectRequest(long teamId, long requestId);
}
