package hr.fer.plavy.tim.esports_turnir.exception;

public class RequestDeniedException extends IllegalArgumentException {
    public RequestDeniedException(String message) {
        super(message);
    }
}
