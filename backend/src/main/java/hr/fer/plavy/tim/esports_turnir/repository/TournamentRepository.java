package hr.fer.plavy.tim.esports_turnir.repository;

import hr.fer.plavy.tim.esports_turnir.model.Match;
import hr.fer.plavy.tim.esports_turnir.model.Tournament;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface TournamentRepository extends JpaRepository<Tournament, Long> {

    boolean existsTournamentByTournamentName(String tournamentName);

    Optional<Tournament> findByTournamentName(String tournamentName);

    @Query("SELECT CASE WHEN (COUNT(t) > 0) THEN TRUE ELSE FALSE END FROM Tournament t WHERE :match MEMBER OF t.matches")
    boolean matchAlreadyInTournament(@Param("match") Match match);
}
