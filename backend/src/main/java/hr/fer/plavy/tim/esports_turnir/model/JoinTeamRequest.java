package hr.fer.plavy.tim.esports_turnir.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "JOIN_TEAM_REQUEST")
public class JoinTeamRequest {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    private Player player;

    @Column
    private String requestMessage;

    public JoinTeamRequest() {
    }

    public JoinTeamRequest(Player player, String requestMessage) {
        this.player = player;
        this.requestMessage = requestMessage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getRequestMessage() {
        return requestMessage;
    }

    public void setRequestMessage(String requestMessage) {
        this.requestMessage = requestMessage;
    }

    @Override
    public String toString() {
        return "Player " + player.getUsername() + " wants to join Team with message:\n" + requestMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JoinTeamRequest)) return false;
        JoinTeamRequest that = (JoinTeamRequest) o;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getPlayer(), that.getPlayer()) && Objects.equals(getRequestMessage(), that.getRequestMessage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getPlayer(), getRequestMessage());
    }
}
