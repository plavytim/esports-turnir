package hr.fer.plavy.tim.esports_turnir.service.impl;

import hr.fer.plavy.tim.esports_turnir.exception.EntityMissingException;
import hr.fer.plavy.tim.esports_turnir.model.Role;
import hr.fer.plavy.tim.esports_turnir.repository.RoleRepository;
import hr.fer.plavy.tim.esports_turnir.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository repository) {
        this.roleRepository = repository;
    }

    @Override
    public Role createRole(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public Optional<Role> findByRolename(String rolename) { return roleRepository.findByRolename(rolename); }

    @Override
    public Role fetchByRolename(String rolename) {
        return findByRolename(rolename).orElseThrow(
                () -> new EntityMissingException(Role.class, rolename)
        );
    }
}
