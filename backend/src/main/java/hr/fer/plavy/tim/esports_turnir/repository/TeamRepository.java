package hr.fer.plavy.tim.esports_turnir.repository;

import hr.fer.plavy.tim.esports_turnir.model.Game;
import hr.fer.plavy.tim.esports_turnir.model.Player;
import hr.fer.plavy.tim.esports_turnir.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface TeamRepository extends JpaRepository<Team, Long> {

    boolean existsByGameAndLeadPlayer(Game game, Player leadPlayer);

    boolean existsByGameAndTeamName(Game game, String teamName);

    @Query("SELECT CASE WHEN (COUNT (team)>0) THEN TRUE ELSE FALSE END FROM Team team WHERE team.game = :game AND :player MEMBER OF team.players")
    boolean existsByGameAndContainsPlayer(Game game, Player player);

    Optional<List<Team>> findAllByGame(Game game);

    Optional<Team> findByTeamName(String teamName);

    Optional<List<Team>> findAllByLeadPlayer(Player leadPlayer);
}
