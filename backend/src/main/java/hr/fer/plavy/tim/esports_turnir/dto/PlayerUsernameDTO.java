package hr.fer.plavy.tim.esports_turnir.dto;

public class PlayerUsernameDTO {
    private String username;

    public PlayerUsernameDTO() {
    }

    public PlayerUsernameDTO(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
