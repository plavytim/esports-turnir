package hr.fer.plavy.tim.esports_turnir.mapper;

import hr.fer.plavy.tim.esports_turnir.dto.UpdateTournamentDTO;
import hr.fer.plavy.tim.esports_turnir.model.Team;
import hr.fer.plavy.tim.esports_turnir.model.Tournament;
import hr.fer.plavy.tim.esports_turnir.service.GameService;
import hr.fer.plavy.tim.esports_turnir.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TournamentMapper {

    private final GameService gameService;

    private final TeamService teamService;

    @Autowired
    public TournamentMapper(GameService gameService, TeamService teamService){
        this.gameService = gameService;
        this.teamService = teamService;
    }

    public Tournament updateTournamentFromTournamentDTO(Tournament oldTournament, UpdateTournamentDTO newTournament){
        if (!oldTournament.getTournamentName().equals(newTournament.getTournamentName())){
            oldTournament.setTournamentName(newTournament.getTournamentName());
        }
        if (oldTournament.getWinnerTeam() == null){
            if (newTournament.getWinnerTeamName() != null) {
                Team newWinnerTeam = teamService.fetchByTeamName(newTournament.getWinnerTeamName());
                oldTournament.setWinnerTeam(newWinnerTeam);
            }
        }
        else if (!oldTournament.getWinnerTeam().getTeamName().equals(newTournament.getWinnerTeamName())){
            Team newWinnerTeam = teamService.fetchByTeamName(newTournament.getWinnerTeamName());
            oldTournament.setWinnerTeam(newWinnerTeam);
        }

        return oldTournament;
    }
}
