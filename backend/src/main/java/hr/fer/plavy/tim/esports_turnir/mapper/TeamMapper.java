package hr.fer.plavy.tim.esports_turnir.mapper;

import hr.fer.plavy.tim.esports_turnir.dto.UpdateTeamDTO;
import hr.fer.plavy.tim.esports_turnir.model.Game;
import hr.fer.plavy.tim.esports_turnir.model.Team;
import hr.fer.plavy.tim.esports_turnir.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TeamMapper {

    private final GameService gameService;

    @Autowired
    public TeamMapper(GameService gameService) {
        this.gameService = gameService;
    }

    public Team updateTeamFromTeamDTO(Team oldTeam, UpdateTeamDTO newTeam) {
        if (!oldTeam.getTeamName().equals(newTeam.getTeamName())) {
            oldTeam.setTeamName(newTeam.getTeamName());
        }

        if (!oldTeam.getGame().getName().equals(newTeam.getGameName())) {
            Game newGame = gameService.fetchByName(newTeam.getGameName());
            oldTeam.setGame(newGame);
        }

        if (oldTeam.getNumberOfWins() != newTeam.getNumberOfWins()) {
            oldTeam.setNumberOfWins(newTeam.getNumberOfWins());
        }

        if (oldTeam.getNumberOfPlayed() != newTeam.getNumberOfPlayed()) {
            oldTeam.setNumberOfPlayed(newTeam.getNumberOfPlayed());
        }

        if (oldTeam.getNumberOfTournaments() != newTeam.getNumberOfTournaments()) {
            oldTeam.setNumberOfTournaments(newTeam.getNumberOfTournaments());
        }

        return oldTeam;
    }
}
