package hr.fer.plavy.tim.esports_turnir.dto;

import hr.fer.plavy.tim.esports_turnir.model.Match;

public class AddMatchToTournamentDTO {

    private String tournamentName;
    private Match match;

    public AddMatchToTournamentDTO(String tournamentName, Match match) {
        this.tournamentName = tournamentName;
        this.match = match;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }
}
