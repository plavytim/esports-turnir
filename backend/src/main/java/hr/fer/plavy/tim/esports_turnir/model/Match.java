package hr.fer.plavy.tim.esports_turnir.model;

import hr.fer.plavy.tim.esports_turnir.service.TournamentService;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "MATCHES")
public class Match {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private LocalDateTime beginning;

    @OneToOne
    private Team team1;

    @OneToOne
    private Team team2;

    @Column(name = "winnerTeamId")
    private Long winnerTeamId;

    @ManyToOne
    @JoinColumn(name="tournament_id", referencedColumnName = "id", updatable = true )
    private Tournament tournament;

    public Match() {
    }

    public Match(LocalDateTime beginning, Team team1, Team team2, Tournament tournament) {
        this.beginning = beginning;
        this.team1 = team1;
        this.team2 = team2;
        this.tournament = tournament;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getBeginning() {
        return beginning;
    }

    public void setBeginning(LocalDateTime beginning) {
        this.beginning = beginning;
    }

    public Team getTeam1() {
        return team1;
    }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }

    public Long getWinnerTeamId() {
        return winnerTeamId;
    }

    public void setWinnerTeamId(Long winnerTeam) {
        this.winnerTeamId = winnerTeam;
    }

    @Override
    public String toString() {
        return "Match between " + team1 + " and " + team2 + "\nbegins at " + beginning + "\ntournament ID is " + tournament.getId();
    }

    public void setTournament(Tournament tournament) {
        this.tournament = tournament;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Match)) return false;
        Match match = (Match) o;
        return getId().equals(match.getId()) && getBeginning().equals(match.getBeginning()) && getTeam1().equals(match.getTeam1()) && getTeam2().equals(match.getTeam2()) && Objects.equals(getWinnerTeamId(), match.getWinnerTeamId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBeginning(), getTeam1(), getTeam2(), getWinnerTeamId());
    }
}
