package hr.fer.plavy.tim.esports_turnir.service;

import hr.fer.plavy.tim.esports_turnir.model.Role;

import java.util.Optional;

public interface RoleService {

    Role createRole(Role role);

    Optional<Role> findByRolename(String rolename);

    Role fetchByRolename(String rolename);

}
