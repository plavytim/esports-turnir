package hr.fer.plavy.tim.esports_turnir.model;


import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "Roles")
public class Role {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @Size(min = 2)
    private String rolename;

    public Role(){
    }

    public Role(String rolename){
        this.rolename = rolename;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getRolename() { return rolename; }

    public void setRolename(String rolename) { this.rolename = rolename; }

    @Override
    public String toString() {return rolename; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Role)) return false;
        Role role = (Role) o;
        return getId().equals(role.getId()) && getRolename().equals(role.getRolename());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getRolename());
    }
}
