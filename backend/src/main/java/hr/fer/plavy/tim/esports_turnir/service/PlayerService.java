package hr.fer.plavy.tim.esports_turnir.service;

import hr.fer.plavy.tim.esports_turnir.dto.PlayerLoginDTO;
import hr.fer.plavy.tim.esports_turnir.model.Player;
import hr.fer.plavy.tim.esports_turnir.security.JWToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface PlayerService {

    List<Player> listAll();

    Optional<Player> findById(long playerId);

    Player fetch(long playerId);

    Optional<Player> findByUsername(String username);

    Player fetchByUsername(String username);

    JWToken createPlayer(Player player);

    Player updatePlayer(Player player);

    Player deletePlayer(long playerId);

    JWToken login(PlayerLoginDTO playerDTO);

    UserDetails loadUserDetailsByUsername(String username);

    Set<SimpleGrantedAuthority> getAuthority(Player player);

    void validate(Player player);
}
