package hr.fer.plavy.tim.esports_turnir.repository;

import hr.fer.plavy.tim.esports_turnir.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface GameRepository extends JpaRepository<Game, Long> {

    boolean existsByName(String name);

    boolean existsByNameAndIdNot(String name, long gameId);

    Optional<Game> findByName(String name);
}
