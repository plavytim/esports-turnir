package hr.fer.plavy.tim.esports_turnir.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.*;

@Entity
@Table(name = "TOURNAMENTS")

public class Tournament {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = true)
    @Size(min = 3)
    private String tournamentName;

    @OneToOne
    private Team winnerTeam;

    @OneToMany(targetEntity=Match.class, mappedBy="tournament", cascade = {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.EAGER)
    private Set<Match> matches;

    @OneToOne
    private Game game;

    public Tournament() {
    }

    public Tournament(String tournamentName, Game game) {
        this.tournamentName = tournamentName;
        this.game = game;
        this.winnerTeam = null;
        this.matches = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public Team getWinnerTeam() {
        return winnerTeam;
    }

    public void setWinnerTeam(Team winnerTeam) {
        this.winnerTeam = winnerTeam;
    }

    public Set<Match> getMatches() {
        return matches;
    }

    public void setMatches(Set<Match> matches) {
        this.matches = matches;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public String toString() {
        return "tournamentName='" + tournamentName + '\'' +
                ", matches=" + matches
                ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tournament)) return false;
        Tournament that = (Tournament) o;
        return getId().equals(that.getId()) && getTournamentName().equals(that.getTournamentName()) && getMatches().equals(that.getMatches()) && getGame().equals(that.getGame());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTournamentName(), getMatches(), getGame());
    }

    public void addMatch(Match match){
        this.matches.add(match);
    }

    public void removeMatch(Match match){
        this.matches.remove(match);
    }
}

