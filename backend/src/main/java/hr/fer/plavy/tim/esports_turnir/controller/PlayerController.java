package hr.fer.plavy.tim.esports_turnir.controller;

import hr.fer.plavy.tim.esports_turnir.exception.RequestDeniedException;
import hr.fer.plavy.tim.esports_turnir.exception.TokenException;
import hr.fer.plavy.tim.esports_turnir.model.Player;
import hr.fer.plavy.tim.esports_turnir.enums.RoleEnum;
import hr.fer.plavy.tim.esports_turnir.security.JWToken;
import hr.fer.plavy.tim.esports_turnir.security.TokenProvider;
import hr.fer.plavy.tim.esports_turnir.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/players")
public class PlayerController {

    private final PlayerService playerService;

    private final TokenProvider jwtToken;

    @Autowired
    public PlayerController(PlayerService service, TokenProvider jwtToken) {
        this.playerService = service;
        this.jwtToken = jwtToken;
    }

    @GetMapping("/status")
    public String getStatus() {
        return "Status OK";
    }

    @GetMapping("")
    public List<Player> listPlayers() {
        return playerService.listAll();
    }

    @GetMapping("/{id}")
    public Player getPlayer(@PathVariable("id") long playerId) {
        return playerService.fetch(playerId);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'PLAYER', 'GAMEMASTER')")
    @PostMapping("")
    public ResponseEntity<JWToken> createPlayer(@RequestBody Player player) {
        JWToken newPlayer = playerService.createPlayer(player);
        return ResponseEntity.created(URI.create("/players/" + newPlayer.getId())).body(newPlayer);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'PLAYER', 'GAMEMASTER')")
    @PutMapping("/{id}")
    public Player updatePlayer(@PathVariable("id") long playerId, @RequestBody Player player, @RequestHeader("Authorization") String authHeader) {
        checkIdOrAdmin(playerId, authHeader);
        if (!player.getId().equals(playerId)) {
            throw new RequestDeniedException("Player ID must be preserved");
        }
        if (!player.getRole().getId().equals(playerService.fetch(playerId).getRole().getId()) && !checkAdmin(authHeader)){
            throw new RequestDeniedException("Admin privileges required for this action");
        }
        return playerService.updatePlayer(player);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'PLAYER', 'GAMEMASTER')")
    @DeleteMapping("/{id}")
    public Player deletePlayer(@PathVariable("id") long playerId, @RequestHeader("Authorization") String authHeader) {
        checkIdOrAdmin(playerId, authHeader);
        return playerService.deletePlayer(playerId);
    }

    private String getToken(String authHeader) {
        String[] authHeaderArr = authHeader.split("Bearer");
        if (authHeader.length() < 1 && authHeaderArr[1] == null) {
            throw new TokenException("Authorization token must be Bearer [token]");
        }
        return authHeaderArr[1];
    }

    private void checkId(long playerId, String authHeader) {
        String token = getToken(authHeader);
        if (!playerService.fetchByUsername(jwtToken.getUsernameFromToken(token)).getId().equals(playerId)){
            throw new RequestDeniedException("TokenId and playerId do not match");
        }
    }

    private void checkIdOrAdmin(long playerId, String authHeader) {
        String token = getToken(authHeader);
        if (!playerService.fetchByUsername(jwtToken.getUsernameFromToken(token)).getRole().getRolename().
                equals(RoleEnum.ADMIN.toString())){
            checkId(playerId, authHeader);
        }
    }

    private Boolean checkAdmin(String authHeader){
        String token = getToken(authHeader);
        return playerService.fetchByUsername(jwtToken.getUsernameFromToken(token)).getRole().getRolename().
                equals(RoleEnum.ADMIN.toString());
    }
}
