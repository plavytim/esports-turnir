package hr.fer.plavy.tim.esports_turnir.service;

import hr.fer.plavy.tim.esports_turnir.dto.CreateMatchDTO;
import hr.fer.plavy.tim.esports_turnir.dto.UpdateMatchDTO;
import hr.fer.plavy.tim.esports_turnir.model.Match;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface MatchService {

    List<Match> listAll();

    Optional<Match> findById(long matchId);

    Match fetch(long matchId);

    Match createMatch(CreateMatchDTO matchDTO);

    Match updateMatch(UpdateMatchDTO matchDTO);

    Match deleteMatch(long matchId);

    Optional<Set<Match>> findTeamsAllMatches(long teamId);

    Set<Match> fetchTeamsAllMatches(long teamId);

    void sendCalendarInvites(CreateMatchDTO newMatchDTO, Match createdMatch) throws Exception ;
}
