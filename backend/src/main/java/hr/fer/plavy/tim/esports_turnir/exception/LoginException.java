package hr.fer.plavy.tim.esports_turnir.exception;

public class LoginException extends IllegalArgumentException {

    public LoginException() {
        super("Login informations are invalid");
    }
}
