package hr.fer.plavy.tim.esports_turnir.dto;

import hr.fer.plavy.tim.esports_turnir.model.Team;
import hr.fer.plavy.tim.esports_turnir.model.Tournament;

import java.time.LocalDateTime;

public class UpdateMatchDTO {
    private Long id;
    private Tournament tournament;
    private LocalDateTime beginning;
    private Team team1;
    private Team team2;
    private Long winner;

    public UpdateMatchDTO(Long id, Tournament tournament, LocalDateTime beginning, Team team1, Team team2, Long winner) {
        this.id = id;
        this.tournament = tournament;
        this.beginning = beginning;
        this.team1 = team1;
        this.team2 = team2;
        this.winner = winner;
    }

    public Long getId() {
        return id;
    }

    public Tournament getTournament() {
        return tournament;
    }

    public void setTournament(Tournament tournament) {
        this.tournament = tournament;
    }

    public LocalDateTime getBeginning() {
        return beginning;
    }

    public void setBeginning(LocalDateTime beginning) {
        this.beginning = beginning;
    }

    public Team getTeam1() {
        return team1;
    }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }

    public Long getWinner() {
        return winner;
    }

    public void setWinner(Long winner) {
        this.winner = winner;
    }
}
