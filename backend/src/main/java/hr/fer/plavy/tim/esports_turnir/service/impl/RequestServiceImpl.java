package hr.fer.plavy.tim.esports_turnir.service.impl;

import hr.fer.plavy.tim.esports_turnir.exception.EntityMissingException;
import hr.fer.plavy.tim.esports_turnir.model.JoinTeamRequest;
import hr.fer.plavy.tim.esports_turnir.model.Player;
import hr.fer.plavy.tim.esports_turnir.repository.RequestRepository;
import hr.fer.plavy.tim.esports_turnir.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RequestServiceImpl implements RequestService {

    private RequestRepository requestRepository;

    @Autowired
    public void setRequestRepository(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    @Override
    public Optional<JoinTeamRequest> findById(Long requestId) {
        return requestRepository.findById(requestId);
    }

    @Override
    public JoinTeamRequest fetch(Long requestId) {
        return findById(requestId).orElseThrow(
                () -> new EntityMissingException(JoinTeamRequest.class, requestId)
        );
    }

    @Override
    public JoinTeamRequest createRequest(Player player, String message) {
        return requestRepository.save(new JoinTeamRequest(
                player,
                message
        ));
    }

    @Override
    public JoinTeamRequest deleteRequest(JoinTeamRequest request) {
        requestRepository.delete(request);
        return request;
    }
}
