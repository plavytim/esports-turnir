package hr.fer.plavy.tim.esports_turnir.service.impl;

import hr.fer.plavy.tim.esports_turnir.dto.PlayerLoginDTO;
import hr.fer.plavy.tim.esports_turnir.exception.EntityMissingException;
import hr.fer.plavy.tim.esports_turnir.exception.LoginException;
import hr.fer.plavy.tim.esports_turnir.exception.RequestDeniedException;
import hr.fer.plavy.tim.esports_turnir.model.Player;
import hr.fer.plavy.tim.esports_turnir.enums.RoleEnum;
import hr.fer.plavy.tim.esports_turnir.model.Team;
import hr.fer.plavy.tim.esports_turnir.repository.PlayerRepository;
import hr.fer.plavy.tim.esports_turnir.security.JWToken;
import hr.fer.plavy.tim.esports_turnir.security.TokenProvider;
import hr.fer.plavy.tim.esports_turnir.service.PlayerService;
import hr.fer.plavy.tim.esports_turnir.service.RoleService;
import hr.fer.plavy.tim.esports_turnir.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class PlayerServiceImpl implements PlayerService {

    private PlayerRepository playerRepository;
    private TeamService teamService;
    private RoleService roleService;
    private TokenProvider tokenProvider;

    @Autowired
    public void setPlayerRepository(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @Autowired
    public void setTeamService(TeamService teamService) {
        this.teamService = teamService;
    }

    @Autowired
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @Autowired
    public void setTokenProvider(TokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    @Value("${email.format}")
    private String EMAIL_FORMAT;


    @Override
    public List<Player> listAll() {
        return playerRepository.findAll();
    }

    @Override
    public Optional<Player> findById(long playerId) {
        return playerRepository.findById(playerId);
    }

    @Override
    public Player fetch(long playerId) {
        return findById(playerId).orElseThrow(
                () -> new EntityMissingException(Player.class, playerId)
        );
    }

    @Override
    public Optional<Player> findByUsername(String username) {
        return playerRepository.findByUsername(username);
    }

    @Override
    public Player fetchByUsername(String username) {
        return findByUsername(username).orElseThrow(
                () -> new EntityMissingException(Player.class, username)
        );
    }

    @Override
    public JWToken createPlayer(Player player) {
        validate(player);
        Assert.isNull(player.getId(), "Player ID must be null, not: " + player.getId());

        if (playerRepository.existsPlayerByUsername(player.getUsername())) {
            throw new RequestDeniedException(
                    "Player with username: " + player.getUsername() + " already exists"
            );
        }
        if (player.getRole() == null) {
            player.setRole(roleService.fetchByRolename(RoleEnum.PLAYER.toString()));
        }
        return tokenProvider.generateJWTToken(playerRepository.save(player));
    }

    @Override
    public Player updatePlayer(Player player) {
        validate(player);
        long playerId = player.getId();
        if (!playerRepository.existsById(playerId)) {
            throw new EntityMissingException(Player.class, playerId);
        } else if (playerRepository.existsPlayerByUsernameAndIdNot(player.getUsername(), playerId)) {
            throw new RequestDeniedException(
                    "Player with username " + player.getUsername() + " already exists"
            );
        }

        return playerRepository.save(player);
    }

    @Override
    public Player deletePlayer(long playerId) {
        Player player = fetch(playerId);
        changeLeadIfPlayerIsLead(player);
        playerRepository.delete(player);
        return player;
    }

    @Override
    public JWToken login(PlayerLoginDTO playerDTO) {
        validateLoginDTO(playerDTO);

        Optional<Player> loggedPlayer = playerRepository.findByUsernameAndPassword(playerDTO.getUsername(), playerDTO.getPassword());

        return tokenProvider.generateJWTToken(loggedPlayer.orElseThrow(LoginException::new));
    }

    @Override
    public UserDetails loadUserDetailsByUsername(String username) {
        Player player = fetchByUsername(username);
        return new User(player.getUsername(), player.getPassword(), getAuthority(player));
    }

    @Override
    public Set<SimpleGrantedAuthority> getAuthority(Player player) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + player.getRole().getRolename()));
        return authorities;
    }

    @Override
    public void validate(Player player) {
        Assert.notNull(player, "Player object must be given");

        String firstname = player.getFirstname();
        Assert.hasText(firstname, "First name must be given");
        Assert.isTrue(firstname.length() >= 2, "First name must be at least 2 characters long");

        String lastname = player.getLastname();
        Assert.hasText(lastname, "Last name must be given");
        Assert.isTrue(lastname.length() >= 2, "Last name must be at least 2 characters long");

        String username = player.getUsername();
        Assert.hasText(username, "Username must be given");
        Assert.isTrue(username.length() >= 5 && username.length() <= 15,
                "Username must be at leas 5 and maximum 15 characters long");

        Assert.hasText(player.getPassword(), "Password must be given");

        String email = player.getEmail();
        Assert.hasText(email, "Email must be given");
        Assert.isTrue(email.matches(EMAIL_FORMAT), "Email must be in valid @fer.hr format");
    }

    private void validateLoginDTO(PlayerLoginDTO playerDTO) {
        Assert.notNull(playerDTO, "Login information must be given");

        String username = playerDTO.getUsername();
        Assert.hasText(username, "Username must be given");
        Assert.isTrue(username.length() >= 5 && username.length() <= 15, "Username must be 5-15 characters long");

        Assert.hasText(playerDTO.getPassword(), "Password must be given");
    }

    public void changeLeadIfPlayerIsLead(Player player){
        Optional<List<Team>> optionalTeams = teamService.findByTeamLead(player);

        optionalTeams.ifPresent(teams -> teams.forEach(team -> teamService.removeLeadFromTeam(team.getId())));
    }
}
