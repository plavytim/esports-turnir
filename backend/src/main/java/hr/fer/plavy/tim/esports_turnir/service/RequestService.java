package hr.fer.plavy.tim.esports_turnir.service;

import hr.fer.plavy.tim.esports_turnir.model.JoinTeamRequest;
import hr.fer.plavy.tim.esports_turnir.model.Player;

import java.util.Optional;

public interface RequestService {

    Optional<JoinTeamRequest> findById(Long requestId);

    JoinTeamRequest fetch(Long requestId);

    JoinTeamRequest createRequest(Player player, String message);

    JoinTeamRequest deleteRequest(JoinTeamRequest request);
}
