package hr.fer.plavy.tim.esports_turnir.repository;

import hr.fer.plavy.tim.esports_turnir.model.JoinTeamRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestRepository extends JpaRepository<JoinTeamRequest, Long> {
}
