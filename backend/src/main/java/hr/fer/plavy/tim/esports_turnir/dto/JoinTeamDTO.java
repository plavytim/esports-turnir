package hr.fer.plavy.tim.esports_turnir.dto;

public class JoinTeamDTO {
    private String playerUsername;
    private String requestMessage;

    public JoinTeamDTO(String playerUsername, String requestMessage) {
        this.playerUsername = playerUsername;
        this.requestMessage = requestMessage;
    }

    public String getPlayerUsername() {
        return playerUsername;
    }

    public void setPlayerUsername(String playerUsername) {
        this.playerUsername = playerUsername;
    }

    public String getRequestMessage() {
        return requestMessage;
    }

    public void setRequestMessage(String requestMessage) {
        this.requestMessage = requestMessage;
    }
}
