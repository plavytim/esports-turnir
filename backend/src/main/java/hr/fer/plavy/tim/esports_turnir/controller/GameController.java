package hr.fer.plavy.tim.esports_turnir.controller;

import hr.fer.plavy.tim.esports_turnir.model.Game;
import hr.fer.plavy.tim.esports_turnir.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/games")
public class GameController {

    private final GameService gameService;

    @Autowired
    public GameController(GameService service) {
        this.gameService = service;
    }

    @GetMapping("/status")
    public String getStatus() {
        return "Games Status OK";
    }

    @GetMapping("")
    public List<Game> getAllGames() {
        return gameService.listAll();
    }

    @GetMapping("/{id}")
    public Game getGame(@PathVariable("id") long gameId) {
        return gameService.fetch(gameId);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'GAMEMASTER')")
    @PostMapping("")
    public ResponseEntity<Game> createGame(@RequestBody Game newGame) {
        Game createdGame = gameService.createGame(newGame);
        return ResponseEntity.created(URI.create("/games" + createdGame.getId())).body(createdGame);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'GAMEMASTER')")
    @PutMapping("/{id}")
    public Game updateGame(@PathVariable("id") long gameId, @RequestBody Game game) {
        if (!game.getId().equals(gameId)) {
            throw new IllegalArgumentException("Game ID must be preserved");
        }
        return gameService.updateGame(game);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'GAMEMASTER')")
    @DeleteMapping("/{id}")
    public Game deleteGame(@PathVariable("id") long gameId) {
        return gameService.deleteGame(gameId);
    }
}
