package hr.fer.plavy.tim.esports_turnir.security;

public class SecurityConstants {
    public static final String API_SECRET_KEY = "secret";
    public static final long TOKEN_VALIDITY = 12 * 60 * 60 * 1000;

}
