package hr.fer.plavy.tim.esports_turnir.controller;

import hr.fer.plavy.tim.esports_turnir.dto.JoinTeamDTO;
import hr.fer.plavy.tim.esports_turnir.dto.PlayerUsernameDTO;
import hr.fer.plavy.tim.esports_turnir.dto.CreateTeamDTO;
import hr.fer.plavy.tim.esports_turnir.dto.UpdateTeamDTO;
import hr.fer.plavy.tim.esports_turnir.enums.RoleEnum;
import hr.fer.plavy.tim.esports_turnir.model.Team;
import hr.fer.plavy.tim.esports_turnir.security.TokenProvider;
import hr.fer.plavy.tim.esports_turnir.service.PlayerService;
import hr.fer.plavy.tim.esports_turnir.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/teams")
public class TeamController {

    private final TeamService teamService;

    private final PlayerService playerService;

    private final TokenProvider jwtToken;

    @Autowired
    public TeamController(TeamService service, PlayerService playerService, TokenProvider jwtToken) {
        this.teamService = service;
        this.playerService = playerService;
        this.jwtToken = jwtToken;
    }

    @GetMapping("")
    public List<Team> getAllTeams() {
        return teamService.listAll();
    }

    @GetMapping("/{teamId}")
    public Team getTeam(@PathVariable long teamId){
        return teamService.fetch(teamId);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'PLAYER', 'GAMEMASTER')")
    @PostMapping("")
    public ResponseEntity<Team> createTeam(@RequestBody CreateTeamDTO newTeamDTO){
        Team createdTeam = teamService.createTeam(newTeamDTO);
        return ResponseEntity.created(URI.create("/teams/" + createdTeam.getId())).body(createdTeam);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'PLAYER', 'GAMEMASTER')")
    @PutMapping("/{teamId}")
    public Team updateTeam(@PathVariable long teamId, @RequestBody UpdateTeamDTO updateTeamDTO, @RequestHeader("Authorization") String authHeader){
        checkLeadId(teamService.fetch(teamId).getLeadPlayer().getId(), authHeader);
        if(!updateTeamDTO.getId().equals(teamId)){
            throw new IllegalArgumentException("Team ID must be preserved");
        }

        return teamService.updateTeam(updateTeamDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'PLAYER', 'GAMEMASTER')")
    @DeleteMapping("/{teamId}")
    public Team deleteTeam(@PathVariable long teamId, @RequestHeader("Authorization") String authHeader){
        checkLeadId(teamService.fetch(teamId).getLeadPlayer().getId(), authHeader);
        return teamService.deleteTeam(teamId);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("/{teamId}/players")
    public ResponseEntity<Team> addPlayerToTeam(@PathVariable long teamId, @RequestBody PlayerUsernameDTO player, @RequestHeader("Authorization") String authHeader){
        checkLeadId(teamService.fetch(teamId).getLeadPlayer().getId(), authHeader);
        Team updatedTeam = teamService.addPlayerToTeam(teamId, player);
        return ResponseEntity.accepted().body(updatedTeam);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'PLAYER', 'GAMEMASTER')")
    @DeleteMapping("/{teamId}/players/{playerId}")
    public Team removePlayerFromTeam(@PathVariable long teamId, @PathVariable long playerId, @RequestHeader("Authorization") String authHeader){
        String token = getToken(authHeader);

        /*TODO
            ovo pokusat prebacit u service, ne bi trebali imat playerService u TeamControlleru
            takodjer vidjet je li uredu da fetch bude u controlleru ili i to prebacujemo u service
         */
        if (!playerService.fetchByUsername(jwtToken.getUsernameFromToken(token)).getId().equals(playerId)) {
            checkLeadIdOrAdmin(teamId, authHeader);
        }
        return teamService.removePlayerFromTeam(teamId, playerId);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'PLAYER', 'GAMEMASTER')")
    @PutMapping("/{teamId}/leadPlayer")
    public Team setNewTeamLead(@PathVariable long teamId, @RequestBody PlayerUsernameDTO playerUsernameDTO, @RequestHeader("Authorization") String authHeader){
        checkLeadIdOrAdmin(teamId, authHeader);
        return teamService.setNewTeamLead(teamId, playerUsernameDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'PLAYER', 'GAMEMASTER')")
    @DeleteMapping("/{teamId}/leadPlayer")
    public Team removeLeadFromTeam(@PathVariable long teamId, @RequestHeader("Authorization") String authHeader){
        checkLeadIdOrAdmin(teamId, authHeader);
        return teamService.removeLeadFromTeam(teamId);
    }

    @PostMapping("/{teamId}/requests")
    public Team sendRequestToTeam(@PathVariable long teamId, @RequestBody JoinTeamDTO joinTeamDTO, @RequestHeader("Authorization") String authHeader){
        return teamService.sendRequest(teamId, joinTeamDTO);
    }

    @PostMapping("/{teamId}/requests/{requestId}")
    public Team confirmRequest(@PathVariable long teamId, @PathVariable long requestId, @RequestHeader("Authorization") String authHeader){
        checkLeadIdOrAdmin(teamId, authHeader);
        return teamService.confirmRequest(teamId, requestId);
    }

    @DeleteMapping("/{teamId}/requests/{requestId}")
    public Team rejectRequest(@PathVariable long teamId, @PathVariable long requestId, @RequestHeader("Authorization") String authHeader){
        checkLeadIdOrAdmin(teamId, authHeader);
        return teamService.rejectRequest(teamId, requestId);
    }

    private void checkLeadIdOrAdmin(@PathVariable long teamId, @RequestHeader("Authorization") String authHeader) {
        String token = getToken(authHeader);
        if (!playerService.fetchByUsername(jwtToken.getUsernameFromToken(token)).getRole().getRolename().
                equals(RoleEnum.ADMIN.toString())) {
            checkLeadId(teamService.fetch(teamId).getLeadPlayer().getId(), authHeader);
        }
    }

    private String getToken(String authHeader) {
        String[] authHeaderArr = authHeader.split("Bearer");
        if (authHeader.length() < 1 && authHeaderArr[1] == null) {
            throw new IllegalArgumentException("Authorization token must be Bearer [token]");
        }
        return authHeaderArr[1];
    }

    private void checkLeadId(long playerId, String authHeader) {
        String token = getToken(authHeader);
        if (!playerService.fetchByUsername(jwtToken.getUsernameFromToken(token)).getId().equals(playerId)){
            throw new IllegalArgumentException("TokenId and playerId do not match");
        }
    }
}
