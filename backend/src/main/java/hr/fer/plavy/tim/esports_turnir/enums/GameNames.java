package hr.fer.plavy.tim.esports_turnir.enums;

public class GameNames {
    public static final String CSGO = "Counter-Strike: Global Offensive";
    public static final String LOL = "League Of Legends";
    public static final String COD = "Call Of Duty";
}
