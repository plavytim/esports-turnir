package hr.fer.plavy.tim.esports_turnir.enums;

public enum RoleEnum {
    ADMIN,
    GAMEMASTER,
    PLAYER
}
