package hr.fer.plavy.tim.esports_turnir.service.impl;

import hr.fer.plavy.tim.esports_turnir.dto.CreateMatchDTO;
import hr.fer.plavy.tim.esports_turnir.dto.UpdateMatchDTO;
import hr.fer.plavy.tim.esports_turnir.email.EmailSenderService;
import hr.fer.plavy.tim.esports_turnir.exception.EntityMissingException;
import hr.fer.plavy.tim.esports_turnir.exception.RequestDeniedException;
import hr.fer.plavy.tim.esports_turnir.email.CalendarRequest;
import hr.fer.plavy.tim.esports_turnir.model.*;
import hr.fer.plavy.tim.esports_turnir.repository.MatchRepository;
import hr.fer.plavy.tim.esports_turnir.service.MatchService;
import hr.fer.plavy.tim.esports_turnir.service.TeamService;
import hr.fer.plavy.tim.esports_turnir.service.TournamentService;
import hr.fer.plavy.tim.esports_turnir.util.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

@Service
public class MatchServiceImpl implements MatchService {

    private MatchRepository matchRepository;
    private TeamService teamService;
    private TournamentService tournamentService;
    private EmailSenderService emailSenderService;

    @Value("${email.address}")
    private String emailAddress;

    @Value("${calendarInvite.subject}")
    private String calendarInviteSubject;

    @Value("${calendarInvite.message}")
    private String calendarInviteMessage;

    @Autowired
    public void setMatchRepository(MatchRepository matchRepository) {
        this.matchRepository = matchRepository;
    }

    @Autowired
    public void setTeamService(TeamService teamService) {
        this.teamService = teamService;
    }

    @Autowired
    public void setTournamentService(TournamentService tournamentService) {
        this.tournamentService = tournamentService;
    }

    @Autowired void setEmailSenderService(EmailSenderService emailSenderService){
        this.emailSenderService = emailSenderService;
    }

    @Override
    public List<Match> listAll() {
        return matchRepository.findAll();
    }

    @Override
    public Optional<Match> findById(long matchId) {
        return matchRepository.findById(matchId);
    }

    @Override
    public Match fetch(long matchId) {
        return findById(matchId).orElseThrow(
                () -> new EntityMissingException(Match.class, matchId)
        );
    }

    //provjera broja clanova tima u odnosu na game clan tima
    @Override
    public Match createMatch(CreateMatchDTO newMatchDTO) {
        validateCreateDTO(newMatchDTO);

        Team team1 = teamService.fetchByTeamName(newMatchDTO.getTeam1Name());
        Team team2 = teamService.fetchByTeamName(newMatchDTO.getTeam2Name());
        Tournament tournament = tournamentService.fetchByTournamentName(newMatchDTO.getTournamentName());

        boolean teamsAlreadyPlay = tournament.getMatches().stream().anyMatch(match -> {
            Team firstTeam = match.getTeam1();
            Team secondTeam = match.getTeam2();

            return firstTeam.equals(team1) && secondTeam.equals(team2) || firstTeam.equals(team2) && secondTeam.equals(team1);
        });

        if (teamsAlreadyPlay) {
            throw new RequestDeniedException(
                    "Teams " + team1.getTeamName() + " and " + team2.getTeamName() + " already play "
            );
        }

        Match createdMatch = new Match(
                newMatchDTO.getMatchBeginning(),
                team1,
                team2,
                tournament
        );

        createdMatch = matchRepository.save(createdMatch);

        return createdMatch;
    }

    @Override
    public Match updateMatch(UpdateMatchDTO updateMatchDTO) {
        validateUpdateDTO(updateMatchDTO);

        long matchId = updateMatchDTO.getId();

        if(!matchRepository.existsMatchById(matchId)){
            throw new RequestDeniedException(
                    "Match with this Id does not exist"
            );
        }

        Match oldMatch = matchRepository.getById(matchId);

        if(!updateMatchDTO.getBeginning().equals(oldMatch.getBeginning())){
            oldMatch.setBeginning(updateMatchDTO.getBeginning());
        }

        if(!updateMatchDTO.getTeam1().equals(oldMatch.getTeam1())){
            oldMatch.setTeam1(updateMatchDTO.getTeam1());
        }

        if(!updateMatchDTO.getTeam2().equals(oldMatch.getTeam2())){
            oldMatch.setTeam2(updateMatchDTO.getTeam2());
        }

        if(!updateMatchDTO.getWinner().equals(oldMatch.getWinnerTeamId())){
            oldMatch.setWinnerTeamId(teamService.fetch(updateMatchDTO.getWinner()).getId());
        }

        return matchRepository.save(oldMatch);

    }

    @Override
    public Optional<Set<Match>> findTeamsAllMatches(long teamId){
        Team team = teamService.fetch(teamId);

        return matchRepository.findAllMatchesOfTeam(team);
    }

    @Override
    public Set<Match> fetchTeamsAllMatches(long teamId) {
        return findTeamsAllMatches(teamId).orElseThrow(
                () -> new EntityMissingException(Team.class, teamId)
        );
    }

    @Override
    public Match deleteMatch(long matchId) {
        Match match = fetch(matchId);
        matchRepository.delete(match);
        return match;
    }

    @Override
    public void sendCalendarInvites(CreateMatchDTO newMatchDTO, Match createdMatch) throws Exception {
        for(Player player : createdMatch.getTeam1().getPlayers()){
            emailSenderService.sendCalendarInvite(
                    emailAddress,
                    new CalendarRequest.Builder()
                            .withSubject(calendarInviteSubject)
                            .withBody(String.format(calendarInviteMessage, player.getUsername()))
                            .withToEmail(player.getEmail())
                            .withMeetingStartTime(createdMatch.getBeginning())
                            .withMeetingEndTime(createdMatch.getBeginning().plusMinutes(tournamentService
                                    .fetchByTournamentName(newMatchDTO.getTournamentName())
                                    .getGame().getDuration().getMinute()))
                            .build()
            );
        }
        for(Player player : createdMatch.getTeam2().getPlayers()){
            emailSenderService.sendCalendarInvite(
                    emailAddress,
                    new CalendarRequest.Builder()
                            .withSubject(calendarInviteSubject)
                            .withBody(String.format(calendarInviteMessage, player.getUsername()))
                            .withToEmail(player.getEmail())
                            .withMeetingStartTime(createdMatch.getBeginning())
                            .withMeetingEndTime(createdMatch.getBeginning().plusMinutes(tournamentService
                                    .fetchByTournamentName(newMatchDTO.getTournamentName())
                                    .getGame().getDuration().getMinute()))
                            .build()
            );
        }
    }

    private void validate(Match match) {
        Assert.notNull(match, "Match object must be given");

        LocalDateTime beginning = match.getBeginning();
        Assert.notNull(beginning, "Beginning must be given");

        Team team1 = match.getTeam1();
        Assert.notNull(team1, "Team1 must be given");

        Team team2 = match.getTeam2();
        Assert.notNull(team2, "Team2 must be given");
    }

    private void validateCreateDTO(CreateMatchDTO matchDTO) {
        Assert.notNull(matchDTO, "Match information must be given");

        LocalDateTime beginning = matchDTO.getMatchBeginning();
        Assert.notNull(beginning, "Beginning must be given");

        String tournamentName = matchDTO.getTournamentName();
        Assert.hasText(tournamentName, "Tournament name must be given");
        Assert.isTrue(tournamentName.length() >= 3,
                "Tournament name must be at least 3 characters long");

        String team1Name = matchDTO.getTeam1Name();
        Assert.hasText(team1Name, "Name for Team 1 must be given");
        Assert.isTrue(team1Name.length() >= 3 && team1Name.length() <= 15,
                "Name for Team 1 must be at least 3 and maximum 15 characters long");

        String team2Name = matchDTO.getTeam2Name();
        Assert.hasText(team2Name, "Name for Team 2 must be given");
        Assert.isTrue(team2Name.length() >= 3 && team2Name.length() <= 15,
                "Name for Team 2 must be at least 3 and maximum 15 characters long");
    }

    private void validateUpdateDTO(UpdateMatchDTO matchDTO){
        Assert.notNull(matchDTO, "Match object must be given");

        Tournament tournament = matchDTO.getTournament();
        Assert.notNull(tournament, "Tournament must be given");

        LocalDateTime beginning = matchDTO.getBeginning();
        Assert.notNull(beginning, "Beggining must be given");

        Team team1 = matchDTO.getTeam1();
        Assert.notNull(team1, "Team 1 must be given");

        Team team2 = matchDTO.getTeam2();
        Assert.notNull(team2, "Team 2 must be given");

        Assert.isTrue(team1.getGame().equals(team2.getGame()), "Teams must play the same game");
        Assert.isTrue(tournament.getGame().equals(team1.getGame()), "Tournament game and teams game must be the same");

        if (matchDTO.getWinner() != null) {
            Long winnerId = matchDTO.getWinner();
            Optional<Team> winnerTeam = teamService.findById(winnerId);
            winnerTeam.ifPresent(team -> Assert.isTrue(
                    team.equals(team1) || team.equals(team2),
                    "Winner team must be team 1 or team 2 or null")
            );
        }

        LocalTime duration = matchDTO.getTeam1().getGame().getDuration();
        LocalDateTime endOfMatch = DateTimeUtil.addLocalTime(beginning, duration);
        LocalDateTime beginningOfMatch = DateTimeUtil.registrationDeadline(beginning);

        Optional<Set<Match>> optionalOtherMatches = matchRepository.findAllMatchesOfTeams(team1, team2);

        if(optionalOtherMatches.isPresent()){
            boolean existsMatch = optionalOtherMatches.get().stream().anyMatch(match -> {
                LocalDateTime matchBeginning = DateTimeUtil.registrationDeadline(match.getBeginning());
                LocalDateTime matchEnding = DateTimeUtil.addLocalTime(match.getBeginning(), duration);

                return DateTimeUtil.isDuringMatch(matchBeginning, beginningOfMatch, endOfMatch) ||
                        DateTimeUtil.isDuringMatch(matchEnding, beginningOfMatch, endOfMatch);
            });

            Assert.isTrue(!existsMatch, "One of the teams already has a match in the given time");
        }
    }

}
