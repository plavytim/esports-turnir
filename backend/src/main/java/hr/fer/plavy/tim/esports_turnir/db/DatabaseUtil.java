package hr.fer.plavy.tim.esports_turnir.db;

import hr.fer.plavy.tim.esports_turnir.dto.CreateMatchDTO;
import hr.fer.plavy.tim.esports_turnir.dto.CreateTeamDTO;
import hr.fer.plavy.tim.esports_turnir.dto.CreateTournamentDTO;
import hr.fer.plavy.tim.esports_turnir.model.Game;
import hr.fer.plavy.tim.esports_turnir.model.Player;
import hr.fer.plavy.tim.esports_turnir.model.Role;
import hr.fer.plavy.tim.esports_turnir.enums.RoleEnum;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

//be964293b51c3863e63092a55411b6ebc0bf4f90 - karlopass
//c0f09a44625c52ff047ca9da92f0a951421da182 - martpass
//0e2a6662351e8a6b17c4cf3dfdf9216887148c24 - tinpass
//5b196f2b04a3da764a1a5cb8098d7d8e5f3932f3 - andropass
//24f40f8499afab976b45e4361334f52df1c98f32 - ivanpass
//0d0eac84ac5267b9fdb32688f98a0fdccf4b5fdc - lukinasifra
//2ca01b5e14fff3fb5ea249b9077df1bfc76e78fd - ivanna1212
//76933ff77291367f7e184ac8a0c952ff08118522 - paula12
//00607f436433b3f75d5e384cf36119a1d87c03c9 - helenana676
//6b952f890ba1bcbad11c56e47236e29cb8919696 - miranmiro
//ff131f23d97746d2fe29b358442c79c33833a42f - pperko
//114648e4af40213f72ec8cb6bda1e0d8ead8341a - bobirobi

public class DatabaseUtil {

    private static final Role roleAdmin = new Role(
            RoleEnum.ADMIN.toString()
    );

    private static final Role roleGamemaster = new Role(
            RoleEnum.GAMEMASTER.toString()
    );

    private static final Role rolePlayer = new Role(
            RoleEnum.PLAYER.toString()
    );

    public static List<Role> getRoles() { return List.of(roleAdmin, roleGamemaster, rolePlayer); }

    private static final Player player1 = new Player(
            "Karlo",
            "Karlic",
            "karlo_karlic",
            "be964293b51c3863e63092a55411b6ebc0bf4f90",
            "kk12345@fer.hr",
            roleAdmin
    );

    private static final Player player2 = new Player(
            "Martina",
            "Martic",
            "martina_martic",
            "c0f09a44625c52ff047ca9da92f0a951421da182",
            "mm12345@fer.hr",
            rolePlayer
    );

    private static final Player player3 = new Player(
            "Tin",
            "Tinic",
            "tin_tinic",
            "0e2a6662351e8a6b17c4cf3dfdf9216887148c24",
            "tt12345@fer.hr",
            rolePlayer
    );

    public static List<Player> getPlayers31() {
        return List.of(player1, player2, player3);
    }

    private static final Player player4 = new Player(
            "Andro",
            "Andric",
            "andro_andric",
            "5b196f2b04a3da764a1a5cb8098d7d8e5f3932f3",
            "aa12345@fer.hr",
            rolePlayer
    );

    private static final Player player5 = new Player(
            "Ivan",
            "Ivanovic",
            "ivan_ivan",
            "24f40f8499afab976b45e4361334f52df1c98f32",
            "iv12345@fer.hr",
            rolePlayer
    );

    private static final Player player6 = new Player(
            "Luka",
            "Lukic",
            "luka123",
            "0d0eac84ac5267b9fdb32688f98a0fdccf4b5fdc",
            "ll12345@fer.hr",
            rolePlayer
    );


    public static List<Player> getPlayers32() {
        return List.of(player4, player5, player6);
    }

    private static final Player player7 = new Player(
            "Ivana",
            "Ivanic",
            "ivanna",
            "2ca01b5e14fff3fb5ea249b9077df1bfc76e78fd",
            "in12345@fer.hr",
            rolePlayer
    );

    private static final Player player8 = new Player(
            "Paula",
            "Paulovic",
            "p1234",
            "76933ff77291367f7e184ac8a0c952ff08118522",
            "pp12345@fer.hr",
            rolePlayer
    );

    public static List<Player> getPlayers21() {
        return List.of(player7, player8);
    }

    private static final Player player9 = new Player(
            "Hele",
            "Nana",
            "helna",
            "00607f436433b3f75d5e384cf36119a1d87c03c9",
            "hh12345@fer.hr",
            rolePlayer
    );

    private static final Player player10 = new Player(
            "Marko",
            "Miric",
            "mmmMM",
            "6b952f890ba1bcbad11c56e47236e29cb8919696",
            "mr12345@fer.hr",
            rolePlayer
    );

    public static List<Player> getPlayers22() {
        return List.of(player9, player10);
    }

    private static final Player player11 = new Player(
            "Pero",
            "Piric",
            "perko",
            "ff131f23d97746d2fe29b358442c79c33833a42f",
            "pf12345@fer.hr",
            rolePlayer
    );

    private static final Player player12 = new Player(
            "Robi",
            "Ribic",
            "RobiBobi",
            "114648e4af40213f72ec8cb6bda1e0d8ead8341a",
            "rg12345@fer.hr",
            rolePlayer
    );

    public static List<Player> getPlayers23() {
        return List.of(player11, player12);
    }

    private static final Player player13 = new Player(
            "Petra",
            "Peric",
            "PeppaPig",
            "114648e4af40213f72ec8cb6bda1e0d8ead8341a",
            "pp54829@fer.hr",
            rolePlayer
    );

    private static final Player player14 = new Player(
            "Sara",
            "Beric",
            "SuzySheep",
            "114648e4af40213f72ec8cb6bda1e0d8ead8341a",
            "ss65439@fer.hr",
            rolePlayer
    );

    private static final Player player15 = new Player(
            "Rebeka",
            "Zecic",
            "RebeccaRabbit",
            "114648e4af40213f72ec8cb6bda1e0d8ead8341a",
            "rr96004@fer.hr",
            rolePlayer
    );

    private static final Player player16 = new Player(
            "Zoe",
            "Zebric",
            "ZoeZebra",
            "114648e4af40213f72ec8cb6bda1e0d8ead8341a",
            "zz77704@fer.hr",
            rolePlayer
    );

    private static final Player player17 = new Player(
            "Pedro",
            "Ponic",
            "PedroPonic",
            "114648e4af40213f72ec8cb6bda1e0d8ead8341a",
            "po88866@fer.hr",
            rolePlayer
    );

    private static final Player player18 = new Player(
            "Mandy",
            "Mouse",
            "MandyTheMouse",
            "114648e4af40213f72ec8cb6bda1e0d8ead8341a",
            "mm11223@fer.hr",
            rolePlayer
    );

    private static final Player player19 = new Player(
            "Molly",
            "Mole",
            "MollyMole",
            "114648e4af40213f72ec8cb6bda1e0d8ead8341a",
            "mm45632@fer.hr",
            rolePlayer
    );

    private static final Player player20 = new Player(
            "Danny",
            "Dogic",
            "DannyTheDog",
            "114648e4af40213f72ec8cb6bda1e0d8ead8341a",
            "dd20006@fer.hr",
            rolePlayer
    );

    public static Player getPlayer13(){ return player13;}
    public static Player getPlayer14(){ return player14;}
    public static Player getPlayer15(){ return player15;}
    public static Player getPlayer16(){ return player16;}
    public static Player getPlayer17(){ return player17;}
    public static Player getPlayer18(){ return player18;}
    public static Player getPlayer19(){ return player19;}
    public static Player getPlayer20(){ return player20;}

    private static final Game game1 = new Game(
            "LOL",
            "MOBA",
            "PC",
            5,
            LocalTime.of(0, 15)
    );

    private static final Game game2 = new Game(
            "CSGO",
            "FPS",
            "PC",
            5,
            LocalTime.of(0, 20)
    );

    public static Game getGame1() {
        return game1;
    }

    public static Game getGame2() {
        return game2;
    }

    private static final CreateTeamDTO team31DTO = new CreateTeamDTO(
            "tim31",
            game1.getName(),
            player1.getUsername()
    );

    private static final CreateTeamDTO team32DTO = new CreateTeamDTO(
            "tim32",
            game1.getName(),
            player4.getUsername()
    );

    private static final CreateTeamDTO team21DTO = new CreateTeamDTO(
            "tim21",
            game2.getName(),
            player7.getUsername()
    );

    private static final CreateTeamDTO team22DTO = new CreateTeamDTO(
            "tim22",
            game2.getName(),
            player9.getUsername()
    );

    private static final CreateTeamDTO team23DTO = new CreateTeamDTO(
            "tim23",
            game2.getName(),
            player11.getUsername()
    );

    public static CreateTeamDTO getTeam31DTO() {
        return team31DTO;
    }

    public static CreateTeamDTO getTeam32DTO() {
        return team32DTO;
    }

    public static CreateTeamDTO getTeam21DTO() {
        return team21DTO;
    }

    public static CreateTeamDTO getTeam22DTO() {
        return team22DTO;
    }

    public static CreateTeamDTO getTeam23DTO() {
        return team23DTO;
    }


    private static final CreateTournamentDTO tournament1DTO = new CreateTournamentDTO(
            "LOLTour",
            game1.getName()
    );

    public static CreateTournamentDTO getTournament1DTO() {
        return tournament1DTO;
    }

    private static final CreateMatchDTO match1DTO = new CreateMatchDTO(
            LocalDateTime.of(2022, 1, 22, 10, 40),
            tournament1DTO.getTournamentName(),
            team31DTO.getTeamName(),
            team32DTO.getTeamName()
    );

    public static CreateMatchDTO getMatch1DTO() {
        return match1DTO;
    }

    private static final CreateTournamentDTO tournament2DTO = new CreateTournamentDTO(
            "CSGOTour",
            game2.getName()
    );

    public static CreateTournamentDTO getTournament2DTO() {
        return tournament2DTO;
    }

    private static final CreateMatchDTO match2DTO = new CreateMatchDTO(
            LocalDateTime.of(2022, 1, 28, 11, 30),
            tournament2DTO.getTournamentName(),
            team21DTO.getTeamName(),
            team22DTO.getTeamName()
    );

    public static CreateMatchDTO getMatch2DTO() {
        return match2DTO;
    }
}
