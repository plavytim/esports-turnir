package hr.fer.plavy.tim.esports_turnir.controller;

import hr.fer.plavy.tim.esports_turnir.dto.CreateMatchDTO;
import hr.fer.plavy.tim.esports_turnir.dto.UpdateMatchDTO;
import hr.fer.plavy.tim.esports_turnir.email.EmailSenderService;
import hr.fer.plavy.tim.esports_turnir.model.Match;
import hr.fer.plavy.tim.esports_turnir.service.MatchService;
import hr.fer.plavy.tim.esports_turnir.service.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/matches")
public class MatchController {

    private final MatchService matchService;

    private final EmailSenderService emailSenderService;

    private final TournamentService tournamentService;

    @Autowired
    public MatchController(MatchService matchService, EmailSenderService emailSenderService, TournamentService tournamentService) {
        this.matchService = matchService;
        this.emailSenderService = emailSenderService;
        this.tournamentService = tournamentService;
    }

    @GetMapping("")
    public List<Match> getAllMatches() {
        return matchService.listAll();
    }

    @GetMapping("/{matchId}")
    public Match getMatch(@PathVariable long matchId) {
        return matchService.fetch(matchId);
    }

    @PostMapping("")
    public ResponseEntity<Match> createMatch(@RequestBody CreateMatchDTO newMatchDTO) throws Exception {
        Match createdMatch = matchService.createMatch(newMatchDTO);
        matchService.sendCalendarInvites(newMatchDTO, createdMatch);
        return ResponseEntity.created(URI.create("/matches/" + createdMatch.getId())).body(createdMatch);
    }

    @PutMapping("/{matchId}")
    public Match updateMatch(@PathVariable long matchId, @RequestBody UpdateMatchDTO updateMatchDTO) {
        if (!updateMatchDTO.getId().equals(matchId)) {
            throw new IllegalArgumentException("Match ID must be preserved");
        }

        return matchService.updateMatch(updateMatchDTO);
    }

    @DeleteMapping("/{matchId}")
    public Match deleteMatch(@PathVariable long matchId) {return matchService.deleteMatch(matchId);}

}
