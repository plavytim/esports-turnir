package hr.fer.plavy.tim.esports_turnir.exception;

public class TokenException extends IllegalArgumentException{

    String message;
    int status;
    String error;

    public TokenException(String message) {
        this.message = message;
        this.status = 403;
        this.error = "Forbidden";
    }

    @Override
    public String toString() {
        return String.format("{\n\t\"message\": \"%s\"\n\t\"error\": \"%s\"\n\t\"status\": \"%d\"\n}",message, error, status);
    }
}
