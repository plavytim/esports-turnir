package hr.fer.plavy.tim.esports_turnir.repository;

import hr.fer.plavy.tim.esports_turnir.model.Match;
import hr.fer.plavy.tim.esports_turnir.model.Team;
import hr.fer.plavy.tim.esports_turnir.model.Tournament;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface MatchRepository extends JpaRepository<Match, Long> {

    Optional<List<Match>> findAllByTournament(Tournament tournament);

    @Query("SELECT m FROM Match m WHERE m.team1 = :team1 OR m.team1 = :team2 OR m.team2 = :team1 OR m.team2 = :team2")
    Optional<Set<Match>> findAllMatchesOfTeams(Team team1, Team team2);

    @Query("SELECT m FROM Match m WHERE m.team1 = :team OR m.team2 = :team")
    Optional<Set<Match>> findAllMatchesOfTeam(Team team);

    boolean existsMatchById(Long id);
}
