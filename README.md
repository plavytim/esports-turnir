# eSports turnir

## Run locally

Requirements:
- jdk 17
- maven
- npm

### Backend

```shell
cd backend
mvn spring-boot:run
```

### Frontend

```shell
cd frontend
npm install
npm start
```

## Visit deployed web app

Web app is deployed at http://turnir.live

## Contributors

- **Tin Plavec** Team Lead, DevOps, Frontend Support, Docs Support
- **Karlo Šutalo** Dev Lead, Backend
- **Luka Penjin** Backend, DevOps Support
- **Ivana Dasović** DB, Backend Support
- **Martina Bertalanić** Frontend, PR, Docs Support
- **Andro Radičević** Frontend, Docs Support
- **Ivan Rajačić** Docs, Design, Frontend Support
